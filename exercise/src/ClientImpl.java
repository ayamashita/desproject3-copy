import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;


public class ClientImpl {
	private static Object[] callService(String action, HashMap hash) throws MalformedURLException, XmlRpcException {
		XmlRpcClientConfigImpl rpcConfig = new XmlRpcClientConfigImpl();
		XmlRpcClient rcpClient = new XmlRpcClient();

		rpcConfig.setServerURL(new URL("http://simula.no:9080/simula/xmlrpcdes"));
		rcpClient.setConfig(rpcConfig);

		Object[] methodParams = new Object[] { hash };
		Object resultSet = rcpClient.execute(action, methodParams);
		Object[] elementList = (Object[]) resultSet;
		return elementList;
	}
	public static Object[] getPublications(HashMap hash) {
		try {
			return callService("getPublications", hash);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	public static Object[] getPeople(HashMap hash) {
		try {
			return callService("getPeople", hash);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

}
