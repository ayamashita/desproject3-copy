import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Exercise {

	/**
	 * Prints out results from a given query. Uses only Integer parameters for
	 * the query and prints all columns of the results, separated by comma.
	 * 
	 * @param conn
	 * @param sql
	 * @param param
	 * @throws SQLException
	 */
	private static void printoutResult(Connection conn, String sql, Integer[] params) {
		try {
			PreparedStatement s = conn.prepareStatement(sql);
			for (int i = 0; i < params.length; i++) {
				Integer p = params[i];
				s.setInt(i + 1, p.intValue());
			}
			s.executeQuery();
			ResultSet rs = s.getResultSet();
			while (rs.next()) {
				System.out.print("  ");
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					System.out.print(rs.getString(i));
					if (i < rs.getMetaData().getColumnCount()) {
						System.out.print(", ");
					}
				}
				System.out.println();
			}
			rs.close();
			s.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Prints out results from a given query. Uses only Integer parameters for
	 * the query and prints all columns of the results, separated by comma.
	 * 
	 * @param conn
	 * @param sql
	 * @param param
	 * @throws SQLException
	 */
	private static ArrayList getResultsString(Connection conn, String sql, Integer[] params) {
		ArrayList list = new ArrayList();
		try {
			PreparedStatement s = conn.prepareStatement(sql);
			for (int i = 0; i < params.length; i++) {
				Integer p = params[i];
				s.setInt(i + 1, p.intValue());
			}
			s.executeQuery();
			ResultSet rs = s.getResultSet();
			while (rs.next()) {
				list.add(rs.getString(1));
			}
			rs.close();
			s.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Connection conn = null;
		try {
			String userName = "admin";
			String password = "masterkey";
			String url = "jdbc:mysql://localhost:3306/des_exercise";
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(url, userName, password);
			System.out.println("Database connection established");

			exerciseA_printStudiesForResponsible(conn, 12);
			exerciseB_printStudiesTypeCount(conn);
			exerciseC_theQuery(conn, new String[] { "SC.4.Dahl.1992", "SC.6.Huseby.2003" });
			exerciseD_peopleByEmails(conn, new String[] { "aiko", "xingca" });
			exerciseE_peopleSorted(conn);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
					System.out.println("Database connection terminated");
				} catch (Exception e) { /* ignore close errors */
				}
			}
		}
	}

	private static void exerciseD_peopleByEmails(Connection conn, String[] emails) {
		for (int i = 0; i < emails.length; i++) {
			String eml = emails[i];
			HashMap hashMap = new HashMap();
			hashMap.put("email", eml);
			Object[] people = ClientImpl.getPeople(hashMap);
			for (int j = 0; j < people.length; j++) {
				HashMap p = (HashMap) people[j];
				System.out.print(p.get("lastname") + ", ");
				System.out.print(p.get("firstname") + ", ");
				System.out.println(p.get("email"));
			}
		}
	}

	private static void exerciseE_peopleSorted(Connection conn) {
		HashMap hashMap = new HashMap();
		hashMap.put("sort_on", "jobtitle");
		Object[] people = ClientImpl.getPeople(hashMap);
		for (int j = 0; j < people.length; j++) {
			HashMap p = (HashMap) people[j];
			System.out.print(p.get("jobtitle") + ", ");
			System.out.print(p.get("lastname") + ", ");
			System.out.print(p.get("firstname") + ", ");
			System.out.println(p.get("email"));
		}
	}

	private static void exerciseC_theQuery(Connection conn, String[] ids) {
		for (int i = 0; i < ids.length; i++) {
			String pid = ids[i];
			HashMap hashMap = new HashMap();
			hashMap.put("id", pid);
			Object[] publications = ClientImpl.getPublications(hashMap);
			for (int j = 0; j < publications.length; j++) {
				HashMap p = (HashMap) publications[j];
				System.out.print(p.get("id") + ", ");
				System.out.print(p.get("Title") + ", ");
				System.out.println(p.get("Source"));
			}
		}
		/*
		 * select pu.publication_id, pu.publication_title, pu.publication_source
		 * from publication pu, publication_author pa where pa.publication_id =
		 * pu.publication_id and pu.publication_id in ( LIST ) order by
		 * pu.publication_title;
		 */

	}

	private static void exerciseB_printStudiesTypeCount(Connection conn) {
		System.out.println("Studies by type");
		printoutResult(conn, "select st.study_type, count(*) as count from study s, study_type st where s.study_type = st.study_type_id", new Integer[0]);
	}

	/**
	 * exercise A
	 * 
	 * @param conn
	 * @param i
	 */
	private static void exerciseA_printStudiesForResponsible(Connection conn, int responsibleId) {
		System.out.println("Studies for person: " + responsibleId);
		printoutResult(conn, "select s.study_name from responsiblesstudies rs, study s where s.study_id = rs.study_id and rs.responsible_id = ?", new Integer[] { new Integer(responsibleId) });

		System.out.println("Publications for person: " + responsibleId);
		ArrayList publicationIDs = getResultsString(conn, "select ps.publication_id from responsiblesstudies rs, publication_study ps where rs.study_id = ps.study_id and  rs.responsible_id = ?", new Integer[] { new Integer(responsibleId) });
		for (Iterator iterator = publicationIDs.iterator(); iterator.hasNext();) {
			String pid = (String) iterator.next();
			HashMap hashMap = new HashMap();
			hashMap.put("id", pid);
			Object[] publications = ClientImpl.getPublications(hashMap);
			for (int i = 0; i < publications.length; i++) {
				HashMap p = (HashMap) publications[i];
				System.out.println(p.get("Title"));
			}
		}
	}

}
