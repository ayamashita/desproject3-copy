/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.data.beans;

import java.util.Collection;


/**
 * Represents a collection of studies
 */
public class StudiesBean {
    private int totalStudies;
    private int startIndex;
    private int endIndex;
    private Collection studies;

    public String toString() {
        return "totalStudies: " + totalStudies + ", startIndex: " + startIndex +
        ", endIndex: " + endIndex + ", studies: " + studies;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public Collection getStudies() {
        return studies;
    }

    public int getTotalStudies() {
        return totalStudies;
    }

    public void setTotalStudies(int totalStudies) {
        this.totalStudies = totalStudies;
    }

    public void setStudies(Collection studies) {
        this.studies = studies;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }
}
