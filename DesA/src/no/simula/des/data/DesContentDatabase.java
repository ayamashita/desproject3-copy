package no.simula.des.data;

import no.simula.des.data.beans.ContentBean;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collection;


/**
 * Retrives and stores front page content
 *
 */
public class DesContentDatabase extends DataObject {
    private final static String GET_CONTENT_QUERY = "select * from descontent where page_id=?;";
    private final static String UPDATE_CONTENT_QUERY = "update descontent set text=? where id=?;";

    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    /**
     * Retrives the content element(s) for a given page. This will normally
     * result in a single ContentBean object wrapped in a Collection to allow for
     * enhancements in case more conent elements may exist on a single page
     *
     * @param pageId ID of the page
     * @return a Collection containing the ContentBean(s) with the page content elements
     * @throws Exception
     */
    public Collection retriveContent(int pageId) throws Exception {
        log.debug("Retriving content for page=" + pageId);

        ArrayList contents = new ArrayList();
        ResultSet rs = null;

        try {
            Connection conn = super.getConnection();
            PreparedStatement pstmt = conn.prepareStatement(DesContentDatabase.GET_CONTENT_QUERY);
            pstmt.setInt(1, pageId);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                ContentBean content = new ContentBean();
                content.setId(rs.getInt(1));
                content.setPage_id(rs.getInt(2));
                content.setText(rs.getString(3));
                contents.add(content);
            }
        } catch (Exception ex) {
            log.error("error retiriving content for page: " + pageId, ex);
        } finally {
            closeConnection(rs);
        }

        return contents;
    }

    /**
     * Updates a page content element
     *
     * @param id identifies the content element to be updated
     * @param pageId the page it is related to
     * @param text the updated content
     * @return true if the update completed successfully
     * @throws Exception
     */
    public boolean updateContent(int id, int pageId, String text)
        throws Exception {
        try {
            Connection conn = super.getConnection();
            PreparedStatement pstmt = conn.prepareStatement(DesContentDatabase.UPDATE_CONTENT_QUERY);
            pstmt.setString(1, text);
            pstmt.setInt(2, id);

            int i = pstmt.executeUpdate();

            if (i > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            log.error("Error occured during update of content", ex);
            throw ex;
        } finally {
            this.closeConnection(null);
        }
    }
}
