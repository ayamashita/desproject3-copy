package no.simula.des.data;

import no.simula.des.beans.NameValueBean;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;


/**
 * Title: DurationUnitsDatabase
 * Description: Contains methods for operations on the durationunits table in the database
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author Anders Aas Hanssen
 * @version 1.0
 *
 */
public class DurationUnitsDatabase extends DataObject {
    private static final String ALL_DURATIONUNITS_QUERY = "select * from durationunits;";
    private static final String INSERT_DURATIONUNITS_QUERY = "insert into durationunits (duration_name) values (?);";
    private static final String CHECK_DEPENDENCY_QUERY = "select count(*) from study where study_duration_unit=?;";
    private static final String DELETE_DURATIONUNIT_QUERY = "delete from durationunits where duration_id=?;";
    private static final String RENAME_DURATIONUNIT_QUERY = "update durationunits set duration_name = ? where duration_id=?;";
    private static final String GET_DURATIONUNIT_QUERY = "select * from durationunits where duration_id=?;";
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    /**
    * Empty constructor
    */
    public DurationUnitsDatabase() {
    }

    /**
    * Returns all duration units
    * @return an ArrayList with all duration units bundled into NameValueBeans
    */
    public ArrayList getDurationUnits() throws Exception {
        ResultSet rs = null;

        try {
            ArrayList units = new ArrayList();
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(this.ALL_DURATIONUNITS_QUERY);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                NameValueBean bean = new NameValueBean();
                bean.setValue(Integer.toString(rs.getInt(1)));
                bean.setName(rs.getString(2));
                units.add(bean);
            }

            return units;
        } catch (Exception e) {
            log.error("Error reading duration units", e);
            throw e;
        } finally {
            closeConnection(rs);
        }
    }

    /**
    * Inserts a new duration unit into the database
    * @param name The name of the new duration unit
    * @return the id if the new duration unit
    */
    public int insertDurationUnit(String name) throws Exception {
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(this.INSERT_DURATIONUNITS_QUERY);
            pstmt.setString(1, name);

            int count = pstmt.executeUpdate();

            if (count > 0) {
                rs = pstmt.executeQuery("SELECT LAST_INSERT_ID();");
                rs.next();

                return rs.getInt(1);
            } else {
                return -1;
            }
        } catch (Exception ex) {
            log.error("Error inserting new duration unit", ex);
            throw ex;
        } finally {
            closeConnection(rs);
        }
    }

    /**
    * Deletes a duration unit.
    * If the duration unit is used in any of the studies the duration unit can not be deleted.
    * @return true if the duration unit was deleted, fale otherwise.
    */
    public boolean deleteDurationUnit(int id) throws Exception {
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(this.CHECK_DEPENDENCY_QUERY);
            pstmt.setString(1, Integer.toString(id));
            rs = pstmt.executeQuery();
            rs.next();

            int count = rs.getInt(1);

            if (count > 0) {
                return false;
            } else {
                pstmt = conn.prepareStatement(this.DELETE_DURATIONUNIT_QUERY);
                pstmt.setString(1, Integer.toString(id));
                count = pstmt.executeUpdate();

                return true;
            }
        } catch (Exception ex) {
            log.error("Error deleting duration unit", ex);
            throw ex;
        } finally {
            closeConnection(rs);
        }
    }

    /**
    * Renames the name of a duration unit
    * @param newDurationUnitName The new name of the duration unit
    * @param id The id of the duration unit
    * @return true if the update was successfull, false otherwise
    */
    public boolean renameDurationUnit(String newDurationUnitName, int id)
        throws Exception {
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(this.RENAME_DURATIONUNIT_QUERY);
            pstmt.setString(1, newDurationUnitName);
            pstmt.setInt(2, id);

            int count = pstmt.executeUpdate();

            if (count == 0) {
                log.error("No duration unit with the id " + id +
                    " where found");

                return false;
            }

            return true;
        } catch (Exception ex) {
            log.error("Error renaming duration unit", ex);
            throw ex;
        } finally {
            closeConnection(rs);
        }
    }

    /**
    * Get the name of a duration unit with a give id
    * @param id The id of the duration unit
    * @return The name of the duration unit
    */
    public String getDurationUnit(int id) throws Exception {
        ResultSet rs = null;
        String ret = "";

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(GET_DURATIONUNIT_QUERY);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                ret = rs.getString(2);

                return ret;
            } else {
                log.error("No duration unit found with id: " + id);
                throw new Exception("No duration unit found with id: " + id);
            }
        } catch (Exception ex) {
            log.error("Error getting duration unit with id: " + id, ex);
            throw ex;
        } finally {
            closeConnection(rs);
        }
    }
}
