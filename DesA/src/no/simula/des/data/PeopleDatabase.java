package no.simula.des.data;

import no.simula.des.data.AdminPrivilegesDatabase;
import no.simula.des.data.beans.AdminPrivilegesBean;
import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.PeoplesBean;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;


/**
 * Handles retrival of all people related data. The "people table" itself is not modified,
 * only relations between it and other tables.
 *
 */
public class PeopleDatabase extends DataObject {
    private final static String DELETE_RESPONSIBLE =
        "DELETE FROM responsiblesstudies " + "WHERE  (responsible_id=?) AND " +
        "       (study_id=?);";
    private final static String ADD_RESPONSIBLE =
        "INSERT IGNORE INTO responsiblesstudies " + "VALUES (?, ?);";
    private static final String GET_ALL_PEOPLES_QUERY = "select * from "+Constants.SIMULAWEB_DB+".people  order by people_family_name ASC ";
    private static final String AUTHENTICATE_USER_QUERY = " select * from "+Constants.SIMULAWEB_DB+".people where people_email= ? AND people_password= AES_ENCRYPT(?, ?);";
    private static final String GET_STUDY_RESPONSIBLES_QUERY = "select p.* from "+Constants.SIMULAWEB_DB+".people p, responsiblesstudies r where p.people_id = r.responsible_id and r.study_id = ?;";

    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    /**
     * The method adds and removed relationships between responsibles and studies at the
     * database level.
     *
     * @param studyId identifies which study we are changing the relationships for.
     * @param publications all responsibles related to the study, including any added or deleted
     *                     ones.
     * @throws Exception
     */
    public void updateResponsibleStudyRelationship(int studyId,
        Collection responsibles) throws Exception {
        if(responsibles == null) return;
        Iterator iterator = responsibles.iterator();

        //Discover any changes
        while (iterator.hasNext()) {
            PeopleBean bean = (PeopleBean) iterator.next();

            if (bean.getDeleted()) {
                //Delete publication from study
                deleteResponsibleFromStudy(studyId, bean.getId());
            } else if (bean.getAdded()) {
                //Add publication to study
                addResponsibleToStudy(studyId, bean.getId());
            }
        }
    }

    /**
     * Retrieves all people from the database
     *
     * @return a PeoplesBean object containing a list of all persons
     * @throws Exception
     */
    public PeoplesBean getPeoples() throws Exception {
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement stmt = conn.prepareStatement(GET_ALL_PEOPLES_QUERY);
            rs = stmt.executeQuery();

            return getPeoples(rs);
        } catch (Exception e) {
            throw e;
        } finally {
            this.closeConnection(rs);
        }
    }

    /**
     * Autheticates a user.
     * Returns null if the user is not authenticated
     *
     * @param String username - (e-mail adress)
     * @param String password - (in clear text)
     * @return PeopleBean, null if user not authenticated
     * @throws Exception if SQL error
     */
    public PeopleBean authentiateUser(String username, String password)
        throws Exception {
        if (username.equals("") || password.equals("")) {
            throw new Exception(
                "The username and/or password can not be empty ");
        }

        ResultSet rs = null;

        try {
            PeopleBean people = new PeopleBean();
            Connection conn = getConnection();
            PreparedStatement stmt = conn.prepareStatement(AUTHENTICATE_USER_QUERY);
            stmt.setString(1, username);
            stmt.setString(2, password);
            stmt.setString(3, "7$");
            System.out.println(password);
            rs = stmt.executeQuery();

            int i = 0;

            while (rs.next()) {
                people = this.createPeopleBean(rs);

                AdminPrivilegesDatabase adminPrivilegeDb = new AdminPrivilegesDatabase();
                int priv = adminPrivilegeDb.getPeoplePrivilege(people.getId());
                people.setPrivilege(priv);

                //people.setId( rs.getInt(1) );
                //people.setFirst_name( rs.getString( 2 ) );
                //people.setFamily_name( rs.getString( 3 ) );
                //people.setPosition( rs.getString( 6 ) );
                i++;
            }

            if (i == 0) {
                return null;
            } else {
                return people;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.closeConnection(rs);
        }
    }

    /**
     * Retrives all persons listed as responsibles for a given study
     *
     * @param studyId ID of the study
     * @return a Collection of PeopleBean objects. An empty Collection if no responsibles exist
     */
    public Collection getResponsibleList(int studyId) throws Exception {
        ArrayList peoples = new ArrayList();
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement stmt = conn.prepareStatement(PeopleDatabase.GET_STUDY_RESPONSIBLES_QUERY);
            stmt.setInt(1, studyId);
            rs = stmt.executeQuery();

            while (rs.next()) {
                PeopleBean people = this.createPeopleBean(rs);
                peoples.add(people);
            }

            return peoples;
        } catch (Exception ex) {
            log.error("Error retrieving study responsibles from study: " +
                studyId, ex);
            throw ex;
        } finally {
            closeConnection(rs);
        }
    }

    /**
     * This method relates a responsible to a study
     *
     * @param studyId
     * @param publicationId
     */
    void addResponsibleToStudy(int studyId, int responsibleId)
        throws Exception {
        //TODO Can we assume that relation is not already existing?
        //If the addPublication action handles this, no additional check (SELECT) is needed here
        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(ADD_RESPONSIBLE);
            pstmt.setInt(1, responsibleId);
            pstmt.setInt(2, studyId);

            int addCount = pstmt.executeUpdate();

            if (log.isDebugEnabled()) {
                log.debug(addCount + " responsibles added to study " + studyId);
            }
            /*
            if (addCount != 1) {
                throw new Exception("Responsible (id=" + responsibleId +
                    ") was not deleted from study id " + studyId);
            }
            */
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            closeConnection(null);
        }
    }

    /**
     * This method removes the relationship between a study and a responsible in the DES database
     *
     * @param studyId
     * @param publicationId
     */
    void deleteResponsibleFromStudy(int studyId, int responsibleId)
        throws Exception {
        //Delete all matching entries in publicationsstudies...
        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(DELETE_RESPONSIBLE);
            pstmt.setInt(1, responsibleId);
            pstmt.setInt(2, studyId);

            int delCount = pstmt.executeUpdate();

            if (log.isDebugEnabled()) {
                log.debug(delCount + " responsibles deleted from study " +
                    studyId);
            }
            /*
            if (delCount != 1) {
                throw new Exception("Responsible (id=" + responsibleId +
                    ") was not deleted from study id " + studyId);
            }
            */
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            closeConnection(null);
        }
    }

    //--------------- Private helper methods ----------------
    private PeopleBean createPeopleBean(ResultSet rs) throws SQLException {
        PeopleBean people = new PeopleBean();
        people.setId(rs.getInt(1));
        people.setFirst_name(rs.getString(2));
        people.setFamily_name(rs.getString(3));
        people.setPosition(rs.getString(6));

        return people;
    }

    private PeoplesBean getPeoples(ResultSet rs) throws Exception {
        PeoplesBean peoples = new PeoplesBean();
        ArrayList tmp = new ArrayList();

        while (rs.next()) {
            PeopleBean people = new PeopleBean();
            people.setId(rs.getInt(1));
            people.setFirst_name(rs.getString(2));
            people.setFamily_name(rs.getString(3));
            people.setPosition(rs.getString(6));

            tmp.add(people);
        }

        peoples.setPeoples(tmp);

        AdminPrivilegesDatabase privilegesDB = new AdminPrivilegesDatabase();
        AdminPrivilegesBean priviileges = privilegesDB.getAdminPrivileges();
        peoples.setPrivileges(priviileges);

        return peoples;
    }
}
