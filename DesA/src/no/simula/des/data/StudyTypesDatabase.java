package no.simula.des.data;

import no.simula.des.beans.NameValueBean;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;


/**
 * Title: StudyTypesDatabase
 * Description: Contains methods for operations on the studytypes table in the database
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author Anders Aas Hanssen
 * @version 1.0
 *
 */
public class StudyTypesDatabase extends DataObject {
    private static final String ALL_STUDYTYPES_QUERY = "select * from studytypes;";
    private static final String INSERT_STUDYTYPE_QUERY = "insert into studytypes (type_name) values (?);";
    private static final String CHECK_DEPENDENCY_QUERY = "select count(*) from study where study_type=?";
    private static final String DELETE_STUDYTYPE_QUERY = "delete from studytypes where type_id=?;";
    private static final String RENAME_STUDYTYPE_QUERY = "update studytypes set type_name = ? where type_id=?;";
    private static final String GET_STUDYTYPE_QUERY = "select * from studytypes where type_id=?;";
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    /**
    * Empty constructor
    */
    public StudyTypesDatabase() {
    }

    /**
    * Returns all studytypes
    * @return All an ArrayList with all study types bundled into NameValueBeans
    */
    public ArrayList getStudyTypes() throws Exception {
        ResultSet rs = null;

        try {
            ArrayList types = new ArrayList();
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(ALL_STUDYTYPES_QUERY);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                NameValueBean bean = new NameValueBean();
                bean.setValue(Integer.toString(rs.getInt(1)));
                bean.setName(rs.getString(2));
                types.add(bean);
            }

            return types;
        } catch (Exception e) {
            log.error("Error reading study types", e);
            throw e;
        } finally {
            closeConnection(rs);
        }
    }

    /**
    * Inserts a new study type into the database
    * @param name The name of the new study types
    * @return the id if the new studytype
    */
    public int insertStudyType(String name) throws Exception {
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(this.INSERT_STUDYTYPE_QUERY);
            pstmt.setString(1, name);

            int count = pstmt.executeUpdate();

            if (count > 0) {
                rs = pstmt.executeQuery("SELECT LAST_INSERT_ID();");
                rs.next();

                return rs.getInt(1);
            } else {
                return -1;
            }
        } catch (Exception ex) {
            log.error("Error inserting new studytype", ex);
            throw ex;
        } finally {
            closeConnection(rs);
        }
    }

    /**
    * Deletes a studytype.
    * If the studytype is used in any of the studies the studytype can not be deleted.
    * @return true if the studytype was deleted, fale otherwise.
    */
    public boolean deleteStudyType(int id) throws Exception {
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(this.CHECK_DEPENDENCY_QUERY);
            pstmt.setString(1, Integer.toString(id));
            rs = pstmt.executeQuery();
            rs.next();

            int count = rs.getInt(1);

            if (count > 0) {
                return false;
            } else {
                pstmt = conn.prepareStatement(this.DELETE_STUDYTYPE_QUERY);
                pstmt.setString(1, Integer.toString(id));
                count = pstmt.executeUpdate();

                return true;
            }
        } catch (Exception ex) {
            log.error("Error deleting studytype", ex);
            throw ex;
        } finally {
            closeConnection(rs);
        }
    }

    /**
    * Renames the name of the studytype
    * @param newStudyTypeName The new name of the studytype
    * @param id The id of the studytype
    * @return true if the update was successfull, false otherwise
    */
    public boolean renameStudyType(String newStudyTypeName, int id)
        throws Exception {
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(this.RENAME_STUDYTYPE_QUERY);
            pstmt.setString(1, newStudyTypeName);
            pstmt.setInt(2, id);

            int count = pstmt.executeUpdate();

            if (count == 0) {
                log.error("No studytype with that id where found");

                return false;
            }

            return true;
        } catch (Exception ex) {
            log.error("Error renaming studytype", ex);
            throw ex;
        } finally {
            closeConnection(rs);
        }
    }

    /**
   * Get the name of a studytype with a give id
   * @param id The id of the studytype
   * @return The name of the studytype
   */
    public String getStudyType(int id) throws Exception {
        ResultSet rs = null;
        String ret = "";

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(this.GET_STUDYTYPE_QUERY);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                ret = rs.getString(2);

                return ret;
            } else {
                log.error("No studytype found with id: " + id);
                throw new Exception("No studytype found with id: " + id);
            }
        } catch (Exception ex) {
            log.error("Error getting studytype with id: " + id, ex);
            throw ex;
        } finally {
            closeConnection(rs);
        }
    }
}
