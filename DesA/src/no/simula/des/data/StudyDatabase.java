/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.data;

import no.simula.des.beans.NameValueBean;
import no.simula.des.beans.StudySortBean;
import no.simula.des.data.PeopleDatabase;
import no.simula.des.data.PublicationDatabase;
import no.simula.des.data.StudyMaterialDatabase;
import no.simula.des.data.beans.StudiesBean;
import no.simula.des.data.beans.StudyBean;
import no.simula.des.data.beans.StudyMaterialBean;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Calendar;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.MessageFormat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;



/**
 * The class handles all database access related to study lists
 */
public class StudyDatabase extends DataObject {
    //Constants
    private final static int ALL_STUDIES = Integer.MAX_VALUE;

    //Prepared statements
    private final static String CHECK_STUDY_NAME_QUERY = "select count(*) from study where study_name=? and study_id != ?;";


    private final static String GET_ALL_STUDY_IDS_QUEREY = "select study_id from study  ORDER BY study_end_date;";
    private final static String SINGLE_STUDY_QUERY = "SELECT study.*, " +
        "       creator.people_id, " +
        "       creator.people_first_name as creator_first_name, " +
        "       creator.people_family_name as creator_family_name, " +
        "       editor.people_id, " +
        "       editor.people_first_name as editor_first_name, " +
        "       editor.people_family_name as editor_family_name, " +
        "       type_name " + "FROM   study, studytypes, " +
        Constants.SIMULAWEB_DB + ".people creator, " + Constants.SIMULAWEB_DB +
        ".people editor " + "WHERE  (study_type = studytypes.type_id) AND " +
        "       (study_id = ?) AND " +
        "       (creator.people_id = study_created_by) AND " +
        "       (editor.people_id = study_edited_by);";
    private final static MessageFormat STUDY_QUERY = new MessageFormat(
            "SELECT study.study_id as study_id, " +
            "       study.study_name as study_name, " +
            "       study.study_description as study_description, " +
            "       study.study_end_date as study_end_date, " +
            "       studytypes.type_name as type_name " +
            "FROM   study, studytypes " +
            "WHERE  (study_type = studytypes.type_id) " + "ORDER BY {0} {1};");
            //"WHERE  (study_type_id = studytypes.type_id) " + "ORDER BY {0} {1};");


      private static MessageFormat STUDY_SEARCH_QUERY = new MessageFormat(
      "select distinct  s.study_id, s.study_name, s.study_description, s.study_end_date, st.type_name " +
      "from study s " +
      "LEFT JOIN study s1  ON s1.study_name REGEXP ? or s1.study_keywords REGEXP ? or s1.study_notes REGEXP ? or  s1.study_description REGEXP ? " +
      "LEFT JOIN publicationsstudies ps ON s.study_id = ps.study_id AND ( s.study_end_date > ? AND  s.study_end_date < ? ) " +
      "LEFT JOIN "+ Constants.SIMULAWEB_DB +".publication p ON s.study_id = ps.study_id " +
      "AND ps.publication_id = p.publication_id " +
      "AND ( p.publication_abstract regexp ? or p.publication_title regexp ? ) " +
      "LEFT JOIN studytypes st ON s.study_type = st.type_id AND  st.type_name regexp ? " +
      "LEFT JOIN responsiblesstudies rs ON rs.study_id = s.study_id " +
      "LEFT JOIN "+ Constants.SIMULAWEB_DB +".people pe ON rs.responsible_id = pe.people_id AND ( pe.people_first_name regexp ? AND pe.people_family_name regexp ? )" +
      "WHERE s1.study_id = s.study_id " +
      "AND s.study_type = st.type_id " +
      "AND pe.people_id IS NOT NULL " +
      "AND ( s.study_end_date > ? AND  s.study_end_date < ? ) " +
      "or ( " +
      "s.study_id = ps.study_id " +
      "AND ps.publication_id = p.publication_id " +
      "AND pe.people_id IS NOT NULL " +
      "AND s.study_type = st.type_id ) ORDER BY {0} {1};");

    /*private static MessageFormat STUDY_SEARCH_QUERY = new MessageFormat(
      "select distinct  s.study_id, s.study_name, s.study_description, s.study_end_date, st.type_name " +
      "from study s " +
      "LEFT JOIN study s1  ON s1.study_name REGEXP ? or s1.study_keywords REGEXP ? or s1.study_notes REGEXP ? or  s1.study_description REGEXP ? " +
      "LEFT JOIN publicationsstudies ps ON s.study_id = ps.study_id " +
      "LEFT JOIN desmain.publication p ON s.study_id = ps.study_id " +
      "AND ps.publication_id = p.publication_id " +
      "AND publication_abstract regexp ? " +
      "LEFT JOIN studytypes st ON s.study_type = st.type_id AND  st.type_id = ? " +
      "LEFT JOIN responsiblesstudies rs ON rs.study_id = s.study_id " +
      "LEFT JOIN desmain.people pe ON rs.responsible_id = pe.people_id AND pe.people_first_name regexp ? AND pe.people_family_name = ? " +
      "WHERE s1.study_id = s.study_id " +
      "AND s.study_type = ? " +
      "AND pe.people_id IS NOT NULL " +
      "AND ( s.study_end_date > ? AND  s.study_end_date < ? ) " +
      "or ( " +
      "s.study_id = ps.study_id " +
      "AND ps.publication_id = p.publication_id " +
      "AND pe.people_id IS NOT NULL);"); */
            /*"SELECT DISTINCT study.study_id as study_id, " +
            "       study.study_name as study_name, " +
            "       study.study_description as study_description, " +
            "       study.study_end_date as study_end_date, " +
            "       studytypes.type_name as type_name " +
            "FROM   study, studytypes {0}" +
            "WHERE   (study_type = studytypes.type_id) " +
            "AND    (study.study_name REGEXP ? OR " +
            "       study.study_description REGEXP ? )" +
            "AND    (studytypes.type_name REGEXP ?)" + "{1}" +
            "AND    study.study_end_date REGEXP ? ORDER BY {2} {3};") */;
    private final static String STUDY_TYPES_QUERY = "SELECT * FROM studytypes;";

    private final static String DURATION_UNITS_QUERY = "SELECT * FROM durationunits;";

    private final static String STUDY_INSERT = "INSERT INTO study (" +
        "       study_name, " + "       study_type, " +
        "       study_description, " + "       study_keywords, " +
        "       study_notes, " + "       study_students, " +
        "       study_professionals, " + "       study_duration, " +
        "       study_duration_unit, " + "       study_start_date, " +
        "       study_end_date, " + "       study_created_by, " +
        "       study_created_date, " + "       study_edited_by, " +
        "       study_edited_date) " +
        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

    private final static String STUDY_UPDATE = "UPDATE study SET " +
        "       study_name=?, " + "       study_type=?, " +
        "       study_description=?, " + "       study_keywords=?, " +
        "       study_notes=?, " + "       study_students=?, " +
        "       study_professionals=?, " + "       study_duration=?, " +
        "       study_duration_unit=?, " + "       study_start_date=?, " +
        "       study_end_date=?, " + "       study_edited_by=?, " +
        "       study_edited_date=? " + "WHERE 	study_id=?;";

    public static final String DELETE_STUDY_QUERY = "DELETE FROM study WHERE study.study_id = ?;";

    public static final String DELETE_STUDY_MATERIAL_QUERY = "DELETE FROM studymaterial WHERE studymaterial.study_id = ?;";

    public static final String DELETE_PUBLICATION_RELATION_QUERY = "DELETE FROM publicationsstudies WHERE publicationsstudies.study_id=?;";

    public static final String DELETE_RESPONSIBLE_RELATION_QUERY = "DELETE FROM responsiblesstudies WHERE responsiblesstudies.study_id=?;";

    /*    private String DYNAMIC_QUERY_RESPONISBLE =
            "AND (responsiblesstudies.study_id = study.study_id " +
            "AND responsiblesstudies.responsible_id = people.people_id " +
            "AND ( people.people_family_name REGEXP ? " +
            "OR people.people_first_name  REGEXP ? ) ) ";
    */

    private static final String STUDY_END_DATES_QUERY ="select distinct(study_end_date) from study;";

    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    /**
     * Retrieves a single study from the database
     *
     * @param id the id of the study to retrieve
     * @return the StudyBean object representing the study
     * @throws Exception
     */
    public StudyBean getStudy(int id, Connection conn)
        throws Exception {
        ResultSet rs = null;
        PreparedStatement pstmt = null;

        try {
            //Connection conn = getConnection();
            pstmt = conn.prepareStatement(SINGLE_STUDY_QUERY);
            pstmt.setInt(1, id);

            rs = pstmt.executeQuery();
            rs.first();

            StudyBean study = populateStudyBeanComplete(rs);

            pstmt = conn.prepareStatement(STUDY_TYPES_QUERY);
            rs = pstmt.executeQuery();

            study.setStudyTypeOptions(getStudyTypes());

            return study;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                }

                // ignore
                rs = null;
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException sqlEx) {
                }

                // ignore
                pstmt = null;
            }
        }
    }

    /**
     * Retrieves study data for a single study
     *
     * @param id identifies the study
     * @return a StudyBean containing the study data
     * @throws Exception
     */
    public StudyBean getStudy(int id) throws Exception {
        try {
            Connection conn = getConnection();

            return getStudy(id, conn);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            closeConnection(null);
        }
    }

    /**
     * Retrieves a number of studies from the study table
     *
     * @param start row number to start on
     * @param count number of studies to retrieve
     * @return a Collection of StudyBean objects
     */
    public StudiesBean getStudies(StudySortBean sortBean)
        throws Exception {
        int start = sortBean.getStart();
        int count = sortBean.getCount();
        String orderBy = sortBean.getOrderBy();
        String orderDirection = sortBean.getOrderDirection();

        StudiesBean studies = null;
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(STUDY_QUERY.format(
                        new Object[] { orderBy, orderDirection }));
            rs = pstmt.executeQuery();

            //Iterate through resultset and instantiate StudyBean objects
            //Start at first hit and return all hits
            studies = getStudyBeans(rs, start, count);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            super.closeConnection(rs);
        }

        return studies;
    }

    /**
     * Performs an advanced search
     *
     * @param StudySortBean containg the sort and search information
     * @return a StudiesBean object containing all the studies that matched the query
     */
    public StudiesBean advancedSearchStudies(StudySortBean sortBean)
        throws Exception {
        int start = sortBean.getStart();
        int count = sortBean.getCount();
        String orderBy = sortBean.getOrderBy();
        String orderDirection = sortBean.getOrderDirection();
        String searchString = sortBean.getSearchString();
        String studyType = sortBean.getStudyType();
        String startYear = sortBean.getStartYear();
        String endYear = sortBean.getEndYear();
        String firstName = sortBean.getResponsible_firstname();
        String familyName = sortBean.getResponsible_familyname();

        if(!endYear.equals("") && !startYear.equals("")) {
          int start_dt = Integer.parseInt(startYear);
          int end_dt = Integer.parseInt(endYear);
          if( start_dt > end_dt ){
            endYear = String.valueOf(start_dt);
            startYear = String.valueOf(end_dt);
          }
        }

        if (endYear.equals("")) {
            endYear = "3000";
        }

        if (startYear.equals("")) {
            startYear = "0";
        }


        Calendar calStartDate = Calendar.getInstance();
        calStartDate.set( Integer.parseInt( startYear ), 0, 1);

        Calendar calEndDate = Calendar.getInstance();
        calEndDate.set( Integer.parseInt( endYear ) +1 , 0, 1);



        log.debug("searcgString:" + searchString);
        log.debug("studyType:" + studyType);
        log.debug("startYear:" + startYear);
        log.debug("endYear:" + endYear);
        log.debug("responsible:" + sortBean.getResponsible_firstname() +
          " " + sortBean.getResponsible_familyname() );

        //String responsible = sortBean.getResponsible();

        if ( firstName  == null) {
            firstName = "";
         }
         if( familyName == null ) {
            familyName = "";
        }

      if( studyType == null || studyType.equals("") ){
        studyType =".*";
      }

        //STUDY_SEARCH_QUERY.getFormats();
        ResultSet rs = null;
        StudiesBean studies = null;
        String orderByColumn ="";
        if( sortBean.getOrderBy().equals("study_type") ){
          orderByColumn = "s.study_type";
        }
        else{
          orderByColumn = sortBean.getOrderBy();
        }

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(STUDY_SEARCH_QUERY.format(
                        new Object[] {
                            orderByColumn,
                            sortBean.getOrderDirection()
                        }));
            int i = 1;
            pstmt.setString(i++, ".*" + searchString + ".*"); //1
            pstmt.setString(i++, ".*" + searchString + ".*"); //2
            pstmt.setString(i++, ".*" + searchString + ".*"); //3
            pstmt.setString(i++, ".*" + searchString + ".*"); //4
            pstmt.setDate(i++, new java.sql.Date( calStartDate.getTime().getTime() ) ); //4
            pstmt.setDate(i++, new java.sql.Date( calEndDate.getTime().getTime() )  ); //5
            pstmt.setString(i++, ".*" + searchString + ".*"); //6
            pstmt.setString(i++, ".*" + searchString + ".*"); //7
            //pstmt.setString(i++, ".*" + searchString + ".*"); //8

            pstmt.setString(i++, studyType ); //9


            pstmt.setString(i++, ".*" + firstName + ".*"); //10
            pstmt.setString(i++, ".*" +  familyName + ".*"); //11




            pstmt.setDate(i++, new java.sql.Date( calStartDate.getTime().getTime() ) ); //12
            pstmt.setDate(i++, new java.sql.Date( calEndDate.getTime().getTime() )  ); //13


            //pstmt.setString(i++, orderBy);
            //pstmt.setString(i++, orderDirection);
            //System.out.println("Search sql: " + pstmt.toString());

            rs = pstmt.executeQuery();



            //Iterate through resultset and instantiate StudyBean objects
            //Start at first hit and return all hits
            studies = getStudyBeans(rs, start, count);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            super.closeConnection(rs);
        }

        return studies;
    }

    /**
     * Updates an existing study
     *
     * @param bean contains all data related to the study
     * @throws Exception
     */
    public void updateStudy(StudyBean bean) throws Exception {
        ResultSet rs = null;

        try {
            //Perform any updates in publications and responsibles
            Collection pubs = bean.getPublications();

            if ( pubs != null && pubs.size() > 0) {
                PublicationDatabase pubDb = new PublicationDatabase();
                pubDb.updatePublicationStudyRelationship(bean.getId(), pubs);
            }

            Collection responsibles = bean.getResponsibles();

            if (responsibles != null && responsibles.size() > 0) {
                PeopleDatabase peopleDb = new PeopleDatabase();
                peopleDb.updateResponsibleStudyRelationship(bean.getId(),
                    responsibles);
            }

            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(STUDY_UPDATE);

            pstmt.setString(1, bean.getName());

            //System.out.println( "Type = " + bean.getType());
            pstmt.setInt(2, Integer.parseInt(bean.getType()));
            pstmt.setString(3, bean.getDescription());
            pstmt.setString(4, bean.getKeywords());
            pstmt.setString(5, bean.getNotes());
            pstmt.setInt(6, bean.getStudents());
            pstmt.setInt(7, bean.getProfessionals());
            pstmt.setInt(8, bean.getDuration());
            pstmt.setInt(9, bean.getDurationUnit());
            if( bean.getStartDate() != null){
              pstmt.setDate(10, new Date(bean.getStartDate().getTime()));
            }
            else{
              pstmt.setNull(10, java.sql.Types.DATE );
            }
            pstmt.setDate(11, new Date(bean.getEndDate().getTime()));
            pstmt.setInt(12, bean.getEditorId());
            pstmt.setDate(13, new Date(new java.util.Date().getTime()));
            pstmt.setInt(14, bean.getId());

            int updates = pstmt.executeUpdate();

            if (updates != 1) {
                throw new Exception("The study was not updated (" +
                    bean.getId() + ", " + bean.getName() + ")");
            }

            PeopleDatabase peopleDao = new PeopleDatabase();
            peopleDao.updateResponsibleStudyRelationship(bean.getId(),
                bean.getResponsibles());

            PublicationDatabase pubDao = new PublicationDatabase();
            pubDao.updatePublicationStudyRelationship(bean.getId(),
                bean.getPublications());

            ArrayList material = bean.getStudyMaterial();
            StudyMaterialDatabase materialDb = new StudyMaterialDatabase();
            Iterator iter = material.iterator();

            while (iter.hasNext()) {
                StudyMaterialBean item = (StudyMaterialBean) iter.next();

                if (item.getAdded()) {
                    item.setStudy_id(bean.getId() );
                    materialDb.insertStudyMateriel(item);
                }
                else if( item.getDeleted() && item.getStudy_material_id() != -1  ){
                  materialDb.deleteStudyMaterial( item.getStudy_material_id() );
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new Exception(e.toString());
        } finally {
            closeConnection(null);
        }
    }

    /**
     * Creates a new study entry
     *
     * @param bean contains all data related to the study
     * @return the ID of the inserted study
     */
    public synchronized int insertStudy(StudyBean bean)
        throws Exception {
        ResultSet rs = null;
        int id = 0;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(STUDY_INSERT);
            pstmt.setString(1, bean.getName());

            //System.out.println( "Type = " + bean.getType());
            pstmt.setInt(2, Integer.parseInt(bean.getType()));
            pstmt.setString(3, bean.getDescription());
            pstmt.setString(4, bean.getKeywords());
            pstmt.setString(5, bean.getNotes());
            pstmt.setInt(6, bean.getStudents());
            pstmt.setInt(7, bean.getProfessionals());
            pstmt.setInt(8, bean.getDuration());
            pstmt.setInt(9, bean.getDurationUnit());
            if( bean.getStartDate() != null){
              pstmt.setDate(10, new Date(bean.getStartDate().getTime()));
            }
            else{
              pstmt.setNull(10, java.sql.Types.DATE );
            }
            pstmt.setDate(11, new Date(bean.getEndDate().getTime()));
            pstmt.setInt(12, bean.getEditorId());
            pstmt.setDate(13, new Date(new java.util.Date().getTime()));
            pstmt.setInt(14, bean.getEditorId());
            pstmt.setDate(15, new Date(new java.util.Date().getTime()));

            int updates = pstmt.executeUpdate();

            rs = pstmt.executeQuery("SELECT LAST_INSERT_ID();");

            while (rs.next()) {
                id = rs.getInt(1);
                log.debug("ID of the new study:" + id);
            }

            if (updates == 0) {
                throw new Exception("A new study was not added (" +
                    bean.getName() + ")");
            }

            PeopleDatabase peopleDao = new PeopleDatabase();
            peopleDao.updateResponsibleStudyRelationship(id,
                bean.getResponsibles());

            PublicationDatabase pubDao = new PublicationDatabase();
            pubDao.updatePublicationStudyRelationship(id, bean.getPublications());

            ArrayList material = bean.getStudyMaterial();
            StudyMaterialDatabase materialDb = new StudyMaterialDatabase();
            Iterator iter = material.iterator();

            while (iter.hasNext()) {
                StudyMaterialBean item = (StudyMaterialBean) iter.next();

                if (item.getAdded()) {
                    item.setStudy_id(id);
                    materialDb.insertStudyMateriel(item);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new Exception(e.toString());
        } finally {
            closeConnection(rs);
        }

        return id;
    }

    /**
     * The method iterates through a result set and adds instantiated StudyBeans to a collection.
     *
     * @param rs the SQL resultset
     * @param start which row to start on
     * @param count how many studies to retrieve
     * @return a collection of StudyBeans
     * @throws SQLException
     */
    private StudiesBean getStudyBeans(ResultSet rs, int start, int count)
        throws SQLException {
        StudiesBean studies = new StudiesBean();
        ArrayList beans = new ArrayList();

        rs.last();

        int total = rs.getRow();
        studies.setTotalStudies(total);

        //Start should always be above zero
        if (start > 1) {
            rs.absolute(start - 1);
            studies.setStartIndex(rs.getRow());
        } else {
            rs.beforeFirst();
            studies.setStartIndex(1);
        }

        int endRow = 0;

        for (int i = 0; rs.next() && (i < count); i++) {
            StudyBean study = populateStudyBeanBasic(rs);
            beans.add(study);
            endRow = rs.getRow();
        }

        studies.setEndIndex(endRow);
        studies.setStudies(beans);

        return studies;
    }

    /**
     * The method instantiates a single StudyBean object from a row in a SQL result set.
     * Only attributes needed for the study list are included
     *
     * @param rs the result set from an SQL query
     * @return a single StudyBean object
     * @throws SQLException
     */
    private StudyBean populateStudyBeanBasic(ResultSet rs)
        throws SQLException {
        StudyBean study = new StudyBean();
        study.setId(rs.getInt(1));
        study.setName(rs.getString(2));
        study.setDescription(rs.getString(3));
        study.setEndDate(rs.getDate(4));
        study.setType(rs.getString(5));

        return study;
    }

    /**
     * The method instantiates a single StudyBean object from a row in a SQL result set
     *
     * @param rs the result set from an SQL query
     * @return a single StudyBean object
     * @throws SQLException
     */
    private StudyBean populateStudyBeanComplete(ResultSet rs)
        throws SQLException {
        StudyBean study = new StudyBean();
        study.setId(rs.getInt("study_id"));

        //Mandatory attributes
        study.setName(rs.getString("study_name"));
        study.setDescription(rs.getString("study_description"));
        study.setEndDate(rs.getDate("study_end_date"));
        study.setType(rs.getString("type_name"));

        //Optional attributes
        study.setKeywords(rs.getString("study_keywords"));
        study.setNotes(rs.getString("study_notes"));
        study.setStudents(rs.getInt("study_students"));
        study.setProfessionals(rs.getInt("study_professionals"));

        //TODO Make changes to duration unit?
        study.setDuration(rs.getInt("study_duration"));
        study.setDurationUnit(rs.getInt("study_duration_unit"));

        study.setStartDate(rs.getDate("study_start_date"));
        study.setEndDate(rs.getDate("study_end_date"));

        study.setCreatedBy(rs.getString("creator_first_name") + " " +
            rs.getString("creator_family_name"));
        study.setCreatedDate(rs.getDate("study_created_date"));

        study.setEditedBy(rs.getString("editor_first_name") + " " +
            rs.getString("editor_family_name"));
        study.setEditedDate(rs.getDate("study_edited_date"));

        return study;
    }

    /**
     * Retrieves study type ids and names from the database
     *
     * @param rs query results
     * @return an ArrayList containing the found study types
     * @throws SQLException
     */
    public ArrayList getStudyTypes() throws Exception {
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(STUDY_TYPES_QUERY);
            rs = pstmt.executeQuery();

            ArrayList types = new ArrayList();

            while (rs.next()) {
                NameValueBean bean = new NameValueBean();
                bean.setName(rs.getString("type_name"));
                bean.setValue(rs.getString("type_id"));
                types.add(bean);
            }

            return types;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage());
        } finally {
            closeConnection(rs);
        }
    }

    /**
     * Retrieves duration unit ids and names from the database
     *
     * @param rs query results
     * @return an ArrayList containing the found duration units
     * @throws SQLException
     */
    public ArrayList getDurationUnits() throws Exception {
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(DURATION_UNITS_QUERY);
            rs = pstmt.executeQuery();

            ArrayList types = new ArrayList();

            while (rs.next()) {
                NameValueBean bean = new NameValueBean();
                bean.setName(rs.getString("duration_name"));
                bean.setValue(rs.getString("duration_id"));
                types.add(bean);
            }

            return types;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage());
        } finally {
            closeConnection(rs);
        }
    }

    /**
     * Deletes a single study
     *
     * @param studyId the study to delete
     * @throws Exception
     */
    public void deleteStudy(int studyId) throws Exception {
        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(StudyDatabase.DELETE_STUDY_QUERY);
            pstmt.setInt(1, studyId);
            pstmt.executeUpdate();
            pstmt.close();

            pstmt = conn.prepareStatement(StudyDatabase.DELETE_PUBLICATION_RELATION_QUERY);
            pstmt.setInt(1, studyId);
            pstmt.executeUpdate();
            pstmt.close();

            pstmt = conn.prepareStatement(StudyDatabase.DELETE_RESPONSIBLE_RELATION_QUERY);
            pstmt.setInt(1, studyId);
            pstmt.executeUpdate();
            pstmt.close();

            pstmt = conn.prepareStatement(StudyDatabase.DELETE_STUDY_MATERIAL_QUERY);
            pstmt.setInt(1, studyId);
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            log.error("Error deleting study " + studyId, ex);
            throw ex;
        } finally {
            this.closeConnection(null);
        }
    }

    /**
     * Retrieves all registered studies
     *
     * @return a Collection containing StudyBean objects
     * @throws Exception
     *
     * @see no.simula.des.data.beans.StudyBean
     */
    public Collection getAllStudies() throws Exception {
        ArrayList studies = new ArrayList();
        ArrayList studyIds = new ArrayList();
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(StudyDatabase.GET_ALL_STUDY_IDS_QUEREY);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                int studyId = rs.getInt(1);
                studyIds.add(new Integer(studyId));
            }

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                }

                // ignore
                rs = null;
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException sqlEx) {
                }

                // ignore
                pstmt = null;
            }

            Iterator ids = studyIds.iterator();

            while (ids.hasNext()) {
                int studyId = ((Integer) ids.next()).intValue();
                StudyBean study = this.getStudy(studyId, conn);
                studies.add(study);
            }

            return studies;
        } catch (Exception ex) {
            log.error("Error fetching data:", ex);
            throw ex;
        } finally {
            this.closeConnection(rs);
        }
    }

    public boolean isStudyNameValid(String studyName, int id) throws Exception{
      ResultSet rs = null;
      boolean ret = true;
      try {
         Connection conn = getConnection();
         PreparedStatement pstmt = conn.prepareStatement(this.CHECK_STUDY_NAME_QUERY);
         pstmt.setString(1, studyName);
         pstmt.setInt(2, id);
         rs = pstmt.executeQuery();
         if(rs.next()){
            int count = rs.getInt(1);
            if(count > 0 ) ret = false;
         }
         return ret;
      }
      catch (Exception ex) {
        log.error("Error checking validy of studyname",ex);
        throw ex;
      }
      finally{
        closeConnection(rs);
      }
    }

   public ArrayList getStudyEndDates() throws Exception{
    ResultSet rs = null;
    ArrayList dates = new ArrayList();
    try {
      Connection conn = getConnection();
      PreparedStatement pstmt = conn.prepareStatement(STUDY_END_DATES_QUERY);
      rs = pstmt.executeQuery();
      while(  rs.next() ){
        java.util.Date date = rs.getDate(1);
        dates.add(date);
      }
      return dates;
    }
    catch (Exception ex) {
      log.error("Error retrieveing study end dates", ex);
      throw ex;
    }
    finally {
      closeConnection(rs);
    }


   }
}
