create table personalised_report (
	id int not null primary key auto_increment,
	name varchar(250),
	orderByTime int,
	conditionOperator varchar(60),
	user_id varchar(250)
);

create table personalised_report_columns (
	report_id int,
	column_name varchar(250),
	seq int
);

create table personalised_report_filter (
	report_id int,
	value varchar(1000)
);
