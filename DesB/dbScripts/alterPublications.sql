alter table study_publication_rel modify column publication_id varchar(255);

drop table publication;
drop table publication_author;

#drop also these unused tables?

#drop table publication_department;
#drop table publication_trustee;
