alter table study_responsible_rel modify column people_id varchar(255);
alter table person_privilege_rel modify column people_id varchar(255);
alter table stu_study modify column stu_owner varchar(255);
alter table stu_study modify column stu_lasteditedby varchar(255);

drop table people;


