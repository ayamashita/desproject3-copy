package no.halogen.utils.table;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import no.halogen.utils.table.SortableTableColumn;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This is a general sortable table that may be used in any Struts-project.
 *
 * @author  Stian Eide
 */
public class Table {

  // Please note that pages start at 1 externally and 0 internally in this table.
  
  /** Creates a new instance of Table */
  public Table(List columns, TableSource source, int rowsPerPage, SortableTableColumn sortedBy) {
    this.columns = columns;
    this.source = source;
    this.rowsPerPage = rowsPerPage;
    this.sortedBy = sortedBy;
    currentPage = 0;
  }

  private static Log log = LogFactory.getLog(Table.class);  
  private int rowsPerPage;
  private int currentPage;
  private SortableTableColumn sortedBy;
  private List columns;
  private TableSource source;
  
  /** Holds value of property searchType. */
//  protected String searchType;
  
  /** Getter for property columns.
   * @return Value of property columns.
   *
   */
  public List getColumns() {
    return(columns);
  }
    
  /** Getter for property rowsPerPage.
   * @return Value of property rowsPerPage.
   *
   */
  public int getRowsPerPage() {
    return(rowsPerPage);
  }
  
  /** Getter for property sortedBy.
   * @return Value of property sortedBy.
   *
   */
  public SortableTableColumn getSortedBy() {
    return(sortedBy);
  }
  
  public void setSortedBy(SortableTableColumn sortedBy) {
    boolean ascending = true;
    if(this.sortedBy != null && this.sortedBy.equals(sortedBy)) {
      ascending = !this.sortedBy.isAscending();
    }
//    else {
//      this.sortedBy = sortedBy;
//    }
    setSortedBy(sortedBy, ascending);
  }
  
  /** Setter for property sortedBy.
   * @param sortedBy New value of property sortedBy.
   *
   */
  public void setSortedBy(SortableTableColumn sortedBy, boolean isAscending) {
    sortedBy.setAscending(isAscending);
    this.sortedBy = sortedBy;
    doSort();
    try {
      setCurrentPage(1);
    }
    catch(IllegalPageException ipe) {
      log.error("Page sort was unable to set current page to 1.", ipe);
    }
  }
  
  public List getVisibleRows() {
    if(source == null) {
      return(null);
    }
    int firstRow = currentPage * rowsPerPage;
    int lastRow = firstRow + rowsPerPage;
    if(lastRow > source.getContent().size()) {
      lastRow = source.getContent().size();
    }
    if(firstRow > lastRow) {
      firstRow = lastRow;
    }
    if(firstRow < 0) {
      firstRow = 0;
    }
    if(lastRow < 0) {
      lastRow = 0;
    }
    return(source.getContent().subList(firstRow, lastRow));
  }
  
  /** Getter for property currentPage.
   * @return Value of property currentPage.
   *
   */
  public int getCurrentPage() {
    return(currentPage + 1);
  }
  
  /** Setter for property currentPage.
   * @param currentPage New value of property currentPage.
   *
   */
  public void setCurrentPage(int currentPage) throws IllegalPageException {
    if(currentPage < 1 || currentPage > getNumberOfPages()) {
      throw new IllegalPageException("Current page can't be less than 1 or more than the number of pages in table.");
    }
    this.currentPage = currentPage - 1;
  }
  
  public int getNumberOfPages() {
    // Some integer "magic" to get the correct number
    return(((source.getContent().size() - 1) / rowsPerPage) + 1);
  }

  public void sortBy(String sortByName) throws IllegalColumnException {
    sortBy(sortByName, null);
  }
  
  public void sortBy(String sortByName, boolean isAscending) throws IllegalColumnException {
    sortBy(sortByName, Boolean.valueOf(isAscending));
  }  
  
  private void sortBy(String sortByName, Boolean isAscending) throws IllegalColumnException {
    for(Iterator i = columns.iterator(); i.hasNext();) {
      TableColumn column = (TableColumn)i.next();
      if(column.getName().equals(sortByName) && column instanceof SortableTableColumn) {
        if(isAscending != null) {
          setSortedBy((SortableTableColumn)column, isAscending.booleanValue());
        }
        else {
          setSortedBy((SortableTableColumn)column);
        }
        return;
      }
    }
    throw new IllegalColumnException("Column '" + sortByName + "' is not defined in table.");
  }
  
  
  /**
   * Sorts the table based on the sortedBy-column
   */
  public void doSort() {
    Collections.sort(source.getContent(), new ColumnSortComparator(sortedBy.getName(), sortedBy.isAscending()));
  }
    
  /** Getter for property content.
   * @return Value of property content.
   *
   */
  public List getContent() {
    return(source.getContent());
  }
  
  /** Getter for property source.
   * @return Value of property source.
   *
   */
  public TableSource getSource() {
    return(source);
  }
  
  /** Setter for property source.
   * @param source New value of property source.
   *
   */
  public void setSource(TableSource source) {
    this.source = source;
  }
  
}