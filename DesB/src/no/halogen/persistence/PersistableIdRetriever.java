package no.halogen.persistence;

public class PersistableIdRetriever {
	public static Object retrieveId(Persistable o) {
		Object oid;
		if (o instanceof PersistableIntegerId) {
			oid = ((PersistableIntegerId) o).getId();
		}
		else if (o instanceof PersistableStringId) {
			oid = ((PersistableStringId) o).getId();
		}
		else {
			throw new RuntimeException("Unknown class: " + o.getClass());
		}
		return oid;
	}
	public static boolean isIdValid(Object id) {
		if (id == null) {
			return false;
		}
		if (id instanceof Integer) {
			return ((Integer)id).intValue() > 0;
		}
		if (id instanceof String) {
			return "".equals(id);
		}
		else {
			throw new IllegalArgumentException("Unknown ID class: " + id.getClass());
		}
	}
}
