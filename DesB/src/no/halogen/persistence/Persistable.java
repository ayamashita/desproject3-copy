package no.halogen.persistence;

/**
 * Objects that shall be persisted must implement
 * this interface
 *
 * @author  Stian Eide
 */
public interface Persistable {
  
}