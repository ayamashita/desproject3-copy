package no.halogen.presentation.web;

/** Contains constants that are used across the classes in this package.
 * @author Stian Eide
 */
public class Constants {
  
  /** The key under, which the navigation path is stored in <CODE>HttpSession</CODE>. */  
  public static final String NAVIGATION_PATH = "no.halogen.presentation.web.NAVIGATION_PATH";
  /** The key under, which the path of the active action is stored in <CODE>HttpSession</CODE>. */  
  public static final String ACTIVE_ACTION = "no.halogen.presentation.web.ACTIVE_ACTION";
  /** The key under, which an <CODE>List</CODE> of actions that are new-requests
   * is stored in <CODE>HttpSession</CODE>.
   */  
  public static final String IS_NEW = "no.halogen.presentation.web.is-new";

}
