/**
 * @(#) CriteriumFactory.java
 */

package no.halogen.search;


/**
 * Creates a new Criterium
 * 
 * @author Frode Langseth
 */
public interface CriteriumFactory
{
	/**
	 * Method create
	 * @param criterium String
	 * @return Criterium
	 */
	public Criterium create(String criterium);
	
}
