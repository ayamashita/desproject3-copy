package no.simula;

import java.util.ArrayList;
import java.util.List;

import no.halogen.persistence.PersistentObject;
import no.simula.des.persistence.PersistentAdminModule;
import no.simula.des.persistence.PersistentPrivilege;
import no.simula.des.persistence.PersistentStudy;
import no.simula.des.persistence.PersistentStudyMaterial;
import no.simula.des.persistence.PersistentStudyReport;
import no.simula.des.persistence.PersistentStudyType;
import no.simula.persistence.PersistentPerson;
import no.simula.persistence.PersistentPublication;

/** This class creates and returns instances of Search
 * based on the parameters that are passed in
 * <code>properties</code>
 * @author Frode Langseth
 */
public class PersistentFactory {

  /** Creates a new instance of SearchFactory */
  private PersistentFactory() {
  }

  public final static List TYPES;

  static {
    TYPES = new ArrayList(8);
    TYPES.add("AdminModule");
    TYPES.add("Privilege");
    TYPES.add("Study");
    TYPES.add("StudyMaterial");
    TYPES.add("StudyReport");
    TYPES.add("Person");
    TYPES.add("Publication");
    TYPES.add("StudyType");
  }

  public synchronized static PersistentFactory getInstance() {
    if (instance == null) {
      instance = new PersistentFactory();
    }
    return (instance);
  }

  private static PersistentFactory instance;

  public List getTypes() {
    ArrayList types = new ArrayList(8);
    types.add(createSearch("AdminModule"));
		types.add(createSearch("Privilege"));
		types.add(createSearch("Study"));
		types.add(createSearch("StudyMaterial"));
		types.add(createSearch("StudyReport"));
		types.add(createSearch("Person"));
		types.add(createSearch("Publication"));
		types.add(createSearch("StudyType"));

    return (types);
  }

  public PersistentObject createSearch(String type) {
    PersistentObject persistent = null;

    if (type.equals("AdminModule")) {
      persistent = new PersistentAdminModule();
    } else if (type.equals("Privilege")) {
    	persistent = new PersistentPrivilege();
    } else if (type.equals("Study")) {
    	persistent = new PersistentStudy();
    } else if (type.equals("StudyMaterial")) {
    	persistent = new PersistentStudyMaterial();
    } else if (type.equals("StudyReport")) {
    	persistent = new PersistentStudyReport();
    } else if (type.equals("Person")) {
    	persistent = new PersistentPerson();
    } else if (type.equals("Publication")) {
    	persistent = new PersistentPublication();
    } else {
    	persistent = new PersistentStudyType();
    }

    return (persistent);
  }
}