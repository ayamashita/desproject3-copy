package no.simula.utils;

import java.lang.Character;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import no.simula.Person;
import no.simula.Publication;
import no.simula.des.DurationUnit;
import no.simula.des.Study;
import no.simula.des.StudyMaterial;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/** Transforms studies into different formats. Currently supports XML and CSV.
 * @author Stian Eide
 */
public class StudyTransformer {
  
  /** Creates a new instance of StudyTransformer */
  public StudyTransformer() {}

  private static Log log = LogFactory.getLog(StudyTransformer.class);  
  private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", new Locale("en"));
    
  public static final String STUDY = "study";
  public static final String NAME = "name";
  public static final String DESCRIPTION = "description";
  public static final String TYPE = "type";
  public static final String RESPONSIBLES = "responsibles";
  public static final String STUDENT_PARTICIPANTS = "student-participants";
  public static final String PROFESSIONAL_PARTICIPANTS = "professional-participants";
  public static final String DURATION = "duration";
  public static final String DURATION_UNIT = "unit";
  public static final String START = "start";
  public static final String END = "end";
  public static final String KEYWORDS = "keywords";
  public static final String NOTES = "notes";
  public static final String PUBLICATIONS = "publications";
  public static final String STUDYMATERIALS = "studymaterials";

  public static final String PERSON = "person";
  public static final String FIRSTNAME = "first-name";
  public static final String FAMILYNAME = "family-name";

  public static final String PUBLICATION = "publication";
  public static final String TITLE = "title";
  
  public static final String STUDYMATERIAL = "studymaterial";

  public static final String DISPLAY_NAME =  "name";
  public static final String DISPLAY_DESCRIPTION =  "description";
  public static final String DISPLAY_DURATION =  "duration";
  public static final String DISPLAY_DURATION_UNIT =  "duration unit";
  public static final String DISPLAY_TYPE =  "type of study";
  public static final String DISPLAY_START =  "start date";
  public static final String DISPLAY_END =  "end date";
  public static final String DISPLAY_KEYWORDS =  "keywords";
  public static final String DISPLAY_STUDENT_PARTICIPANTS =  "student participants";
  public static final String DISPLAY_PROFESSIONAL_PARTICIPANTS =  "professional participants";
  public static final String DISPLAY_NOTES =  "notes";
  public static final String DISPLAY_OWNED_BY =  "owned by";
  public static final String DISPLAY_LAST_EDITED_BY =  "last edited by";
  public static final String DISPLAY_RESPONSIBLES =  "responsibles";
  public static final String DISPLAY_STUDYMATERIALS =  "study material";
  public static final String DISPLAY_PUBLICATIONS =  "publications";
  
  public static final String DISPLAY_ATTRIBUTE = "display-name";
  
  /** Returns a study as XML.
   * @param study a study
   * @throws Exception if an error occurs
   * @return the study as a DOM Document
   */  
  public static Document getXML(Study study) throws Exception {
    try {
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      factory.setNamespaceAware(true);
      factory.setValidating(false);
      DocumentBuilder builder = factory.newDocumentBuilder();
      Document xml = builder.newDocument();
      Element studyElement = xml.createElement(STUDY);
      addElement(xml, studyElement, NAME, study.getName(), DISPLAY_NAME);
      addElement(xml, studyElement, DESCRIPTION, study.getDescription(), DISPLAY_DESCRIPTION);
      addElement(xml, studyElement, TYPE, study.getType(), DISPLAY_TYPE);
      addElement(xml, studyElement, RESPONSIBLES, study.getResponsibles(), DISPLAY_RESPONSIBLES);
      addElement(xml, studyElement, STUDENT_PARTICIPANTS, study.getNoOfStudents(), DISPLAY_STUDENT_PARTICIPANTS);
      addElement(xml, studyElement, PROFESSIONAL_PARTICIPANTS, study.getNoOfProfessionals(), DISPLAY_PROFESSIONAL_PARTICIPANTS);
      addElement(xml, studyElement, DURATION, study.getDuration(), study.getDurationUnit(), DISPLAY_DURATION);
      addElement(xml, studyElement, START, study.getStart(), DISPLAY_START);
      addElement(xml, studyElement, END, study.getEnd(), DISPLAY_END);
      addElement(xml, studyElement, KEYWORDS, study.getKeywords(), DISPLAY_KEYWORDS);
      addElement(xml, studyElement, NOTES, study.getNotes(), DISPLAY_NOTES);
      addElement(xml, studyElement, STUDYMATERIALS, study.getStudyMaterial(), DISPLAY_STUDYMATERIALS);
      addElement(xml, studyElement, PUBLICATIONS, study.getPublications(), DISPLAY_PUBLICATIONS);
      xml.appendChild(studyElement);
      return(xml);
    }
    
    catch(Exception e) {
      log.error("Failed to render study as XML.", e);
      throw new Exception(e);
    }
  }
  
  private static Element addElement(Document xml, Element parent, String name, Object value, String displayName) {
    Element elm = xml.createElement(name);
    if(displayName != null) {
      elm.setAttribute(DISPLAY_ATTRIBUTE, displayName);
    }
    if(value != null) {
      elm.appendChild(xml.createTextNode(value.toString()));
    }
    parent.appendChild(elm);
    return(parent);
  }

  private static Element addElement(Document xml, Element parent, String name, Integer value, DurationUnit unit, String displayName) {
    Element elm = xml.createElement(name);
    if(displayName != null) {
      elm.setAttribute(DISPLAY_ATTRIBUTE, displayName);
    }
    if(value != null) {
      elm.appendChild(xml.createTextNode(value.toString()));
    }
    if(unit != null) {
      elm.setAttribute(DURATION_UNIT, unit.getName());
    }
    parent.appendChild(elm);
    return(parent);
  }
  
  private static Element addElement(Document xml, Element parent, String name, List value, String displayName) {
    Element elm = xml.createElement(name);
    if(displayName != null) {
      elm.setAttribute(DISPLAY_ATTRIBUTE, displayName);
    }
    if(value != null) {
      for(Iterator i = value.iterator(); i.hasNext();) {
        Object item = i.next();
        if(item instanceof Person) {
          Person person = (Person)item;
          Element personElm = xml.createElement(PERSON);
          addElement(xml, personElm, FIRSTNAME, person.getFirstName(), null);
          addElement(xml, personElm, FAMILYNAME, person.getFamilyName(), null);
          elm.appendChild(personElm);
        }
        if(item instanceof Publication) {
          Publication publication = (Publication)item;
          Element publicationElm = xml.createElement(PUBLICATION);
          addElement(xml, publicationElm, TITLE, publication.getTitle(), null);
          elm.appendChild(publicationElm);
        }
        if(item instanceof StudyMaterial) {
          StudyMaterial material = (StudyMaterial)item;
          Element materialElm = xml.createElement(STUDYMATERIAL);
          addElement(xml, materialElm, DESCRIPTION, material.getDescription(), null);
          elm.appendChild(materialElm);
        }
      }
    }
    parent.appendChild(elm);
    return(parent);
  }
  
  /** Returns the specified studies as a comma-separated string.
   * @param studies a list of studies
   * @param fieldSeparator a token used to separate fields
   * @return a comma-separated representation of the list of studies
   */  
  public static String getCSV(List studies, char fieldSeparator) {
    
    StringBuffer buf = new StringBuffer();

    buf.append(DISPLAY_NAME).append(fieldSeparator);
    buf.append(DISPLAY_DESCRIPTION).append(fieldSeparator);
    buf.append(DISPLAY_DURATION).append(fieldSeparator);
    buf.append(DISPLAY_DURATION_UNIT).append(fieldSeparator);
    buf.append(DISPLAY_START).append(fieldSeparator);
    buf.append(DISPLAY_END).append(fieldSeparator);
    buf.append(DISPLAY_KEYWORDS).append(fieldSeparator);
    buf.append(DISPLAY_STUDENT_PARTICIPANTS).append(fieldSeparator);
    buf.append(DISPLAY_PROFESSIONAL_PARTICIPANTS).append(fieldSeparator);
    buf.append(DISPLAY_NOTES).append(fieldSeparator);
    buf.append(DISPLAY_OWNED_BY).append(fieldSeparator);
    buf.append(DISPLAY_LAST_EDITED_BY).append(fieldSeparator);
    buf.append(DISPLAY_RESPONSIBLES).append(fieldSeparator);
    buf.append("\n");
    
    for(Iterator i = studies.iterator(); i.hasNext();) {
      Study study = (Study)i.next();
      buf.append(getField(study.getName(), fieldSeparator)).append(fieldSeparator);
      buf.append(getField(study.getDescription(), fieldSeparator)).append(fieldSeparator);
      buf.append(getField(study.getDuration(), fieldSeparator)).append(fieldSeparator);
      buf.append(getField(study.getDurationUnit(), fieldSeparator)).append(fieldSeparator);
      buf.append(getField(formatDate(study.getStart()), fieldSeparator)).append(fieldSeparator);
      buf.append(getField(formatDate(study.getEnd()), fieldSeparator)).append(fieldSeparator);
      buf.append(getField(study.getKeywords(), fieldSeparator)).append(fieldSeparator);
      buf.append(getField(study.getNoOfStudents(), fieldSeparator)).append(fieldSeparator);
      buf.append(getField(study.getNoOfProfessionals(), fieldSeparator)).append(fieldSeparator);
      buf.append(getField(study.getNotes(), fieldSeparator)).append(fieldSeparator);
      buf.append(getField(study.getOwnedBy(), fieldSeparator)).append(fieldSeparator);
      buf.append(getField(study.getLastEditedBy(), fieldSeparator)).append(fieldSeparator);
      buf.append(getResponsibles(study.getResponsibles(), fieldSeparator));
      buf.append("\n");
    }
    return(buf.toString());
  }
  
  private static String getDuration(Integer duration, DurationUnit unit) {
    if(duration == null) {
      return("");
    }
    else {
      return(duration + " " + unit.getName());
    }
  }
  
  private static String getResponsibles(List responsibles, char fieldSeparator) {
    // Make sure we use a different separator from the field separator
    char subFieldSeparator = (fieldSeparator == ',') ? ';' : ',';
    StringBuffer buf = new StringBuffer();
    for(Iterator i = responsibles.iterator(); i.hasNext();) {
      String name = ((Person)i.next()).getFullName();
      name = name.replace(fieldSeparator, ' ').replace(subFieldSeparator, ' ');
      buf.append(name).append(subFieldSeparator);
    }
    if(buf.length() > 0) {
      buf.setLength(buf.length() - 1);
    }
    return(buf.toString());
  }
  
  private static String getField(Object value, char fieldSeparator) {
    if(value == null) {
      return("");
    }
    else {
      return(value.toString().replace(fieldSeparator, ' '));
    }
  }
  
  private static String formatDate(Date date) {
    if(date == null) {
      return(null);
    }
    else {
      return(sdf.format(date));
    }
  }
}