package no.simula.utils;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;
import no.simula.Person;

/**
 * This comparator compares two person's family name. May be used
 * to sort a collection of persons by family name. Note! This
 * class use the RuleBasedCollator to compare the names by the
 * rules that apply for <code>Locale.UK</code>.
 *
 * @author  Stian Eide
 */
public class PersonFamilyNameComparator implements Comparator {
  
  /** Creates a new instance of PersonLastNameComparator */
  public PersonFamilyNameComparator() {
    collator = Collator.getInstance(Locale.UK);
  }
  
  Collator collator;
  
  /** Compares two <CODE>Person</CODE>-objects by their family names.
   * @param o1 first person
   * @param o2 second person
   * @return Returns an integer value. Value is less than zero if o1 is less than o2,
   * value is zero if o1 and o2 are equal, value is greater than zero if o1 is
   * greater than o2.
   * @throws ClassCastException if the arguments are not <CODE>String</CODE>s
   */  
  public int compare(Object o1, Object o2) {
    return(collator.compare(((Person)o1).getFamilyName(), ((Person)o2).getFamilyName()));
  }
}
