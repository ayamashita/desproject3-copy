package no.simula.realm;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.Principal;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.realm.GenericPrincipal;
import org.apache.catalina.realm.RealmBase;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

/** A Tomcat realm-implementation that use SQL to lookup a username in a specified
 * database (the <CODE>Simula</CODE>-database) and a http-request to check the
 * provided credentials. Roles are retrieved from the <CODE>DES</CODE>-database.
 * @author Stian Eide
 */
public class SimulaRealm extends RealmBase {
  
  /** Creates a new instance of SimulaRealm */
  public SimulaRealm() {}
  
  private Connection desCon;
  
  /**
   * The connection username to use when trying to connect to the database.
   */
  protected String connectionName = null;
  
  /**
   * The connection URL to use when trying to connect to the database.
   */
  protected String connectionPassword = null;
  
  /**
   * The connection URL to use when trying to connect to the database.
   */
  protected String desConnectionURL = null;
  
  /**
   * Instance of the JDBC Driver class we use as a connection factory.
   */
  protected Driver driver = null;
  
  /**
   * The JDBC driver to use.
   */
  protected String driverName = null;
  
  /**
   * Descriptive information about this Realm implementation.
   */
  protected static final String info = "no.simula.realm.SimulaRealm";
  
  /**
   * Descriptive information about this Realm implementation.
   */
  protected static final String name = "SimulaRealm";
  
  /**
   * The PreparedStatement to use for finding the user id of a user.
   */
  protected PreparedStatement preparedUserId = null;
  
  
  /**
   * The PreparedStatement to use for identifying the roles for
   * a specified user.
   */
  protected PreparedStatement preparedRoles = null;
  
  private String authServer;
  
  private String authFunction;
  
  
  protected String getName() {
    return(this.getClass().getName());
  }
  
  /** Return the Principal associated with the specified username and
   * credentials, if there is one; otherwise return <code>null</code>.
   *
   * If there are any errors with the JDBC connection, executing
   * the query or anything we return null (don't authenticate). This
   * event is also logged, and the connection will be closed so that
   * a subsequent request will automatically re-open it.
   * @param username Username of the Principal to look up
   * @param credentials Password or other credentials to use in
   *  authenticating this username
   * @return the <CODE>Principal</CODE> if the provided username and credentials are
   * accepted.
   */
  public Principal authenticate(String username, String credentials) {
    
    try {
      
      // Ensure that we have an open database connection
      open();
      
      log("password:"+ credentials);
      // Acquire a Principal object for this user
      Principal principal = doAuthenticate(username, credentials);
      
      // Release the database connection we just used
      release();
      
      // Return the Principal (if any)
      return(principal);
    }
    
    catch(SQLException e) {
      // Log the problem for posterity
      log(sm.getString("jdbcRealm.exception"), e);
      // Close the connection so that it gets reopened next time
       close();
      // Return "not authenticated" for this request
      return(null);
    }
    
    catch(MalformedURLException mfurle) {
      log("Not a legal URL.", mfurle);
      // Return "not authenticated" for this request
      return(null);
    }
    
    catch(IOException ioe) {
      log("OI Error.", ioe);
      // Return "not authenticated" for this request
      return(null);
    }    
  }
  
	public boolean checkUser(String username, String password) {
		HashMap hash = new HashMap();
		hash.put("login", username);
		hash.put("password", password);

		XmlRpcClientConfigImpl rpcConfig = new XmlRpcClientConfigImpl();
		XmlRpcClient rcpClient = new XmlRpcClient();

		try {
			rpcConfig.setServerURL(new URL(authServer));
			rcpClient.setConfig(rpcConfig);

			Object[] methodParams = new Object[] { hash };
			Object resultSet = rcpClient.execute(authFunction, methodParams);
			Boolean result = (Boolean) resultSet;

			return result.booleanValue();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public synchronized Principal doAuthenticate(String username, String credentials) throws MalformedURLException, IOException, SQLException {
		if (username == null) {
			return null;
		}
		if (checkUser(username, credentials)) {
			// accessing the admin module
			List roles = new ArrayList();
			PreparedStatement pstmt = roles(username);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				roles.add(rs.getString(1).trim());
			}
			rs.close();
			desCon.commit();
        	return(new GenericPrincipal(this, username, credentials, roles));
		}
		return (null);
	}
  
  /**
	 * Return a PreparedStatement configured to perform the SELECT required to
	 * retrieve user roles for the specified username.
	 * 
	 * @param userId
	 *            UserId for which roles should be retrieved
	 * 
	 * @exception SQLException
	 *                if a database error occurs
	 */
  protected PreparedStatement roles(String userId) throws SQLException {
    if(preparedRoles == null) {
      String sql = "SELECT pr_name FROM pr_privilege, person_privilege_rel " + 
        "WHERE pr_privilege.pr_id = person_privilege_rel.pr_id " +
        "AND person_privilege_rel.people_id  = ?";
      preparedRoles = desCon.prepareStatement(sql);
    }
    preparedRoles.setObject(1, userId);
    return(preparedRoles);
  }  
  
  protected String getPassword(String username) {
    return(null);
  }
  
  protected Principal getPrincipal(String username) {
    return(null);
  }
  
  /**
   * Open (if necessary) and return a database connection for use by
   * this Realm.
   *
   * @exception SQLException if a database error occurs
   */
  protected void open() throws SQLException {
    
    // Do nothing if there is a database connection already open
    if(desCon != null) {
      return;
    }
    
    // Instantiate our database driver if necessary
    if(driver == null) {
      try {
        Class clazz = Class.forName(driverName);
        driver = (Driver)clazz.newInstance();
      } catch (Throwable e) {
        throw new SQLException(e.getMessage());
      }
    }

    // Open a new connection
    Properties props = new Properties();
    if(connectionName != null) {
      props.put("user", connectionName);
    }
    if(connectionPassword != null) {
      props.put("password", connectionPassword);
    }

    if(desCon == null) {
      desCon = driver.connect(desConnectionURL, props);
      desCon.setAutoCommit(false);
    }
    return;
  }
  
  /**
   * Release our use of this connection so that it can be recycled.
   */
  protected void release() {
    ; // NO-OP since we are not pooling anything
  }
  
  /**
   * Close the specified database connection.
   */
  protected void close() {
    
    // Do nothing if the database connection is already closed
    if(desCon == null) {
      return;
    }
    
    // Close our prepared statements (if any)
    try {
      preparedUserId.close();
    } catch (Throwable f) {
      ;
    }
    try {
      preparedRoles.close();
    } catch (Throwable f) {
      ;
    }
    
    // Close this database connection, and log any errors
    try {
      if(desCon != null) {
        desCon.close();
      }
    } catch (SQLException e) {
      log(sm.getString("jdbcRealm.close"), e); // Just log it here
    }
    
    // Release resources associated with the closed connection
    desCon = null;
    preparedUserId = null;
    preparedRoles = null;
  }
  
  /**
   *
   * Prepare for active use of the public methods of this Component.
   *
   * @exception LifecycleException if this component detects a fatal error
   *  that prevents it from being started
   */
  public void start() throws LifecycleException {
    // Validate that we can open our connection
    try {
      open();
    } catch (SQLException e) {
      throw new LifecycleException("Could not get database connections", e);
    }
    // Perform normal superclass initialization
    super.start();
  }
  
  /**
   * Gracefully shut down active use of the public methods of this Component.
   *
   * @exception LifecycleException if this component detects a fatal error
   *  that needs to be reported
   */
  public void stop() throws LifecycleException {
    // Perform normal superclass finalization
    super.stop();
    // Close any open DB connection
    close();
  }
  
  /** Getter for property desConnectionURL.
   * @return Value of property desConnectionURL.
   *
   */
  public java.lang.String getDesConnectionURL() {
    return desConnectionURL;
  }
  
  /** Setter for property desConnectionURL.
   * @param desConnectionURL New value of property desConnectionURL.
   *
   */
  public void setDesConnectionURL(java.lang.String desConnectionURL) {
    this.desConnectionURL = desConnectionURL;
  }
  
  /** Getter for property connectionPassword.
   * @return Value of property connectionPassword.
   *
   */
  public java.lang.String getConnectionPassword() {
    return connectionPassword;
  }
  
  /** Setter for property connectionPassword.
   * @param connectionPassword New value of property connectionPassword.
   *
   */
  public void setConnectionPassword(java.lang.String connectionPassword) {
    this.connectionPassword = connectionPassword;
  }
  
  /** Getter for property connectionName.
   * @return Value of property connectionName.
   *
   */
  public java.lang.String getConnectionName() {
    return connectionName;
  }
  
  /** Setter for property connectionName.
   * @param connectionName New value of property connectionName.
   *
   */
  public void setConnectionName(java.lang.String connectionName) {
    this.connectionName = connectionName;
  }
  
  /** Getter for property driverName.
   * @return Value of property driverName.
   *
   */
  public java.lang.String getDriverName() {
    return driverName;
  }
  
  /** Setter for property driverName.
   * @param driverName New value of property driverName.
   *
   */
  public void setDriverName(java.lang.String driverName) {
    this.driverName = driverName;
  }

public String getAuthServer() {
	return authServer;
}

public void setAuthServer(String authServer) {
	this.authServer = authServer;
}

public String getAuthFunction() {
	return authFunction;
}

public void setAuthFunction(String authFunction) {
	this.authFunction = authFunction;
}
  
}