package no.simula.des.presentation.web.forms;

import no.simula.Person;

import org.apache.struts.action.ActionForm;

public class ReportEditForm extends ActionForm {

	private String action;
	private Integer id;
	private String name;
	private String[] columnsAll;
	private String[] columnsSel;
	private Person[] responsiblesAll;
	private String[] responsiblesSel;
	private String conditionOperator;
	private boolean orderByTime;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String[] getColumnsAll() {
		if (columnsAll == null) {
			columnsAll = new String[0];
		}

		return columnsAll;
	}

	public void setColumnsAll(String[] columnsAll) {
		this.columnsAll = columnsAll;
	}

	public String[] getColumnsSel() {
		if (columnsSel == null) {
			columnsSel = new String[0];
		}

		return columnsSel;
	}

	public void setColumnsSel(String[] columnsSel) {
		this.columnsSel = columnsSel;
	}

	public Person[] getResponsiblesAll() {
		if (responsiblesAll == null) {
			responsiblesAll = new Person[0];
		}
		return responsiblesAll;
	}

	public void setResponsiblesAll(Person[] responsiblesAll) {
		this.responsiblesAll = responsiblesAll;
	}

	public String[] getResponsiblesSel() {
		if (responsiblesSel == null) {
			responsiblesSel = new String[0];
		}
		return responsiblesSel;
	}

	public void setResponsiblesSel(String[] responsiblesSel) {
		this.responsiblesSel = responsiblesSel;
	}

	public String getConditionOperator() {
		return conditionOperator;
	}

	public void setConditionOperator(String conditionOperator) {
		this.conditionOperator = conditionOperator;
	}

	public boolean isOrderByTime() {
		return orderByTime;
	}

	public void setOrderByTime(boolean orderByTime) {
		this.orderByTime = orderByTime;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
}
