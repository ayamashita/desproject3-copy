package no.simula.des.presentation.web.forms;

import java.util.Collection;

import org.apache.struts.action.ActionForm;

public class ReportListForm extends ActionForm {
	private Collection displaytagCollection;

	public Collection getDisplaytagCollection() {
		return displaytagCollection;
	}

	public void setDisplaytagCollection(Collection displaytagCollection) {
		this.displaytagCollection = displaytagCollection;
	}
}
