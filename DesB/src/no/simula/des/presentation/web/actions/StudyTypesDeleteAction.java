package no.simula.des.presentation.web.actions;

import org.apache.struts.action.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.Study;
import no.simula.des.StudyType;

/** Controls the flow of the delete study types process. The request may be in
 * two states:
 * <ul>
 *  <li>
 *    A list of deletable study types exists in session
 *  </li>
 *  <li>
 *    If not an <CODE>error</CODE> action forward is issued
 *  </li>
 * </ul>

 *
 * Next, a study type, if specified by the <CODE>id</CODE> request parameter,
 * is deleted unless it is used by some study.
 * <p>
 * The deleted study type is deleted from deletable study types, and the next
 * in this list is processed. If the list is empty a <CODE>return</CODE> action
 * forward is returned.
 * <p>
 * If the study type is unused, it is added as deletable to the request, if not,
 * it is added as not delatable along with a list of studies, which use this
 * study type. Then a <CODE>confirm</CODE> action forward is returned.
 */
public class StudyTypesDeleteAction extends Action {

  /** Execute this action
   * @param mapping the action mapping that match the current request
   * @param actionForm the action form linked to the current action mapping
   * @param request the current servlet request object
   * @param response the current servlet response object
   * @throws Exception if the action could not be successfully completed
   * @return an action forward from the action mapping depending on the outcome of this
   * action
   */  
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm actionForm,
    HttpServletRequest request,
    HttpServletResponse response)
    throws Exception
  {
    
    if(isCancelled(request)) {
      return(mapping.findForward("return"));
    }
    
    Simula simula = SimulaFactory.getSimula();

    // Get deletable study types from the session.
    List deletableStudyTypes = (List)request.getSession().getAttribute(Constants.DELETE_STUDYTYPES);
        
    // If the list is null at this point, we have an illegal request and return an error.
    if(deletableStudyTypes == null) {
      return(mapping.findForward("error"));
    }

    // Lets delete a study as specified in the request and display either a new
    // confirmation if there are more studies to be deleted or the new list of
    // studies.
    else {
      String deleteIdString = request.getParameter("id");
      if(!skipStudyType(request) && deleteIdString != null) {
        Integer deleteId = Integer.valueOf(deleteIdString);
        if(simula.getStudiesByStudyType(deleteId) == null) {
          simula.deleteStudyType(deleteId);
        }
      }
      if(deletableStudyTypes.isEmpty()) {
        return(mapping.findForward("return"));
      }
      else {
        StudyType studyType = (StudyType)deletableStudyTypes.remove(0);
        request.getSession().setAttribute(Constants.DELETE_STUDYTYPES, deletableStudyTypes);
        List containingStudies = simula.getStudiesByStudyType(studyType.getId());
        if(containingStudies == null) {
          request.setAttribute(Constants.DELETE_THIS_STUDYTYPE, studyType);
          request.setAttribute(Constants.DONT_DELETE_THIS_STUDYTYPE, null);
          request.setAttribute(Constants.BEAN_STUDIES, null);
        }
        else {
          request.setAttribute(Constants.DELETE_THIS_STUDYTYPE, null);
          request.setAttribute(Constants.DONT_DELETE_THIS_STUDYTYPE, studyType);
          request.setAttribute(Constants.BEAN_STUDIES, containingStudies);
        }
        return(mapping.findForward("confirm"));
      }
    }
  }
  
  private boolean skipStudyType(HttpServletRequest request) {
    return(request.getParameter("skip") != null);
  }
}