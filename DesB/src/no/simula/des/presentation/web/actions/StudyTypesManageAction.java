package no.simula.des.presentation.web.actions;

import org.apache.struts.action.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import no.halogen.struts.ActionForwardUtil;
import no.halogen.utils.table.Table;
import no.halogen.utils.table.SortableTableColumn;
import no.halogen.utils.table.TableColumn;
import no.halogen.utils.table.TableRenderer;
import no.halogen.utils.table.TableSource;
import no.halogen.utils.table.TableSourceList;
import no.simula.Simula;
import no.simula.SimulaFactory;

/** Displays the table of registered study types and handles dispatching of
 * requests to other actions or views used to manage study types.
 * <p>
 * Returns:
 *
 * <ul>
 *  <li><CODE>new</CODE> to display an empty study type form</li>
 *  <li><CODE>edit</CODE> to display a study type form with values</li>
 *  <li><CODE>delete</CODE> to confirm deletion of selected study types</li>
 *  <li><CODE>list</CODE> to display the table of registered study types</li>
 * </ul>
 *
 * Please note that all actions linked from the main view of manage study types
 * is dispatched through this action
 */
public class StudyTypesManageAction extends Action {

  /** Execute this action
   * @param mapping the action mapping that match the current request
   * @param actionForm the action form linked to the current action mapping
   * @param request the current servlet request object
   * @param response the current servlet response object
   * @throws Exception if the action could not be successfully completed
   * @return an action forward from the action mapping depending on the outcome of this
   * action
   */  
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm actionForm,
    HttpServletRequest request,
    HttpServletResponse response)
    throws Exception
  {
    Simula simula = SimulaFactory.getSimula();
    Table table = (Table)request.getSession().getAttribute(Constants.TABLE_STUDYTYPES);

    if(deleteStudyType(request)) {
      // Firstly, we check if this is an initial delete-request. If it is, the deletable
      // is populated with the ids in the request.
      List deletableStudyTypeIds = new ArrayList();
      for(Iterator i = request.getParameterMap().keySet().iterator(); i.hasNext();) {
        Object o = i.next();
        if(o instanceof String) {
          String paramName = (String)o;
          if(paramName.startsWith("id[")) {
            String id = paramName.substring(paramName.indexOf("[")+1, paramName.indexOf("]"));
            deletableStudyTypeIds.add(Integer.valueOf(id));
          }
        }
      }
      if(deletableStudyTypeIds.isEmpty()) {
        ActionError error = new ActionError("errors.delete.empty", "study types");
        ActionErrors errors = new ActionErrors();
        errors.add(ActionErrors.GLOBAL_ERROR, error);
        saveErrors(request, errors);
      }
      else {
        List deletableStudyTypes = simula.getStudyTypesByIds(deletableStudyTypeIds);
        request.getSession().setAttribute(Constants.DELETE_STUDYTYPES, deletableStudyTypes);
        return(mapping.findForward("delete"));
      }
    }

    if(editStudyType(request)) {
      ActionForward forward = mapping.findForward("edit");
      forward = ActionForwardUtil.addRequestParameter(forward, request, "id");
      return(forward);
    }

    if(newStudyType(request)) {
      return(mapping.findForward("new"));
    }

    if(table == null) {
      TableSource source = new TableSourceList(simula.getStudyTypes());
      List columns = new ArrayList(2);
      SortableTableColumn name = new SortableTableColumn("name", "studytypes.table.heading.name", null, null, null, "/adm/studytypes/manage?edit=true", "id", "id", null);
      Properties resource = ((TableRenderer)request.getSession().getServletContext().getAttribute(Constants.TABLE_RENDERER)).getResource();
      columns.add(name);
      table = new Table(columns, source, 10, name);
      request.getSession().setAttribute(Constants.TABLE_STUDYTYPES, table);
    }

    String sortBy = request.getParameter("sortBy");
    if(sortBy != null && sortBy.length() > 0) {
      table.sortBy(sortBy);
    }

    String page = request.getParameter("page");
    if(page != null && page.length() > 0) {
      table.setCurrentPage(Integer.parseInt(page));
    }

    return(mapping.findForward("list"));
  }

  private boolean deleteStudyType(HttpServletRequest request) {
    return(request.getParameter("delete") != null);
  }

  private boolean newStudyType(HttpServletRequest request) {
    return(request.getParameter("new") != null);
  }

  private boolean editStudyType(HttpServletRequest request) {
    return(request.getParameter("edit") != null);
  }

}