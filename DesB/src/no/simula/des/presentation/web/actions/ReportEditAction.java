package no.simula.des.presentation.web.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.Person;
import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.Report;
import no.simula.des.persistence.PersistentReport;
import no.simula.des.presentation.web.forms.ReportEditForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ReportEditAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ReportEditForm f = (ReportEditForm) form;
		
		String action = f.getAction();
		if (action == null) {
			action = "new";
		}
		
		if ("save".equals(action)) {
			Report report = new Report();
			report.setId(f.getId());
			report.setName(f.getName());
			report.setConditionOperator(f.getConditionOperator());
			report.setOrderByTime(f.isOrderByTime());
			report.setColumnsSel(f.getColumnsSel());
			report.setResponsiblesSel(f.getResponsiblesSel());
			report.setUserId(request.getUserPrincipal().getName());
			
			if (report.getId() == null || report.getId().intValue() <= 0) {
				// insert
				PersistentReport persistentReport = new PersistentReport();
				Integer newId = persistentReport.create(report);
				f.setId(newId);
			}
			else {
				// update
				PersistentReport persistentReport = new PersistentReport();
				persistentReport.update(report);
			}
			
		}
		else if ("edit".equals(action)) {
			PersistentReport persistentReport = new PersistentReport();
			Report r = (Report) persistentReport.findByKey(f.getId());

			// TODO read columns and responsibles
			f.setName(r.getName());
			f.setConditionOperator(r.getConditionOperator());
			f.setOrderByTime(r.isOrderByTime());
			f.setColumnsSel(r.getColumnsSel());
			f.setResponsiblesSel(r.getResponsiblesSel());
		}
		
	    Simula simula = SimulaFactory.getSimula();
		Person[] responsiblesAll = (Person[]) simula.getPersons().toArray(new Person[0]);
		f.setResponsiblesAll(responsiblesAll);
		
		return mapping.findForward("view");
	}
}
