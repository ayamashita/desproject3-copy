package no.simula.des.presentation.web.actions;

import org.apache.struts.action.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import no.halogen.search.Criterium;
import no.halogen.search.Search;

import no.halogen.search.SearchFactory;
import no.halogen.struts.ActionForwardUtil;
import no.halogen.utils.table.Table;
import no.halogen.utils.table.SortableTableColumn;
import no.halogen.utils.table.TableColumn;
import no.halogen.utils.table.TableSource;
import no.halogen.utils.table.TableSourceList;
import no.simula.Simula;
import no.simula.SimulaException;
import no.simula.SimulaFactory;
import no.simula.des.StudySearchTableSource;
import no.simula.des.search.SearchFactoryImpl;
import no.simula.des.presentation.web.DateColumnDecorator;
import no.simula.des.presentation.web.forms.StudySearchForm;

/** Handles the navigation of the study search result table and dispatch requests to
 * view study details.
 * <p>
 * Return <CODE>success</CODE> to view the search results and <CODE>view</CODE> to
 * view the study details.
 */
public class StudiesSearchResultAction extends Action {

  /** Execute this action
   * @param mapping the action mapping that match the current request
   * @param actionForm the action form linked to the current action mapping
   * @param request the current servlet request object
   * @param response the current servlet response object
   * @throws Exception if the action could not be successfully completed
   * @return an action forward from the action mapping depending on the outcome of this
   * action
   */  
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm actionForm,
    HttpServletRequest request,
    HttpServletResponse response)
    throws Exception
  {
    
    if(viewStudyDetails(request)) {
      ActionForward forward = mapping.findForward("view");
      forward = ActionForwardUtil.addRequestParameter(forward, request, "id");
      return(forward);
    }

    Simula simula = SimulaFactory.getSimula();
    Table table = (Table)request.getSession().getAttribute(Constants.TABLE_SEARCH_STUDIES);
    StudySearchForm form = (StudySearchForm)actionForm;

    String sortBy = request.getParameter("sortBy");
    if(sortBy != null && sortBy.length() > 0) {
      table.sortBy(sortBy);
    }

    String page = request.getParameter("page");
    if(page != null && page.length() > 0) {
      table.setCurrentPage(Integer.parseInt(page));
    }

    List types = simula.getStudyTypes();
    request.setAttribute(Constants.BEAN_STUDY_TYPES, types);
    List persons = simula.getPersons();
    request.setAttribute(Constants.BEAN_PERSONS, persons);    
    request.getSession().setAttribute(Constants.TABLE_SEARCH_STUDIES, table);
    return(mapping.findForward("success"));
  }

  private boolean viewStudyDetails(HttpServletRequest request) {
    return(request.getParameter("view") != null);
  }
}