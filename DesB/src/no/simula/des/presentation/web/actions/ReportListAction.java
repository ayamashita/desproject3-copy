package no.simula.des.presentation.web.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.des.persistence.PersistentReport;
import no.simula.des.presentation.web.forms.ReportListForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ReportListAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		PersistentReport persistentReport = new PersistentReport();
		List reports = persistentReport.find();
		((ReportListForm)form).setDisplaytagCollection(reports);
		
		return mapping.findForward("view");
	}
}
