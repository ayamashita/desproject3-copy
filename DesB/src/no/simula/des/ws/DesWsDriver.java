package no.simula.des.ws;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.util.Properties;

public class DesWsDriver implements Driver {
	
	static {
		try {
			DriverManager.registerDriver(new DesWsDriver());
		}
		catch (SQLException e) {
			// nothing to do
		}
	}
	
	public boolean acceptsURL(String url) throws SQLException {
		return (url.indexOf("desws") > 0);
	}

	public Connection connect(String url, Properties info) throws SQLException {
		return new DesWsConnection();
	}

	public int getMajorVersion() {
		return 1;
	}

	public int getMinorVersion() {
		return 0;
	}

	public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
		return null;
	}

	public boolean jdbcCompliant() {
		return false;
	}

}
