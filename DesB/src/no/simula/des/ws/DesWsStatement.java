package no.simula.des.ws;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

/**
 * Supports only simple query from one table with one where condition with positive operator (=, like, in)
 * SELECT [columns,] FROM [people|publication] [WHERE column =|like|in [?..]] 
 * @author junm
 *
 */
public class DesWsStatement implements PreparedStatement {

	private class Query {
		String table;
		String[] columns;
		String whereColumn;
		String[] whereValues;
	}
	
	private Query query = new Query();
	
	public DesWsStatement(String sql) throws SQLException {
		// test acceptable query
		Matcher m = Pattern.compile("SELECT .+ FROM (\\w+).*").matcher(sql); // TODO check only one where condition
		if (!m.matches()) {
			throw new SQLException("Unsupported query: " + sql);
		}
		
		// parse the query
		m = Pattern.compile("SELECT (.*) FROM (\\w*).*").matcher(sql);
		m.find();
		this.query.columns = m.group(1).split(", ");
		this.query.table = m.group(2);

		m = Pattern.compile(".*WHERE \\(*\\s*(\\w+).*").matcher(sql);
		if (m.find()) {
			this.query.whereColumn = m.group(1);
			this.query.whereValues = new String[(sql + " ").split("\\?").length-1];
		}
	}

	public void addBatch() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void clearParameters() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public boolean execute() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public ResultSet executeQuery() throws SQLException {
		List results;
		if (query.whereColumn == null) {
			HashMap filter = new HashMap();
			results = DesWSClientImpl.getResults(query.table, filter, null);
		}
		else {
			results = new ArrayList();
			for (int i = 0; i < query.whereValues.length; i++) {
				String value = query.whereValues[i];
				HashMap filter = new HashMap();
				filter.put(query.whereColumn, value); // TODO mapping
				results.addAll(DesWSClientImpl.getResults(query.table, filter, null));
			}
		}
		return new DesWsResultset(results);
	}

	public int executeUpdate() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public ResultSetMetaData getMetaData() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public ParameterMetaData getParameterMetaData() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public void setArray(int i, Array x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setAsciiStream(int parameterIndex, InputStream x, int length) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setBigDecimal(int parameterIndex, BigDecimal x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setBinaryStream(int parameterIndex, InputStream x, int length) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setBlob(int i, Blob x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setBoolean(int parameterIndex, boolean x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setByte(int parameterIndex, byte x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setBytes(int parameterIndex, byte[] x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setCharacterStream(int parameterIndex, Reader reader, int length) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setClob(int i, Clob x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setDate(int parameterIndex, Date x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setDate(int parameterIndex, Date x, Calendar cal) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setDouble(int parameterIndex, double x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setFloat(int parameterIndex, float x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setInt(int parameterIndex, int x) throws SQLException {
		query.whereValues[parameterIndex-1] = String.valueOf(x);
	}

	public void setLong(int parameterIndex, long x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setNull(int parameterIndex, int sqlType) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setNull(int paramIndex, int sqlType, String typeName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setObject(int parameterIndex, Object x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setObject(int parameterIndex, Object x, int targetSqlType) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setObject(int parameterIndex, Object x, int targetSqlType, int scale) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setRef(int i, Ref x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setShort(int parameterIndex, short x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setString(int parameterIndex, String x) throws SQLException {
		// ignore %'s, because the webservice already interprets the filter value as "like %x%."
		query.whereValues[parameterIndex-1] = StringUtils.replace(x, "%", "");
	}

	public void setTime(int parameterIndex, Time x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setTime(int parameterIndex, Time x, Calendar cal) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setURL(int parameterIndex, URL x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setUnicodeStream(int parameterIndex, InputStream x, int length) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void addBatch(String sql) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void cancel() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void clearBatch() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void clearWarnings() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void close() throws SQLException {
		// do nothing
	}

	public boolean execute(String sql) throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public boolean execute(String sql, int[] columnIndexes) throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public boolean execute(String sql, String[] columnNames) throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public int[] executeBatch() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public ResultSet executeQuery(String sql) throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public int executeUpdate(String sql) throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public int executeUpdate(String sql, String[] columnNames) throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public Connection getConnection() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public int getFetchDirection() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public int getFetchSize() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public ResultSet getGeneratedKeys() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public int getMaxFieldSize() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public int getMaxRows() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public boolean getMoreResults() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public boolean getMoreResults(int current) throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public int getQueryTimeout() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public ResultSet getResultSet() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public int getResultSetConcurrency() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public int getResultSetHoldability() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public int getResultSetType() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public int getUpdateCount() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public SQLWarning getWarnings() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public void setCursorName(String name) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setEscapeProcessing(boolean enable) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setFetchDirection(int direction) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setFetchSize(int rows) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setMaxFieldSize(int max) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setMaxRows(int max) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setQueryTimeout(int seconds) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

}
