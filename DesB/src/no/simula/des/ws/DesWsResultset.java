package no.simula.des.ws;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DesWsResultset implements ResultSet {

	Iterator rsIterator;
	HashMap currentRow = null;
	
	public DesWsResultset(List results) {
		this.rsIterator = results.iterator();
	}

	public boolean absolute(int row) throws SQLException {
		throw new SQLException("Not implemented method.");
	}

	public void afterLast() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void beforeFirst() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void cancelRowUpdates() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void clearWarnings() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void close() throws SQLException {
		// do nothing
	}

	public void deleteRow() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public int findColumn(String columnName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public boolean first() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Array getArray(int i) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Array getArray(String colName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public InputStream getAsciiStream(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public InputStream getAsciiStream(String columnName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public BigDecimal getBigDecimal(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public BigDecimal getBigDecimal(String columnName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public BigDecimal getBigDecimal(int columnIndex, int scale) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public BigDecimal getBigDecimal(String columnName, int scale) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public InputStream getBinaryStream(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public InputStream getBinaryStream(String columnName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Blob getBlob(int i) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Blob getBlob(String colName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public boolean getBoolean(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public boolean getBoolean(String columnName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public byte getByte(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public byte getByte(String columnName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public byte[] getBytes(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public byte[] getBytes(String columnName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Reader getCharacterStream(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Reader getCharacterStream(String columnName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Clob getClob(int i) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Clob getClob(String colName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public int getConcurrency() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public String getCursorName() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Date getDate(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Date getDate(String columnName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Date getDate(int columnIndex, Calendar cal) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Date getDate(String columnName, Calendar cal) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public double getDouble(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public double getDouble(String columnName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public int getFetchDirection() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public int getFetchSize() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public float getFloat(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public float getFloat(String columnName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public int getInt(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public int getInt(String columnName) throws SQLException {
		try {
			return Integer.parseInt((String) currentRow.get(columnName));
		}
		catch (Exception e) {
			return 0;
		}
	}

	public long getLong(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public long getLong(String columnName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public ResultSetMetaData getMetaData() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Object getObject(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Object getObject(String columnName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Object getObject(int i, Map map) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Object getObject(String colName, Map map) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Ref getRef(int i) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Ref getRef(String colName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public int getRow() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public short getShort(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public short getShort(String columnName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Statement getStatement() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public String getString(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public String getString(String columnName) throws SQLException {
		return (String) currentRow.get(columnName);
	}

	public Time getTime(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Time getTime(String columnName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Time getTime(int columnIndex, Calendar cal) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Time getTime(String columnName, Calendar cal) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Timestamp getTimestamp(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Timestamp getTimestamp(String columnName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Timestamp getTimestamp(int columnIndex, Calendar cal) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public Timestamp getTimestamp(String columnName, Calendar cal) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public int getType() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public URL getURL(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public URL getURL(String columnName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public InputStream getUnicodeStream(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public InputStream getUnicodeStream(String columnName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public SQLWarning getWarnings() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void insertRow() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public boolean isAfterLast() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public boolean isBeforeFirst() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public boolean isFirst() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public boolean isLast() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public boolean last() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void moveToCurrentRow() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void moveToInsertRow() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public boolean next() throws SQLException {
		if (rsIterator.hasNext()) {
			currentRow = (HashMap) rsIterator.next();
			return true;
		}
		else {
			return false;
		}
	}

	public boolean previous() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void refreshRow() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public boolean relative(int rows) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public boolean rowDeleted() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public boolean rowInserted() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public boolean rowUpdated() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setFetchDirection(int direction) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void setFetchSize(int rows) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateArray(int columnIndex, Array x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateArray(String columnName, Array x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateAsciiStream(int columnIndex, InputStream x, int length) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateAsciiStream(String columnName, InputStream x, int length) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateBigDecimal(int columnIndex, BigDecimal x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateBigDecimal(String columnName, BigDecimal x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateBinaryStream(int columnIndex, InputStream x, int length) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateBinaryStream(String columnName, InputStream x, int length) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateBlob(int columnIndex, Blob x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateBlob(String columnName, Blob x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateBoolean(int columnIndex, boolean x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateBoolean(String columnName, boolean x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateByte(int columnIndex, byte x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateByte(String columnName, byte x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateBytes(int columnIndex, byte[] x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateBytes(String columnName, byte[] x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateCharacterStream(int columnIndex, Reader x, int length) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateCharacterStream(String columnName, Reader reader, int length) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateClob(int columnIndex, Clob x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateClob(String columnName, Clob x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateDate(int columnIndex, Date x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateDate(String columnName, Date x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateDouble(int columnIndex, double x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateDouble(String columnName, double x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateFloat(int columnIndex, float x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateFloat(String columnName, float x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateInt(int columnIndex, int x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateInt(String columnName, int x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateLong(int columnIndex, long x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateLong(String columnName, long x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateNull(int columnIndex) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateNull(String columnName) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateObject(int columnIndex, Object x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateObject(String columnName, Object x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateObject(int columnIndex, Object x, int scale) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateObject(String columnName, Object x, int scale) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateRef(int columnIndex, Ref x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateRef(String columnName, Ref x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateRow() throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateShort(int columnIndex, short x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateShort(String columnName, short x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateString(int columnIndex, String x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateString(String columnName, String x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateTime(int columnIndex, Time x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateTime(String columnName, Time x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateTimestamp(int columnIndex, Timestamp x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public void updateTimestamp(String columnName, Timestamp x) throws SQLException {
		throw new SQLException("Not implemented method.");
		
	}

	public boolean wasNull() throws SQLException {
		throw new SQLException("Not implemented method.");
	}

}
