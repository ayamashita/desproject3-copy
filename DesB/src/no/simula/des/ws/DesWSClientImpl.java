package no.simula.des.ws;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.commons.collections.iterators.ArrayIterator;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

public class DesWSClientImpl {
	/**
	 * low-level calling of the service
	 * 
	 * @param action
	 * @param hash
	 * @return
	 * @throws MalformedURLException
	 * @throws XmlRpcException
	 */
	private static Object[] callService(String action, HashMap hash) throws MalformedURLException, XmlRpcException {
		XmlRpcClientConfigImpl rpcConfig = new XmlRpcClientConfigImpl();
		XmlRpcClient rcpClient = new XmlRpcClient();

		rpcConfig.setServerURL(new URL(getProperties().getProperty("desws.url")));
		rcpClient.setConfig(rpcConfig);

		Object[] methodParams = new Object[] { hash };
		Object resultSet = rcpClient.execute(action, methodParams);
		Object[] elementList = (Object[]) resultSet;
		return elementList;
	}

	private static class ColumnMapping {
		List original;
		List des;

		ColumnMapping(List original, List des) {
			this.original = original;
			this.des = des;
		}

		HashMap translateToWs(HashMap map) {
			HashMap result = new HashMap();
			for (int i = 0; i < original.size(); i++) {
				String key = (String) original.get(i);
				if (map.containsKey(key)) {
					result.put(des.get(i), map.get(key));
				}
			}
			return result;
		}

		HashMap translateFromWs(HashMap map) {
			HashMap result = new HashMap();
			for (int i = 0; i < des.size(); i++) {
				String key = (String) des.get(i);
				if (map.containsKey(key)) {
					result.put(original.get(i), correctValue(map.get(key)));
				}
			}
			return result;
		}
	}

	/** column mappings for all tables */
	private static HashMap tableMappings = new HashMap();
	
	static {
		// init mappings for people and publication tables
		ColumnMapping peopleMapping = new ColumnMapping(Arrays.asList(new String[] { "people_id", "people_first_name", "people_family_name", "people_email", "url" }), Arrays.asList(new String[] { "id", "firstname", "lastname", "email", "url" }));
		tableMappings.put("people", peopleMapping);

		ColumnMapping publicationsMapping = new ColumnMapping(Arrays.asList(new String[] { "publication_id", "publication_title", "authors", "url" }), Arrays.asList(new String[] { "id", "title", "authors", "url" }));
		tableMappings.put("publication", publicationsMapping);
	}

	private static Properties properties = null;
	static final private String PROPERTIES = "no/simula/simula.properties";

	/**
	 * Gets properties from a property file Used to get database connection
	 * strings
	 * 
	 * @return Properties the properties in the property file
	 */
	private static Properties getProperties() {
		if (properties  == null) {
			ClassLoader cl = DesWSClientImpl.class.getClassLoader();
			properties = new Properties();
			try {
				properties.load(cl.getResourceAsStream(PROPERTIES));
			}
			catch (IOException e) {
				throw new RuntimeException("Cannot open properties.", e);
			}
		}

		return properties;
	}

	public static List getResults(String table, HashMap filter, String sortOn) {
		try {
			ColumnMapping mapping = (ColumnMapping) tableMappings.get(table);
			if (mapping == null) {
				throw new IllegalArgumentException("Unknown table: " + table);
			}
			HashMap hash = mapping.translateToWs(filter);
			// hash.put("sort_on", sortOn);

			Object[] serviceResult = callService(getProperties().getProperty("desws.function."+table), hash);

			ArrayList results = new ArrayList(serviceResult.length);
			for (int i = 0; i < serviceResult.length; i++) {
				HashMap rec = (HashMap) serviceResult[i];
				results.add(mapping.translateFromWs(rec));
			}

			return results;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * remove characters like %20, etc.
	 * 
	 * @param value
	 * @return
	 */
	static String correctValue(Object value) {
		if (value == null) {
			return null;
		}

		if (value instanceof String) {
			try {
				return URLDecoder.decode((String) value, "UTF-8");
			}
			catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				throw new RuntimeException("Encoding UTF-8 not supported.", e);
			}
		}
		// authors field
		else if (value instanceof Object[]) {
			StringBuffer authors = new StringBuffer();
			for (Iterator iter = new ArrayIterator((Object[]) value); iter.hasNext();) {
				HashMap author = (HashMap) iter.next();
				authors.append(author.get("firstname")).append(" ").append(author.get("lastname"));
				if (iter.hasNext()) {
					authors.append(", ");
				}
			}
			return authors.toString();
		}
		else {
			return value.toString();
		}
	}

}
