package no.simula.des.persistence;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import no.halogen.persistence.CreatePersistentObjectException;
import no.halogen.persistence.DeletePersistentObjectException;
import no.halogen.persistence.PersistentObject;
import no.halogen.persistence.UpdatePersistentObjectException;
import no.halogen.statements.StatementException;
import no.simula.des.Report;
import no.simula.des.statements.ReportColumnsStatement;
import no.simula.des.statements.ReportStatement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 */
public class PersistentReport implements PersistentObject {
	/**
	 * Field log
	 */
	private static Log log = LogFactory.getLog(PersistentReport.class);

	public Integer create(Object instance) throws CreatePersistentObjectException {
		ReportStatement statement = new ReportStatement();
		statement.setDataBean(instance);
		try {
			return statement.executeInsert();
		}
		catch (StatementException e) {
			log.error(e);
			throw new CreatePersistentObjectException();
		}
	}

	public boolean delete(Object id) throws DeletePersistentObjectException {
		// TODO Auto-generated method stub
		return false;
	}

	public List find() {
		ReportStatement statement = new ReportStatement();
		statement.setDataBean(new Report());
		try {
			return statement.executeSelect();
		}
		catch (StatementException e) {
			log.error(e);
			throw new RuntimeException(e);
		}
	}

	public Object findByKey(Object id) {
		ReportStatement statement = new ReportStatement();
		statement.setDataBean(new Report());
		try {
			return statement.executeSelect(id);
		}
		catch (StatementException e) {
			log.error(e);
			throw new RuntimeException(e);
		}
	}

	public List findByKeys(List entityIds) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean update(Object instance) throws UpdatePersistentObjectException {
		return update(((Report)instance).getId(), instance);
	}

	public boolean update(Object id, Object instance) throws UpdatePersistentObjectException {
		ReportStatement statement = new ReportStatement();
		statement.setDataBean(instance);
		try {
			statement.executeUpdate(id);
		}
		catch (StatementException e) {
			log.error(e);
			throw new UpdatePersistentObjectException();
		}
		
		ReportColumnsStatement rcs = new ReportColumnsStatement();
		try {
			rcs.executeDelete(id);
			
			rcs = new ReportColumnsStatement();
			rcs.setReportId(((Integer)id).intValue());
			
			for (Iterator iterator = Arrays.asList(((Report)instance).getColumnsSel()).iterator(); iterator.hasNext();) {
				String column = (String) iterator.next();
				rcs.setDataBean(column);
				rcs.executeInsert();
			}
		}
		catch (StatementException e) {
			log.error(e);
			throw new UpdatePersistentObjectException();
		}
		
		return true;
	}

}
