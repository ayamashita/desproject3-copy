/*
 * Created on 04.nov.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.persistence;

import java.util.ArrayList;
import java.util.List;

import no.halogen.persistence.CreatePersistentObjectException;
import no.halogen.persistence.DeletePersistentObjectException;
import no.halogen.persistence.PersistentObject;
import no.halogen.persistence.UpdatePersistentObjectException;
import no.halogen.statements.StatementException;
import no.simula.Privilege;
import no.simula.des.statements.PrivilegeStatement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Frode Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class PersistentPrivilege implements PersistentObject {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(PersistentPrivilege.class);

  /* (non-Javadoc)
   * @see no.halogen.persistence.PersistentObject#create(java.lang.Object)
   */
  /**
   * Method create
   * @param instance Object
   * @return Integer
   * @throws CreatePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#create(Object)
   */
  public Integer create(Object instance) throws CreatePersistentObjectException {
    // TODO Auto-generated method stub
    return null;
  }

  /* (non-Javadoc)
   * @see no.halogen.persistence.PersistentObject#delete(java.lang.Integer)
   */
  /**
   * Method delete
   * @param id Integer
   * @return boolean
   * @throws DeletePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#delete(Integer)
   */
  public boolean delete(Object id) throws DeletePersistentObjectException {
    // TODO Auto-generated method stub
    return (false);
  }

  /**
   * Finds a privilege by it's id/key
   * 
   * @param id - The privilege's id/key
   * @return Populated Privilege object
   * 
   * @see no.halogen.persistence.PersistentObject#findByKey(Integer)
   */
  public Object findByKey(Object id) {
    Object result = new Object();
    Privilege privilege = new Privilege(); //dataBean that will contain the values from the search

    PrivilegeStatement statement = new PrivilegeStatement();

    statement.setDataBean(privilege);

    try {
      result = statement.executeSelect(id);
    } catch (StatementException e) {
      log.error("findByKey() - Fetching of privilege with id " + id + " failed!!", e);
      return (null);
    }

    if (result instanceof Privilege) {
      privilege = (Privilege) result;
    }

    return (privilege);
  }

  /**
   * Finds privileges by a list of identities/key
   * 
   * @param entityIds - A List of privilege ids, each id as an Integer 
   * @return List with populated Privilege objects. If the List is empty, all publications will be returned
   * 
   * @see no.halogen.persistence.PersistentObject#findByKeys(List)
   */
  public List findByKeys(List entityIds) {
    Privilege privilege = null; //dataBean to populate with data
    List privileges = new ArrayList();

    if (!entityIds.isEmpty()) {
      PrivilegeStatement statement = new PrivilegeStatement();

      statement.setDataBean(privilege);

      try {
        privileges = (ArrayList) statement.executeSelect(entityIds);
      } catch (StatementException e) {
        log.error("findByKeys() - Fetching list of privileges: " + entityIds.toString(), e);
        return (null);
      }
    }

    return (privileges);
  }

  /* (non-Javadoc)
   * @see no.halogen.persistence.PersistentObject#update(java.lang.Object)
   */
  /**
   * Method update
   * @param instance Object
   * @return boolean
   * @throws UpdatePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#update(Object)
   */
  public boolean update(Object instance) throws UpdatePersistentObjectException {
    // TODO Auto-generated method stub
    return (false);
  }

  /* (non-Javadoc)
   * @see no.halogen.persistence.PersistentObject#update(java.lang.Integer, java.lang.Object)
   */
  /**
   * Method update
   * @param id Integer
   * @param instance Object
   * @return boolean
   * @throws UpdatePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#update(Integer, Object)
   */
  public boolean update(Object id, Object instance) throws UpdatePersistentObjectException {
    // TODO Auto-generated method stub
    return (false);
  }

  /**   
   * Finding all Privileges
   * 
   * @return - All privileges in the database
   * 
   * @see no.halogen.persistence.PersistentObject#find()
   */
  public List find() {
    //    Privilege privilege = null; //dataBean to populate with data
    Privilege privilege = new Privilege();
    List privileges = new ArrayList();

    PrivilegeStatement statement = new PrivilegeStatement();

    statement.setDataBean(privilege);

    try {
      privileges = (ArrayList) statement.executeSelect();
    } catch (StatementException e) {
      log.error("findByKeys() - Fetching list of all privileges", e);
      return (null);
    }

    return (privileges);

  }

}
