package no.simula.des;

import java.io.Serializable;

import no.halogen.persistence.PersistableIntegerId;

public class Report implements PersistableIntegerId, Serializable{

	private Integer id;
	private String name;
	private String[] columnsSel;
	private String[] responsiblesSel;
	private String conditionOperator;
	private boolean orderByTime;
	private String userId;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String[] getColumnsSel() {
		return columnsSel;
	}

	public void setColumnsSel(String[] columnsSel) {
		this.columnsSel = columnsSel;
	}

	public String[] getResponsiblesSel() {
		return responsiblesSel;
	}

	public void setResponsiblesSel(String[] responsiblesSel) {
		this.responsiblesSel = responsiblesSel;
	}

	public String getConditionOperator() {
		return conditionOperator;
	}

	public void setConditionOperator(String conditionOperator) {
		this.conditionOperator = conditionOperator;
	}

	public boolean isOrderByTime() {
		return orderByTime;
	}

	public void setOrderByTime(boolean orderByTime) {
		this.orderByTime = orderByTime;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
