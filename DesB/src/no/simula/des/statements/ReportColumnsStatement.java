package no.simula.des.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import no.halogen.statements.ObjectStatementImpl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ReportColumnsStatement extends ObjectStatementImpl {
	/**
	 * Field log
	 */
	private static Log log = LogFactory.getLog(ReportColumnsStatement.class);

	/**
	 * Field INSERT_COLUMNS
	 */
	private final String INSERT_COLUMNS = "report_id, column_name, seq";

	private final String KEY = "report_id";

	/**
	 * Field INSERT_VALUES
	 */
	private final String INSERT_VALUES = "(?, ?, ?)";

	private final String SELECT_COLUMNS = "column_name";

	/**
	 * Field TABLE_NAME
	 */
	private final String TABLE_NAME = "personalised_report_columns";

	/**
	 * Returns the columns and the order they must appear in in the insert
	 * statement
	 * 
	 * @return String
	 * @see no.halogen.statements.Entity#getInsertColumnNames()
	 */
	public String getInsertColumnNames() {
		return (INSERT_COLUMNS);
	}

	/**
	 * Returns name of the id column
	 * 
	 * @return String
	 * @see no.halogen.statements.Entity#getKey()
	 */
	public String getKey() {
		return KEY;
	}

	/**
	 * Returns the columns that will be fetched in a select statement
	 * 
	 * @return String
	 * @see no.halogen.statements.Entity#getSelectColumnNames()
	 */
	public String getSelectColumnNames() {
		return SELECT_COLUMNS;
	}

	/**
	 * Returns the database name of the table
	 * 
	 * @return String
	 * @see no.halogen.statements.Entity#getTableName()
	 */
	public String getTableName() {
		return TABLE_NAME;
	}

	/**
	 * Returns the columns and and a '?' to use in a prepared update statement
	 * 
	 * @return String
	 * @see no.halogen.statements.Entity#getUpdateValuesString()
	 */
	public String getUpdateValuesString() {
		return null; // no updates to this entity
	}

	/**
	 * Populates the data bean with the result from a query
	 * 
	 * @param rs
	 *            ResultSet
	 * @return Object
	 * @throws SQLException
	 * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
	 */
	public Object fetchResults(ResultSet rs) throws SQLException {
		return rs.getString("column_name");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see no.halogen.statements.ObjectStatementImpl#getInsertValues()
	 */
	/**
	 * Method getInsertValues
	 * 
	 * @return String
	 */
	public String getInsertValues() {
		return (INSERT_VALUES);
	}

	int seq = 0; // internal counter for sequence numbers in table
	int reportId;
	
	/**
	 * Method generateValues
	 * 
	 * @param pstmt
	 *            PreparedStatement
	 * @throws SQLException
	 */
	public void generateValues(PreparedStatement pstmt) throws SQLException {
		if (!getConditions().isEmpty()) {
			super.generateValues(pstmt);
		}
		else {
			String column = (String) getDataBean();
			pstmt.setInt(1, reportId);
			pstmt.setString(2, column);
			pstmt.setInt(3, seq++);
		}

	}

	public int getReportId() {
		return reportId;
	}

	public void setReportId(int reportId) {
		this.reportId = reportId;
	}

}
