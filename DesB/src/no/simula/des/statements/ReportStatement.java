package no.simula.des.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.simula.des.Report;
import no.simula.des.StudyType;
import no.halogen.statements.ObjectStatementImpl;

public class ReportStatement extends ObjectStatementImpl {
	/**
	 * Field log
	 */
	private static Log log = LogFactory.getLog(ReportStatement.class);

	/**
	 * Field INSERT_COLUMNS
	 */
	private final String INSERT_COLUMNS = "name, orderByTime, conditionOperator, user_id";

	/**
	 * Field KEY
	 */
	private final String KEY = "id";

	/**
	 * Field INSERT_VALUES
	 */
	private final String INSERT_VALUES = "(?, ?, ?, ?)";

	private final String SELECT_COLUMNS = "id, name, orderByTime, conditionOperator";

	/**
	 * Field TABLE_NAME
	 */
	private final String TABLE_NAME = "personalised_report";

	/**
	 * Field UPDATE_VALUES
	 */
	private final String UPDATE_VALUES = "name = ?, orderByTime = ?, conditionOperator = ?, user_id = ?";

	/**
	 * Returns the columns and the order they must appear in in the insert
	 * statement
	 * 
	 * @return String
	 * @see no.halogen.statements.Entity#getInsertColumnNames()
	 */
	public String getInsertColumnNames() {
		return (INSERT_COLUMNS);
	}

	/**
	 * Returns name of the id column
	 * 
	 * @return String
	 * @see no.halogen.statements.Entity#getKey()
	 */
	public String getKey() {
		return KEY;
	}

	/**
	 * Returns the columns that will be fetched in a select statement
	 * 
	 * @return String
	 * @see no.halogen.statements.Entity#getSelectColumnNames()
	 */
	public String getSelectColumnNames() {
		return SELECT_COLUMNS;
	}

	/**
	 * Returns the database name of the table
	 * 
	 * @return String
	 * @see no.halogen.statements.Entity#getTableName()
	 */
	public String getTableName() {
		return TABLE_NAME;
	}

	/**
	 * Returns the columns and and a '?' to use in a prepared update statement
	 * 
	 * @return String
	 * @see no.halogen.statements.Entity#getUpdateValuesString()
	 */
	public String getUpdateValuesString() {
		return UPDATE_VALUES;
	}

	/**
	 * Populates the data bean with the result from a query
	 * 
	 * @param rs
	 *            ResultSet
	 * @return Object
	 * @throws SQLException
	 * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
	 */
	public Object fetchResults(ResultSet rs) throws SQLException {

		Report r = new Report();

		r.setId(new Integer(rs.getInt("id")));
		r.setName(rs.getString("name"));
		r.setOrderByTime(rs.getBoolean("orderByTime"));
		r.setConditionOperator(rs.getString("conditionOperator"));

		return (r);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see no.halogen.statements.ObjectStatementImpl#getInsertValues()
	 */
	/**
	 * Method getInsertValues
	 * 
	 * @return String
	 */
	public String getInsertValues() {
		return (INSERT_VALUES);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see no.halogen.statements.ObjectStatementImpl#generateValues(java.sql.PreparedStatement)
	 */
	/**
	 * Method generateValues
	 * 
	 * @param pstmt
	 *            PreparedStatement
	 * @throws SQLException
	 */
	public void generateValues(PreparedStatement pstmt) throws SQLException {
		if (!getConditions().isEmpty()) {
			super.generateValues(pstmt);
		}
		else {
			Report entity = (Report) getDataBean();
			pstmt.setString(1, entity.getName());
			pstmt.setBoolean(2, entity.isOrderByTime());
			pstmt.setString(3, entity.getConditionOperator());
			pstmt.setString(4, entity.getUserId());
		}

	}

}
