/*
 * Created on 30.okt.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.simula.des.Link;

import no.halogen.statements.ObjectStatementImpl;

/**
 * @author Frode Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class StudyMaterialLinkStatement extends ObjectStatementImpl {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(StudyMaterialLinkStatement.class);

  /**
   * Field BEAN_NAME
   */
  private final String BEAN_NAME = "no.simula.des.Link";

  private final String INSERT_COLUMNS = "sml_sm_id, sml_url";

  /**
   * Field INSERT_VALUES
   */
  private final String INSERT_VALUES = "(?, ?)";

  /**
   * Field KEY
   */
  private final String KEY = "sml_sm_id";

  /**
   * Field SELECT_COLUMNS
   */
  private final String SELECT_COLUMNS = "sml_sm_id, sml_url";

  /**
   * Field TABLE_NAME
   */
  private final String TABLE_NAME = "sml_smlink";

  private final String UPDATE_VALUES = "sml_sm_id = ?, sml_url = ?";

  /**
   * Returns the columns and the order they must appear in in the insert statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  public String getInsertColumnNames() {
    return INSERT_COLUMNS;
  }

  /**
   * Returns name of the id column
   * 
   */
  public String getKey() {
    return KEY;
  }

  /**
   * Returns the columns that will be fetched in a select statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getSelectColumnNames()
   */
  public String getSelectColumnNames() {
    return SELECT_COLUMNS;
  }

  /**
   * Returns the database name of the table
   * 
   */
  public String getTableName() {
    return TABLE_NAME;
  }

  /**
   * Returns the columns and and a '?' to use in a prepared update statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  public String getUpdateValuesString() {
    return UPDATE_VALUES;
  }

  /** 
   * Populates the data bean with the result from a query
   * Since StudyMaterial is abstract, and implemented by Link, this method will populate Link from both sm_studymaterial and sml_smlink
   * 
   * @param rs ResultSet
   * @return Object
   * @throws SQLException
   * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
   */
  public Object fetchResults(ResultSet rs) throws SQLException {
    Link studyMaterialLink = null;

    try {
      if (getDataBean() != null) {
        studyMaterialLink = (Link) getDataBean().getClass().newInstance();
      } else {
        studyMaterialLink = new Link();
      }
    } catch (InstantiationException e) {
      log.error("fetchListResults() - Could not create new study material link data object");
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      log.error("fetchListResults() - Could not create new study material file data object");
      e.printStackTrace();
    }

    studyMaterialLink.setId(new Integer(rs.getInt("sml_sm_id")));
    studyMaterialLink.setDescription(rs.getString("sm_description"));
    studyMaterialLink.setUrl(rs.getString("sml_url"));

    return (studyMaterialLink);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#getInsertValues()
   */
  /**
   * Method getInsertValues
   * @return String
   */
  public String getInsertValues() {
    return (INSERT_VALUES);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#generateValues(java.sql.PreparedStatement)
   */
  /**
   * Method generateValues
   * @param pstmt PreparedStatement
   * @throws SQLException
   */
  public void generateValues(PreparedStatement pstmt) throws SQLException {
    if (!getConditions().isEmpty()) {
      super.generateValues(pstmt);
    } else {
      Link link = (Link) getDataBean();

      pstmt.setInt(1, link.getId().intValue());
      pstmt.setString(2, link.getUrl());
    }
  }

}
