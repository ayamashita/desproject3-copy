// -------------------------------------------------------------------
// moveSelectedOptions(select_object,select_object)
//  This function moves options between select boxes. Works best with
//  multi-select boxes to create the common Windows control effect.
//  Passes all selected values from the first object to the second
//  object and re-sorts each box.
//  You can also put this into the <SELECT> object as follows:
//    onDblClick="moveSelectedOptions(this,this.form.target)
//  This way, when the user double-clicks on a value in one box, it
//  will be transferred to the other (in browsers that support the
//  onDblClick() event handler).
// -------------------------------------------------------------------
function moveSelectedOptions(from, to) {
	// Unselect matching options, if required
	if (arguments.length > 3) {
		var regex = arguments[3];
		if (regex != "") {
			unSelectMatchingOptions(from, regex);
		}
	}
	// Move them over
	for (var i = 0; i < from.options.length; i++) {
		var o = from.options[i];
		if (o.selected) {
			to.options[to.options.length] = new Option(o.text, o.value, false, false);
		}
	}
	// Delete them from original
	for (var i = (from.options.length - 1); i >= 0; i--) {
		var o = from.options[i];
		if (o.selected) {
			from.options[i] = null;
		}
	}
	from.selectedIndex = -1;
	to.selectedIndex = -1;
}

// -------------------------------------------------------------------
// moves all selected items in the list up
// -------------------------------------------------------------------
function moveSelectedOptionsUp(list) {
	// Move item up
	for (var i = 1; i < list.options.length; i++) {
		var o = list.options[i];
		if (o.selected) {
			var tmpText = list.options[i].text;
			var tmpValue = list.options[i].value;
			list.options[i].text = list.options[i - 1].text;
			list.options[i].value = list.options[i - 1].value;
			list.options[i].selected = false;
			list.options[i - 1].text = tmpText;
			list.options[i - 1].value = tmpValue;
			list.options[i - 1].selected = true;
		}
	}
}
// -------------------------------------------------------------------
// moves all selected items in the list down
// -------------------------------------------------------------------
function moveSelectedOptionsDown(list) {
	// Move item up
	for (var i = list.options.length - 2; i >= 0; i--) {
		var o = list.options[i];
		if (o.selected) {
			var tmpText = list.options[i].text;
			var tmpValue = list.options[i].value;
			list.options[i].text = list.options[i + 1].text;
			list.options[i].value = list.options[i + 1].value;
			list.options[i].selected = false;
			list.options[i + 1].text = tmpText;
			list.options[i + 1].value = tmpValue;
			list.options[i + 1].selected = true;
		}
	}
}
// -------------------------------------------------------------------
// selectAllOptions(select_object)
//  This function takes a select box and selects all options (in a
//  multiple select object). This is used when passing values between
//  two select boxes. Select all options in the right box before
//  submitting the form so the values will be sent to the server.
// -------------------------------------------------------------------
function selectAllOptions(obj) {
	for (var i = 0; i < obj.options.length; i++) {
		obj.options[i].selected = true;
	}
}

