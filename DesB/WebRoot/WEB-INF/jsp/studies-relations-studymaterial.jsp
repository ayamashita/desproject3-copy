<%@page import="no.simula.des.presentation.web.forms.StudyStudyMaterialRelationForm"%>
<%@include file="include/include-top.jsp"%>
<%
  StudyStudyMaterialRelationForm form = (StudyStudyMaterialRelationForm)request.getSession().getAttribute(Constants.FORM_STUDY_STUDYMATERIAL_RELATION);
%>

  <h1>Attach study material to study</h1>

  <html:form action="/adm/studies/relations/studymaterial" enctype="multipart/form-data" method="post">

  <%-- TODO: This view has become severly non-userfriendly due to stacking of funtionallity --%>

  <%-- The main table that divides the page into two columns --%>
  <table class="layout">
    <tr>

      <%-- Column One --%>
      <td>
        registered study material
        <halogen:table name="<%= Constants.TABLE_STUDY_RELATIONS_STUDYMATERIAL %>"/>
      </td>
      <%-- End of Column One --%>

      <%-- Column Two --%>
      <td class="justified">
        attached study material
        <table class="container">
          <logic:iterate name="<%= Constants.FORM_STUDY_STUDYMATERIAL_RELATION %>" property="attachedStudyMaterial" id="material" type="no.simula.des.StudyMaterial">
            <tr>
              <td>
                <html:link
                  action="/adm/studies/relations/studymaterial?edit=true"
                  paramId="id"
                  paramName="material"
                  paramProperty="id"
                >
                  <%
                  if(material.getDescription().length() > 30) {
                    out.write("<span title=\"" + material.getDescription() + "\">");
                    out.write(material.getDescription().substring(0, 30) + "...");
                    out.write("</span>");
                  }
                  else {
                    out.write(material.getDescription());
                  } %>
                </html:link>
              </td>
              <td class="button">
                <html:link action="/adm/studies/relations/studymaterial" paramId="detach" paramName="material" paramProperty="id">
                  [ <bean:message key="studymaterial.table.button.detach"/> ]
                </html:link>
            </tr>
          </logic:iterate>
          <logic:empty name="<%= Constants.FORM_STUDY_STUDYMATERIAL_RELATION %>" property="attachedStudyMaterial">
            <tr>
              <td><i>--- empty ---</i></td>
            </tr>
          </logic:empty>
        </table>
      </td>
      <%-- End of Column Two --%>
      
    </tr>
  </table>

  <%-- End of Main Table --%>

  <logic:messagesPresent>
    <table class="error">
      <tr>
        <td>
          <ul>
            <html:messages id="error">
              <li><bean:write name="error"/></li>
            </html:messages>
          </ul>
        </td>
      </tr>
    </table>
  </logic:messagesPresent>

  <%-- Copied from studymaterial-edit.jsp --%>
    
    attach new study material
    <table class="container">
      <tr>
        <td>description</td>
        <td><html:text property="studyMaterial.description" size="36"/></td>
      </tr>
      <tr>
        <td>type</td>
        <td>
          <logic:iterate name="<%= Constants.FORM_STUDY_STUDYMATERIAL_RELATION %>" property="studyMaterial.types" id="type" type="no.simula.des.StudyMaterial">
            <html:radio property="studyMaterial.type" idName="type" value="type" onclick="<%= ( "changeType('" + type.getType() + "')" ) %>">
              <bean:message key="<%= ("studymaterial.type." + type.getType()) %>"/>
            </html:radio>
          </logic:iterate>
        </td>
      </tr>
      <tr>
        <td id="file" colspan="2">
          file
          <table class="container">
            <% if((form.getStudyMaterial().getRemoteFile() != null && form.getStudyMaterial().getRemoteFile().getFileSize() > 0) || form.getStudyMaterial().getName() != null) { %>
              <tr>
                <td colspan="2">
                  <% if(form.getStudyMaterial().getRemoteFile() != null && form.getStudyMaterial().getRemoteFile().getFileSize() > 0) { %>
                    <b><bean:write name="<%= Constants.FORM_STUDY_STUDYMATERIAL_RELATION %>" property="studyMaterial.remoteFile.fileName"/></b>
                  <% } else {  %>
                    <b><bean:write name="<%= Constants.FORM_STUDY_STUDYMATERIAL_RELATION %>" property="studyMaterial.name"/></b>
                  <% } %>
                </td>
              </tr>
              <tr>
                <td colspan="2" class="button">
                  <html:submit property="studyMaterial.remove" value="remove file"/>
                </td>
              </tr>

            <% } else { %>

<%-- 
              "-- empty --" only confuses in this view
              <tr>
                <td colspan="2">
                  <i>--- empty ---</i>
                </td>
              </tr>
--%>
              <tr>
                <td colspan="2" class="button">
                  <html:file property="studyMaterial.remoteFile" size="36"/>
                </td>
              </tr>
            <% } %>
            <tr>
              <td>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td id="url" colspan="2">
          external resource
          <table class="container">
            <tr>
              <td>url</td>
              <td>
                <html:text property="studyMaterial.url" size="42"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <html:submit property="studyMaterial.attach" value="save new and attach"/>
        </td>
      </tr>
    </table>
    <%--html:cancel value="cancel"/--%>
    <html:submit property="ok" value="finished"/>
  </html:form>


<%@include file="include/include-bottom.jsp"%>