<%@page contentType="text/html" import="java.util.Calendar, org.apache.struts.util.RequestUtils, java.text.SimpleDateFormat"%>
<%@taglib prefix="logic" uri="/WEB-INF/struts-logic.tld"%>
<%@taglib prefix="bean" uri="/WEB-INF/struts-bean.tld"%>
<%@taglib prefix="html" uri="/WEB-INF/struts-html.tld"%>
<%@taglib prefix="halogen" uri="/WEB-INF/halogen-presentation.tld"%>
<%@taglib prefix="htgui" uri="/WEB-INF/htgui.tld"%>

<%
  String context = request.getContextPath();
  String yearString = request.getParameter("year");
  String monthString = request.getParameter("month");
  String dayString = request.getParameter("day");
  int activeYear;
  int activeMonth;
  int activeDay;
  Calendar activeDate = Calendar.getInstance();
  if(yearString == null || monthString == null || dayString == null ||
    yearString.length() == 0 || monthString.length() == 0 || dayString.length() == 0
  ) {
    activeDate.setTimeInMillis(System.currentTimeMillis());
    activeYear = activeDate.get(Calendar.YEAR);
    activeMonth = activeDate.get(Calendar.MONTH) + 1;
    activeDay = activeDate.get(Calendar.DAY_OF_MONTH);
  }
  else {
    activeYear = Integer.parseInt(yearString);
    activeMonth = Integer.parseInt(monthString);
    activeDay = Integer.parseInt(dayString);
    activeDate.set(activeYear, activeMonth - 1, activeDay);
  }
%>

<html:html>

  <head>
    <link rel="stylesheet" href="<%= context %>/simula.css" type="text/css"/>
    <style>
      a:link, a:visited, a:hover { color: #000000; }
    </style>
    <script>
      function onCalendarDaySelect(name, year, month, day) {
        name = "<%= request.getParameter("name") %>";
        var yearName = name + "Year";
        var monthName = name + "Month";
        var dayName = name + "Day";
        window.opener.document.forms(1).elements[yearName].value = year;
        window.opener.document.forms(1).elements[monthName].value = month;
        window.opener.document.forms(1).elements[dayName].value = day;
        window.close();
      }
    </script>
  </head>

  <body style="background-color: #f5f5f5">
    <html:form action="/calendar" method="get">
      <input type="hidden" name="name" value="<%= request.getParameter("name") %>"/>
      <input type="hidden" name="day" value="1"/>
      <select name="month" onChange="submit()">
     <% Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM");
        for(int i = 1; i < 13; i++) {
          cal.set(Calendar.MONTH, i-1);
          String month = sdf.format(cal.getTime());
          String selected = (activeMonth == i) ? "selected " : "";
          out.println("<option " + selected + " value=\"" + i + "\">" + month + "</option>");
        } %>
      </select>
      <select name="year" onChange="submit()">
     <% 
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        for(int i = 1990; i <= (currentYear + 5); i++) {
          String selected = (activeYear == (i)) ? "selected " : "";
          out.println("<option " + selected + " value=\"" + i + "\">" + i + "</option>");
        } %>
      </select>
    </html:form>
    <htgui:calendar visibleDate="<%= activeDate.getTime() %>" enableDaySelection="true"/>
  </body>
</html:html>