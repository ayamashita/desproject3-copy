<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="no.simula.des.reports.AvailableColumns"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>

<%@include file="include/include-top.jsp"%>

<script language="JavaScript" src="<%=request.getContextPath() %>/selectbox.js"></script>
<script language="JavaScript">
	function checkForm() {
		var validated = true;
		var message = "Errors:\n";
		if (document.getElementById("name").value == '') {
			message += "Name of the report is mandatory.\n";
			validated = false;
		}
		
		if (document.getElementById("columnsSel").options.length == 0) {
			message += "At least one column must be selected.\n";
			validated = false;
		}
		
		if (!validated) {
			window.alert(message);
		}
		
		return validated;
	}
</script>


<h1>
	Edit report
</h1>

<html:form action="/reports/edit">
	<html:hidden property="action" value="save"/>
	<html:hidden property="id"/>
	<table class="layout">
		<tr>
			<td class="bodytext-bold">
				Report name
			</td>
			<td class="bodytext">
				<html:text property="name"/>
			</td>
		</tr>
		<tr>
			<td class="bodytext-bold">
				Columns
			</td>
			<td class="bodytext">
				<table>
					<tr>
						<td>
							<bean:define id="selectedColumns" name="report-edit" property="columnsSel" type="String[]"/>
							<select
								ondblclick="moveSelectedOptions(this.form['columnsAll'],this.form['columnsSel'],true)"
								style="width: 100%" class="input" name="columnsAll" size="8"
								multiple>
								<%
									for (Iterator iterator = AvailableColumns.getValues().iterator(); iterator.hasNext();) {
										AvailableColumns col = (AvailableColumns) iterator.next();
										if (!Arrays.asList(selectedColumns).contains(col.getName())) {
								%>

											<option value="<%=col.getName()%>"><%=col.getName()%></</option>
								<%
										}
									}
								%>
							</select>
						</td>
						<td>
							<input type="button" value="->"
								onclick="moveSelectedOptions(this.form['columnsAll'],this.form['columnsSel'],false)">
							<br>
							<br>
							<input type="button" value="<-"
								onclick="moveSelectedOptions(this.form['columnsSel'],this.form['columnsAll'],false)">
						</td>
						<td>
							<select id="columnsSel"
								ondblclick="moveSelectedOptions(this.form['columnsSel'],this.form['columnsAll'],true)"
								style="width: 100%" class="input" name="columnsSel" size="8"
								multiple="true">
								<%
									for (int i=0; i<selectedColumns.length; i++) {
										String col = (String) selectedColumns[i];
								%>

										<option value="<%=col%>"><%=col%></option>
								<%
									}
								%>
							</select>
						</td>
						<td>
							<input type="button" value="Up"
								onclick="moveSelectedOptionsUp(this.form['columnsSel'])">
							<br>
							<br>
							<input type="button" value="Down"
								onclick="moveSelectedOptionsDown(this.form['columnsSel'])">
						</td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>
			<td class="bodytext-bold">
				Conditions
			</td>
			<td class="bodytext">
				<table>
					<tr>
						<td>
							<select
								ondblclick="moveSelectedOptions(this.form['responsiblesAll'],this.form['responsiblesSel'],true)"
								style="width: 100%" class="input" name="responsiblesAll"
								size="8" multiple>
								<%
									List selectedResponsibles = new ArrayList(); // this list is used to prevent reading the people details repeatedly
								%>
								<logic:iterate id="resp" type="Person" property="responsiblesAll" name="report-edit">
								<%
									if (!selectedResponsibles.contains(resp.getId())) {
								%>
										<option value="<%=resp.getId()%>"><%=resp.getFamilyAndFirstName()%></option>
								<%
									}
									else {
										selectedResponsibles.add(resp);
									}
								%>
								</logic:iterate>
							</select>
						</td>
						<td>
							<input type="button" value="->"
								onclick="moveSelectedOptions(this.form['responsiblesAll'],this.form['responsiblesSel'],true)">
							<br>
							<br>
							<input type="button" value="<-"
								onclick="moveSelectedOptions(this.form['responsiblesSel'],this.form['responsiblesAll'],true)">
						</td>
						<td>
							<select id="responsiblesSel"
								ondblclick="moveSelectedOptions(this.form['responsiblesSel'],this.form['responsiblesAll'],true)"
								style="width: 100%" class="input" name="responsiblesSel"
								size="8" multiple>
								<%
									for (Iterator iterator = selectedResponsibles.iterator(); iterator.hasNext();) {
										Person resp = (Person) iterator.next();
								%>
								<option value="<%=resp.getId()%>"><%=resp.getFamilyAndFirstName()%></option>
								<%
									}
								%>
							</select>
						</td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>
			<td class="bodytext-bold">
				Operator
			</td>
			<td class="bodytext">
				<html:select property="conditionOperator">
					<html:option value="AND">AND</html:option>
					<html:option value="OR">OR</html:option>
				</html:select>
			</td>
		</tr>
		<tr>
			<td class="bodytext-bold">
				Order by time
			</td>
			<td class="bodytext">
				<html:checkbox property="orderByTime"/>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="bodytext">
				<input type="button" value="Save"
					onclick="selectAllOptions(this.form['columnsSel']); selectAllOptions(this.form['responsiblesSel']); this.form.submit();"
					>
				<input type="button" value="Cancel">
			</td>
		</tr>

	</table>
</html:form>

<%@include file="include/include-bottom.jsp"%>
