<%@include file="include/include-top.jsp"%>

  <h1>Attach publications to study</h1>

  <html:form action="/adm/studies/relations/publications" method="post">

  <%-- The main table that divides the page into two columns --%>
  <table class="layout">
    <tr>

      <%-- Column One --%>
      <td>
        available publications
        <halogen:table name="<%= Constants.TABLE_STUDY_RELATIONS_PUBLICATIONS %>"/>
      </td>
      <%-- End of Column One --%>

      <%-- Column Two --%>
      <td class="justified">
        attached publications
        <table class="container">
          <logic:iterate name="<%= Constants.FORM_STUDY_PUBLICATION_RELATION %>" property="attachedPublications" id="publication" type="no.simula.Publication">
            <tr>
              <td>
                <html:link
                  href="<%=publication.getUrl() %>" 
                >
                  <%
                  if(publication.getTitle().length() > 30) {
                    out.write("<span title=\"" + publication.getTitle() + "\">");
                    out.write(publication.getTitle().substring(0, 30) + "...");
                    out.write("</span>");
                  }
                  else {
                    out.write(publication.getTitle());
                  } %>
                </html:link>
              </td>
              <td class="button">
                <html:link action="/adm/studies/relations/publications" paramId="detach" paramName="publication" paramProperty="id">
                  [ <bean:message key="publications.table.button.detach"/> ]
                </html:link>
            </tr>
          </logic:iterate>
          <logic:empty name="<%= Constants.FORM_STUDY_PUBLICATION_RELATION %>" property="attachedPublications">
            <tr>
              <td><i>--- empty ---</i></td>
            </tr>
          </logic:empty>
        </table>
      </td>
      <%-- End of Column Two --%>
      
    </tr>
  </table>
  <%-- End of Main Table --%>

  <html:submit property="ok" value="ok"/>
  <html:cancel value="cancel"/>

 </html:form>

<%@include file="include/include-bottom.jsp"%>
