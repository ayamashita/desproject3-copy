<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="no.simula.des.reports.AvailableColumns"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@include file="include/include-top.jsp"%>

<bean:define id="col" name="report-list" property="displaytagCollection"/>

<display:table list="<%=col%>">
	<% String url = "/do/reports/edit?action=edit"; %>
	<display:column property="name" url="<%=url %>" paramId="id" paramProperty="id"/>
	<display:column title="run" value="run report"/>
	<display:column title="delete" value="delete report"/>
</display:table>

<%@include file="include/include-bottom.jsp"%>
