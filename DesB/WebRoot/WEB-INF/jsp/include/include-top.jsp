<%@page contentType="text/html"%>
<%@page import="org.apache.struts.util.RequestUtils"%>
<%@page import="org.apache.struts.config.impl.ModuleConfigImpl"%>
<%@page import="no.simula.des.presentation.web.actions.Constants"%>
<%@page import="no.simula.Person"%>

<%@taglib prefix="logic" uri="/WEB-INF/struts-logic.tld"%>
<%@taglib prefix="bean" uri="/WEB-INF/struts-bean.tld"%>
<%@taglib prefix="html" uri="/WEB-INF/struts-html.tld"%>
<%@taglib prefix="halogen" uri="/WEB-INF/halogen-presentation.tld"%>
<%
  String context = request.getContextPath();
//  Person person = (Person)request.getSession().getAttribute(no.simula.struts.UserInSessionRequestProcessor.PERSON_KEY);
//  String login = null;
//  if(person != null) {
//    login = person.getFirstAndFamilyName();
//  }
  String loginURL = request.getContextPath() + "/do/adm/welcome";
  //String loginURL = "https://" + request.getServerName() + context + "/do/adm/welcome";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//Dtd HTML 4.0 Transitional//EN">

<html>
  <head>
    <title>[ simula . research laboratory ]</title>
    <link media="screen" href="<%= context %>/simula.css" rel="stylesheet">

    <script>
      function openWindow(prefix) {    
        year = prefix + "Year";
        month = prefix + "Month";
        day = prefix + "Day";
        
        url = "<%= context %>/do/calendar";
        url += "?year=" + document.forms(1).elements[year].value;
 		url += "&amp;month=" + document.forms(1).elements[month].value;
        url += "&amp;day=" + document.forms(1).elements[day].value;
        url += "&amp;name=" + prefix;
              
        window.open(url, 'Calendar', 'width=200,height=250,toolbar=no,menubar=no,location=no,scrollbars=no');
      }
    </script>

    <!-- S T A R T  C O P I E D  F R O M  S I M U L A W E B -->
    <script language="JavaScript">
    <!--
    function MM_preloadImages() { //v3.0
      var d = document;
      if(d.images) { 
        if(!d.MM_p) {
          d.MM_p = new Array();
        }
        var i,j = d.MM_p.length, a = MM_preloadImages.arguments;
        for(i = 0; i < a.length; i++) {
          if(a[i].indexOf("#")!=0) { 
            d.MM_p[j] = new Image;
            d.MM_p[j++].src = a[i];
          }
        }
      }
    }

    function MM_swapImgRestore() { //v3.0
      var i,x,a = document.MM_sr;
      for(i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) {
        x.src = x.oSrc;
      }
    }

    function MM_findObj(n, d) { //v4.0
      var p,i,x;
      if(!d) {
        d=document;
      }
      if((p = n.indexOf("?")) > 0 && parent.frames.length) {
        d = parent.frames[n.substring(p + 1)].document;
        n = n.substring(0, p);
      }
      if(!(x = d[n]) && d.all) {
        x = d.all[n]; 
      }
      for(i = 0; !x && i < d.forms.length; i++) {
        x = d.forms[i][n];
      }
      for(i = 0; !x && d.layers && i < d.layers.length; i++) {
        x = MM_findObj(n, d.layers[i].document);
      }
      if(!x && document.getElementById) {
        x = document.getElementById(n);
      }
      return x;
    }

    function MM_swapImage() { //v3.0
      var i,j = 0,x,a = MM_swapImage.arguments; 
      document.MM_sr = new Array;
      for(i = 0; i < (a.length - 2); i += 3) {
        if((x = MM_findObj(a[i])) != null) {
          document.MM_sr[j++] = x;
          if(!x.oSrc) {
            x.oSrc = x.src;
          }
          x.src = a[i + 2];
        }
      }
    }
    //-->
    </script>
    <script>
      <!--
      CSInit = new Array;
      function CSScriptInit() {
      if(typeof(skipPage) != "undefined") { if(skipPage) return; }
      idxArray = new Array;
      for(var i=0;i<CSInit.length;i++)
              idxArray[i] = i;
      CSAction2(CSInit, idxArray);}
      CSAg = window.navigator.userAgent; CSBVers = parseInt(CSAg.charAt(CSAg.indexOf("/")+1),10);
      CSIsW3CDOM = ((document.getElementById) && !(IsIE()&&CSBVers<6)) ? true : false;
      function IsIE() { return CSAg.indexOf("MSIE") > 0;}
      function CSIEStyl(s) { return document.all.tags("div")[s].style; }
      function CSNSStyl(s) { if (CSIsW3CDOM) return document.getElementById(s).style; else return CSFindElement(s,0);  }
      CSIImg=false;
      function CSInitImgID() {if (!CSIImg && document.images) { for (var i=0; i<document.images.length; i++) { if (!document.images[i].id) document.images[i].id=document.images[i].name; } CSIImg = true;}}
      function CSFindElement(n,ly) { if (CSBVers<4) return document[n];
              if (CSIsW3CDOM) {CSInitImgID();return(document.getElementById(n));}
              var curDoc = ly?ly.document:document; var elem = curDoc[n];
              if (!elem) {for (var i=0;i<curDoc.layers.length;i++) {elem=CSFindElement(n,curDoc.layers[i]); if (elem) return elem; }}
              return elem;
      }
      function CSGetImage(n) {if(document.images) {return ((!IsIE()&&CSBVers<5)?CSFindElement(n,0):document.images[n]);} else {return null;}}
      CSDInit=false;
      function CSIDOM() { if (CSDInit)return; CSDInit=true; if(document.getElementsByTagName) {var n = document.getElementsByTagName('DIV'); for (var i=0;i<n.length;i++) {CSICSS2Prop(n[i].id);}}}
      function CSICSS2Prop(id) { var n = document.getElementsByTagName('STYLE');for (var i=0;i<n.length;i++) { var cn = n[i].childNodes; for (var j=0;j<cn.length;j++) { CSSetCSS2Props(CSFetchStyle(cn[j].data, id),id); }}}
      function CSFetchStyle(sc, id) {
              var s=sc; while(s.indexOf("#")!=-1) { s=s.substring(s.indexOf("#")+1,sc.length); if (s.substring(0,s.indexOf("{")).toUpperCase().indexOf(id.toUpperCase())!=-1) return(s.substring(s.indexOf("{")+1,s.indexOf("}")));}
              return "";
      }
      function CSGetStyleAttrValue (si, id) {
              var s=si.toUpperCase();
              var myID=id.toUpperCase()+":";
              var id1=s.indexOf(myID);
              if (id1==-1) return "";
              s=s.substring(id1+myID.length+1,si.length);
              var id2=s.indexOf(";");
              return ((id2==-1)?s:s.substring(0,id2));
      }
      function CSSetCSS2Props(si, id) {
              var el=document.getElementById(id);
              if (el==null) return;
              var style=document.getElementById(id).style;
              if (style) {
                      if (style.left=="") style.left=CSGetStyleAttrValue(si,"left");
                      if (style.top=="") style.top=CSGetStyleAttrValue(si,"top");
                      if (style.width=="") style.width=CSGetStyleAttrValue(si,"width");
                      if (style.height=="") style.height=CSGetStyleAttrValue(si,"height");
                      if (style.visibility=="") style.visibility=CSGetStyleAttrValue(si,"visibility");
                      if (style.zIndex=="") style.zIndex=CSGetStyleAttrValue(si,"z-index");
              }
      }

      function CSClickReturn () {
              var bAgent = window.navigator.userAgent; 
              var bAppName = window.navigator.appName;
              if ((bAppName.indexOf("Explorer") >= 0) && (bAgent.indexOf("Mozilla/3") >= 0) && (bAgent.indexOf("Mac") >= 0))
                      return true; // dont follow link
              else return false; // dont follow link
      }

      function CSButtonReturn () {
              var bAgent = window.navigator.userAgent; 
              var bAppName = window.navigator.appName;
              if ((bAppName.indexOf("Explorer") >= 0) && (bAgent.indexOf("Mozilla/3") >= 0) && (bAgent.indexOf("Mac") >= 0))
                      return false; // follow link
              else return true; // follow link
      }

      CSIm=new Object();
      function CSIShow(n,i) {
              if (document.images) {
                      if (CSIm[n]) {
                              var img=CSGetImage(n);
                              if (img&&typeof(CSIm[n][i].src)!="undefined") {img.src=CSIm[n][i].src;}
                              if(i!=0) self.status=CSIm[n][3]; else self.status=" ";
                              return true;
                      }
              }
              return false;
      }
      function CSILoad(action) {
              im=action[1];
              if (document.images) {
                      CSIm[im]=new Object();
                      for (var i=2;i<5;i++) {
                              if (action[i]!='') {CSIm[im][i-2]=new Image(); CSIm[im][i-2].src=action[i];}
                              else CSIm[im][i-2]=0;
                      }
                      CSIm[im][3] = action[5];
              }
      }
      CSStopExecution=false;
      function CSAction(array) {return CSAction2(CSAct, array);}
      function CSAction2(fct, array) { 
              var result;
              for (var i=0;i<array.length;i++) {
                      if(CSStopExecution) return false; 
                      var aa = fct[array[i]];
                      if (aa == null) return false;
                      var ta = new Array;
                      for(var j=1;j<aa.length;j++) {
                              if((aa[j]!=null)&&(typeof(aa[j])=="object")&&(aa[j].length==2)){
                                      if(aa[j][0]=="VAR"){ta[j]=CSStateArray[aa[j][1]];}
                                      else{if(aa[j][0]=="ACT"){ta[j]=CSAction(new Array(new String(aa[j][1])));}
                                      else ta[j]=aa[j];}
                              } else ta[j]=aa[j];
                      }			
                      result=aa[0](ta);
              }
              return result;
      }
      CSAct = new Object;


      // -->
    </script>
    <script>
      <!--
      CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'search',/*URL*/'<%= context %>/images/topright-search-normal.png',/*URL*/'<%= context %>/images/topright-search-mouseover.png',/*URL*/'<%= context %>/images/topright-search-mouseover.png','Search');
      CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'advsearch',/*URL*/'<%= context %>/images/topright-advsearch-normal.png',/*URL*/'<%= context %>/images/topright-advsearch-mouseover.png',/*URL*/'<%= context %>/images/topright-advsearch-mouseover.png','Advanced Search');
      CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'home',/*URL*/'<%= context %>/images/menu-home-normal.png',/*URL*/'<%= context %>/images/menu-home-selected.png',/*URL*/'<%= context %>/images/menu-home-selected.png','Home Page');
      CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'news',/*URL*/'<%= context %>/images/menu-news-normal.png',/*URL*/'<%= context %>/images/menu-news-selected.png',/*URL*/'<%= context %>/images/menu-news-selected.png','News and press releases at Simula Research Laboratory');
      CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'publications',/*URL*/'<%= context %>/images/menu-publications-normal.png',/*URL*/'<%= context %>/images/menu-publications-selected.png',/*URL*/'<%= context %>/images/menu-publications-selected.png','Publications by Simula Research Laboratory');
      CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'projects',/*URL*/'<%= context %>/images/menu-projects-normal.png',/*URL*/'<%= context %>/images/menu-projects-selected.png',/*URL*/'<%= context %>/images/menu-projects-selected.png','');
      CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'research',/*URL*/'<%= context %>/images/menu-research-normal.png',/*URL*/'<%= context %>/images/menu-research-selected.png',/*URL*/'<%= context %>/images/menu-research-selected.png','Research Departments at Simula Research Laboratory');
      CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'innovation',/*URL*/'<%= context %>/images/menu-innovation-normal.png',/*URL*/'<%= context %>/images/menu-innovation-selected.png',/*URL*/'<%= context %>/images/menu-innovation-selected.png','Innovation at Simula Research Laboratory');
      CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'people',/*URL*/'<%= context %>/images/menu-people-normal.png',/*URL*/'<%= context %>/images/menu-people-selected.png',/*URL*/'<%= context %>/images/menu-people-selected.png','People that work at Simula Research Laboratory');
      CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'opportunities',/*URL*/'<%= context %>/images/menu-opportunities-normal.png',/*URL*/'<%= context %>/images/menu-opportunities-selected.png',/*URL*/'<%= context %>/images/menu-opportunities-selected.png','Opportunities for students and professionals at Simula Research Laboratory');
      CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'button',/*URL*/'<%= context %>/images/next-normal.png',/*URL*/'<%= context %>/images/next-mouseover.png',/*URL*/'<%= context %>/images/next-mouseover.png','Next 50 projects');
      CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'button1',/*URL*/'<%= context %>/images/back-normal.png',/*URL*/'<%= context %>/images/back-mouseover.png',/*URL*/'<%= context %>/images/back-mouseover.png','Back 50 projects');
      CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'finished',/*URL*/'<%= context %>/images/finished_projects-normal.png',/*URL*/'<%= context %>/images/finished_projects-mouseover.png',/*URL*/'<%= context %>/images/finished_projects-mouseover.png','All finished projects');
      CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'current',/*URL*/'<%= context %>/images/all_current_projects-normal.png',/*URL*/'<%= context %>/images/all_current_projects-mouseover.png',/*URL*/'<%= context %>/images/all_current_projects-mouseover.png','All current projects');
      // -->
    </script>
  </head>

  <body onload="CSScriptInit();MM_preloadImages('<%= context %>/images/menu-intranet-selected.png')">

    <table cellSpacing=0 cellPadding=0 width="100%" border=0>
      <script>
        <!--
        function Submit_Simple_Search() {
          if(document.simple_search.search_text.value){
            document.simple_search.submit();
          }
          else {
            alert("Please input text to search.");
          }
        }
        -->
      </script>

      <tbody>
      <tr height="87">
        <td vAlign="bottom" width="144" height="87"><a 
          href="http://www.simula.no/index.php"><img 
          src="<%= context %>/images/logo-part1.png" align="right" 
          border="0"></a></td>
        <td vAlign="top" height="87">
          <table height="87" cellSpacing="0" cellPadding="0" width="100%" border="0">
            <tbody>
            <tr>
              <td vAlign="bottom"><img 
                src="<%= context %>/images/logo-part2.png" align="left" border="0"></td>
              <td vAlign="bottom">
                <form name="simple_search" action="search.php" method="post">
                  <table cellSpacing="0" cellPadding="0" align="right" border="0">
                    <tbody>
                    <tr>
                      <td><input size="12" name="search_text"> <input type="hidden" 
                        value="n" name="search_advance"></td>
                    </tr>
                    <tr>
                      <td><a 
                        onmouseover="return CSIShow(/*CMP*/'search',1)" 
                        onclick="CSIShow(/*CMP*/'search',2);return CSButtonReturn();" 
                        onmouseout="return CSIShow(/*CMP*/'search',0)" 
                        href="javascript:onClink=Submit_Simple_Search();"><img 
                        height="25" alt="Search" 
                        src="<%= context %>/images/topright-search-normal.png" 
                        width="108" border="0" name="search"></a></td>
                    </tr>
                    <tr>
                      <td><a 
                        onmouseover="return CSIShow(/*CMP*/'advsearch',1)" 
                        onclick="CSIShow(/*CMP*/'advsearch',2);return CSButtonReturn()" 
                        onmouseout="return CSIShow(/*CMP*/'advsearch',0)" 
                        href="http://www.simula.no/search.php"><img height="29" 
                        alt="Advanced Search" 
                        src="<%= context %>/images/topright-advsearch-normal.png" 
                        width="108" border="0" name="advsearch"></a> 
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </form>
              </td>
            </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <tr>
      <td width="144" bgColor="#aeaeae"></td>

      <td bgColor="#aeaeae">
        <table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
          <tbody>
          <tr height="23">
            <td width="660"><a onmouseover="return CSIShow(/*CMP*/'home',1)" 
              onclick="CSIShow(/*CMP*/'home',2);return CSButtonReturn()" 
              onmouseout="return CSIShow(/*CMP*/'home',0)" 
              href="http://www.simula.no/index.php"><img height=23 alt="Home Page" 
              src="<%= context %>/images/menu-home-normal.png" 
              width="49" border="0" name="home"></a><a 
              onmouseover="return CSIShow(/*CMP*/'news',1)" 
              onclick="CSIShow(/*CMP*/'news',2);return CSButtonReturn()" 
              onmouseout="return CSIShow(/*CMP*/'news',0)" 
              href="http://www.simula.no/news.php"><img height="23" 
              alt="News and press releases at Simula Research Laboratory" 
              src="<%= context %>/images/menu-news-normal.png" 
              width="46" border="0" name="news"></a><a
              onmouseover="return CSIShow(/*CMP*/'publications',1)" 
              onclick="CSIShow(/*CMP*/'publications',2);return CSButtonReturn()" 
              onmouseout="return CSIShow(/*CMP*/'publications',0)" 
              href="http://www.simula.no/publication.php"><img height="23" 
              alt="Publications by Simula Research Laboratory" 
              src="<%= context %>/images/menu-publications-normal.png" 
              width="82" border="0" name="publications"></a><a
              onmouseover="return CSIShow(/*CMP*/'projects',1)" 
              onclick="return CSButtonReturn()" 
              onmouseout="return CSIShow(/*CMP*/'projects',0)" 
              href="http://www.simula.no/project.php#"><img height="23" 
              src="<%= context %>/images/menu-projects-normal.png" 
              width="60" border="0" name="projects"></a><a
              onmouseover="return CSIShow(/*CMP*/'research',1)" 
              onclick="CSIShow(/*CMP*/'research',2);return CSButtonReturn()" 
              onmouseout="return CSIShow(/*CMP*/'research',0)" 
              href="http://www.simula.no/department.php"><img height="23" 
              alt="Research Departments at Simula Research Laboratory" 
              src="<%= context %>/images/menu-research-normal.png" 
              width="64" border="0" name="research"></a><a
              onmouseover="return CSIShow(/*CMP*/'innovation',1)" 
              onclick="CSIShow(/*CMP*/'innovation',2);return CSButtonReturn()" 
              onmouseout="return CSIShow(/*CMP*/'innovation',0)" 
              href="http://www.simula.no/innovation.php"><img height="23" 
              alt="Innovation at Simula Research Laboratory" 
              src="<%= context %>/images/menu-innovation-normal.png" 
              width="72" border="0" name="innovation"></a><a
              onmouseover="return CSIShow(/*CMP*/'people',1)" 
              onclick="CSIShow(/*CMP*/'people',2);return CSButtonReturn()" 
              onmouseout="return CSIShow(/*CMP*/'people',0)" 
              href="http://www.simula.no/people.php"><img height="23" 
              alt="People that work at Simula Research Laboratory" 
              src="<%= context %>/images/menu-people-normal.png" 
              width="54" border="0" name="people"></a><a 
              onmouseover="return CSIShow(/*CMP*/'opportunities',1)" 
              onclick="CSIShow(/*CMP*/'opportunities',2);return CSButtonReturn()" 
              onmouseout="return CSIShow(/*CMP*/'opportunities',0)" 
              href="http://www.simula.no/opportunity.php"><img height="23" 
              alt="Opportunities for students and professionals at Simula Research Laboratory" 
              src="<%= context %>/images/menu-opportunities-normal.png" 
              width="88" border="0" name="opportunities"></a><img
              border="0" src="<%= context %>/images/menu-studies-selected.png" width="58" height="23"></td>
            <td height=23><img height="23" 
              src="<%= context %>/images/menu-all-grey.png" 
              width="78"><a href="https://www.simula.no/intranet/"><img 
              onmouseover="MM_swapImage('Image1','','<%= context %>/images/menu-intranet-selected.png',1)" 
              onmouseout="MM_swapImgRestore()" height="23" 
              src="<%= context %>/images/menu-intranet-normal.png" 
              width="59" border="0" name="Image1"></a></td>
            </tr>
            </tbody>
          </table>
        </td>
      </tr>

      <!-- E N D  C O P I E D  F R O M  S I M U L A W E B -->

      <tr bgColor="#f5f5f5">
        <td width="144">&nbsp;</td>
        <td>
          <p class="path">
            studies &gt;
            <halogen:historypath/>
          </p>
        </td>
      </tr>
      <tr bgColor="#f5f5f5" height="350">
        <td width="134" height="350">&nbsp;</td>
        <td height="350">
          <table height="100%" cellPadding=0 width="100%" border="0">
            <tbody>
            <tr>
              <td vAlign="top">

                <table class="layout" border="0" cellpadding="5" cellspacing="0">
                  <tr>
                    <td width="100%">
