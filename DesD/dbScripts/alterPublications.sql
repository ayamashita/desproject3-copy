alter table publication_study modify column publication_id varchar(255);
alter table temp_publication_study modify column publication_id varchar(255);
drop table publication;

# drop also these unused tables?
#drop table publication_trustee
#drop table publication_author
#drop table publication_department
#drop table publication_trustee