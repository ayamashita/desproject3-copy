alter table people_study modify column people_id varchar(255);
alter table temp_people_study modify column people_id varchar(255);
drop table people;
alter table people_role modify column people_id varchar(250);

# drop also these unused tables?
#drop table people_course
#drop table people_link

