<%@page import="com.tec.des.http.WebKeys"%>
<%@page import="com.tec.des.http.WebConstants"%>
<%@ taglib uri="/util" prefix="util" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<jsp:include page="top.jsp"/>

<!-- Start navigation text -->
<TABLE cellspacing="0" cellpadding="0" width="96%">
    <TR>
        <TD class="path" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Personalised Reports</TD>
        <TD align="right"><A href="JavaScript:help('help.html#reports')" onmouseover="document.help.src='images/Help.png'" onmouseout="document.help.src='images/Help_gray.png'"><IMG height="18" width="18" border="0" name="help" src="images/Help_gray.png"></A></TD>
    </TR>
</TABLE>
<!-- End navigation text -->
<jsp:include page="messages.jsp"/>

<display:table name="<%=WebKeys.REQUEST_BEAN_DISPLAYTAG_LIST %>">
	<% String url = WebConstants.FRONTCONTROLLER_URL_BASE +"?" + WebKeys.REQUEST_PARAM_USECASE + "=" + WebConstants.USECASE_REPORT_EDIT; %>
	<display:column property="name" url="<%=url %>" paramId="<%=WebKeys.REQUEST_PARAM_REPORT_ID %>" paramProperty="id" class="bodytext" headerClass="bodytext-bold"/>
	<% url = WebConstants.FRONTCONTROLLER_URL_BASE +"?" + WebKeys.REQUEST_PARAM_USECASE + "=" + WebConstants.USECASE_REPORT_RUN; %>
	<display:column title="run" value="run report" url="<%=url %>" paramId="<%=WebKeys.REQUEST_PARAM_REPORT_ID %>" paramProperty="id" class="bodytext" headerClass="bodytext-bold"/>
	<% url = WebConstants.FRONTCONTROLLER_URL_BASE +"?" + WebKeys.REQUEST_PARAM_USECASE + "=" + WebConstants.USECASE_REPORT_DELETE; %>
	<display:column title="delete" value="delete report" url="<%=url %>" paramId="<%=WebKeys.REQUEST_PARAM_REPORT_ID %>" paramProperty="id" class="bodytext" headerClass="bodytext-bold"/>
</display:table>

<jsp:include page="bottom.html"/>