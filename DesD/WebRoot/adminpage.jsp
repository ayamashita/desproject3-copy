<%@ page import="com.tec.des.http.*" %>
<%@ page import="com.tec.des.util.Constants" %>
<%@ taglib uri="/util" prefix="util" %>
<jsp:include page="top.jsp"/>
<jsp:useBean id="adminText" class="java.lang.String" scope="request"/> 
<!-- Start navigation text -->
<TABLE cellspacing="0" cellpadding="0" width="98%">
<TR><TD class="path" valign="top">&nbsp;&nbsp;&nbsp;administration</TD>
<TD align="right"><A href="JavaScript:help('help.html#administrationpage')" onmouseover="document.help.src='images/Help.png'" onmouseout="document.help.src='images/Help_gray.png'"><IMG height="18" width="18" border="0" name="help" src="images/Help_gray.png"></A></TD>
</TR>
</TABLE>
<!-- End navigation text -->
<TABLE cellspacing="4" cellpadding="4">
<jsp:include page="messages.jsp"/>
<%=adminText%>
</TABLE> 
<jsp:include page="bottom.html"/>