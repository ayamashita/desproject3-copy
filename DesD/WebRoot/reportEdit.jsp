<%@page import="com.tec.des.dto.ReportDTO"%>
<%@page import="com.tec.des.http.WebKeys"%>
<%@page import="com.tec.des.http.WebConstants"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.tec.des.report.AvailableColumns"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tec.des.dto.StudyResponsibleDTO"%>

<%@ taglib uri="/util" prefix="util" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<jsp:include page="top.jsp"/>

<script language="JavaScript" src="selectbox.js"></script>
<script language="JavaScript">
	function checkForm() {
		var validated = true;
		var message = "Errors:\n";
		if (document.getElementById("name").value == '') {
			message += "Name of the report is mandatory.\n";
			validated = false;
		}
		
		if (document.getElementById("columns_sel").options.length == 0) {
			message += "At least one column must be selected.\n";
			validated = false;
		}
		
		if (!validated) {
			window.alert(message);
		}
		
		return validated;
	}
</script>

<% 
	ReportDTO report = (ReportDTO) request.getAttribute(WebKeys.REQUEST_BEAN_REPORT);
	if (report == null) {
		report = new ReportDTO();
	}
%>

<!-- Start navigation text -->
<TABLE cellspacing="0" cellpadding="0" width="96%">
    <TR>
        <TD class="path" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Personalised Reports &gt; <%= report.getName()%></TD>
        <TD align="right"><A href="JavaScript:help('help.html#reports')" onmouseover="document.help.src='images/Help.png'" onmouseout="document.help.src='images/Help_gray.png'"><IMG height="18" width="18" border="0" name="help" src="images/Help_gray.png"></A></TD>
    </TR>
</TABLE>
<!-- End navigation text -->
<jsp:include page="messages.jsp"/>

<form action="<%=WebConstants.FRONTCONTROLLER_URL %>">
	<input type="hidden" name="<%= WebKeys.REQUEST_PARAM_USECASE %>" value="<%=WebConstants.USECASE_REPORT_SAVE%>">
	<input type="hidden" name="<%= WebKeys.REQUEST_PARAM_REPORT_ID %>" value="<%= report.getId()%>">
	<table width="100%">
		<tr>
			<td class="bodytext-bold">
				Report name
			</td>
			<td class="bodytext">
				<input id="name" type="text" name="name" value="<%= report.getName()%>">
			</td>
		</tr>
		<tr>
			<td class="bodytext-bold">
				Columns
			</td>
			<td class="bodytext">
				<table>
					<tr><td>
						<select ondblclick="moveSelectedOptions(this.form['columns_all'],this.form['columns_sel'],true)" style="width:100%" class="input" name="columns_all" size="8" multiple>
							<%
								for (Iterator iterator = AvailableColumns.getValues().iterator(); iterator.hasNext();) {
									AvailableColumns col = (AvailableColumns) iterator.next();
							 		if (!report.getSelectedColumns().contains(col.getName())) {
							 %>
							 
										<option value="<%=col.getName() %>"><%=col.getName() %></option>
							<% 
									}
								} 
							%>
						</select>
					</td><td>
						<input type="button" value="->" onclick="moveSelectedOptions(this.form['columns_all'],this.form['columns_sel'],false)"><br><br>
						<input type="button" value="<-" onclick="moveSelectedOptions(this.form['columns_sel'],this.form['columns_all'],false)">
					</td><td>
						<select id="columns_sel" ondblclick="moveSelectedOptions(this.form['columns_sel'],this.form['columns_all'],true)" style="width:100%" class="input" name="columns_sel" size="8" multiple>
							<%
								for (Iterator iterator = report.getSelectedColumns().iterator(); iterator.hasNext();) {
									String col = (String) iterator.next();
							 %>
							 
										<option value="<%=col %>"><%=col %></option>
							<% 
								} 
							%>
						</select>
					</td><td>
						<input type="button" value="Up" onclick="moveSelectedOptionsUp(this.form['columns_sel'])"><br><br>
						<input type="button" value="Down" onclick="moveSelectedOptionsDown(this.form['columns_sel'])">
					</td></tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="bodytext-bold">
				Conditions
			</td>
			<td class="bodytext">
				<table>
					<tr><td>
						<select ondblclick="moveSelectedOptions(this.form['responsibles_all'],this.form['responsibles_sel'],true)" style="width:100%" class="input" name="responsibles_all" size="8" multiple>
							<%
								List selectedResponsibles = new ArrayList(); // this list is used to prevent reading the people details repeatedly
								List allPeople = (List)request.getAttribute(WebKeys.REQUEST_BEAN_ALL_PEOPLE);
								for (Iterator iterator = allPeople.iterator(); iterator.hasNext();) {
									StudyResponsibleDTO resp = (StudyResponsibleDTO) iterator.next();
							 		if (!report.getSelectedResponsibles().contains(resp.getId())) {
							 %>
										<option value="<%=resp.getId() %>"><%=resp.getListName() %></option>
							<% 
									}
									else {
										selectedResponsibles.add(resp);
									}									
								} 
							%>
						</select>
					</td><td>
						<input type="button" value="->" onclick="moveSelectedOptions(this.form['responsibles_all'],this.form['responsibles_sel'],true)"><br><br>
						<input type="button" value="<-" onclick="moveSelectedOptions(this.form['responsibles_sel'],this.form['responsibles_all'],true)">
					</td><td>
						<select ondblclick="moveSelectedOptions(this.form['responsibles_sel'],this.form['responsibles_all'],true)" style="width:100%" class="input" name="responsibles_sel" size="8" multiple>
							<%
								for (Iterator iterator = selectedResponsibles.iterator(); iterator.hasNext();) {
									StudyResponsibleDTO resp = (StudyResponsibleDTO) iterator.next();
							 %>
									<option value="<%=resp.getId() %>"><%=resp.getListName() %></option>
							<% 
								} 
							%>
						</select>
					</td></tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="bodytext-bold">
				Operator
			</td>
			<td class="bodytext">
				<select name="conditionOperator">
					<option value="AND" <% if ("AND".equals(report.getConditionOperator())) out.print("selected"); %>>AND</option>
					<option value="OR" <% if ("OR".equals(report.getConditionOperator())) out.print("selected"); %>>OR</option>
				</select>
			</td>
		</tr>
		<tr>
			<td class="bodytext-bold">
				Order by time
			</td>
			<td class="bodytext">
				<input type="checkbox" name="orderByTime" <% if (report.isOrderByTime()) out.print("checked"); %>>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="bodytext">
				<input type="button" value="Save" onclick="if (checkForm()) {selectAllOptions(this.form['columns_sel']); selectAllOptions(this.form['responsibles_sel']);this.form.submit();}">
				<input type="button" value="Cancel" onclick="<%= WebKeys.REQUEST_PARAM_USECASE %>.value='<%=WebConstants.USECASE_REPORT_LIST%>'; submit();">
			</td>
		</tr>
	</table>
</form>

<jsp:include page="bottom.html"/>