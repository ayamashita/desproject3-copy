<%@page import="com.tec.des.http.WebKeys"%>
<%@page import="com.tec.des.http.WebConstants"%>
<%@page import="com.tec.des.report.AvailableColumns"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.tec.des.dto.ReportDTO"%>

<%@ taglib uri="/util" prefix="util" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<jsp:include page="top.jsp"/>
<% ReportDTO report = (ReportDTO) request.getAttribute(WebKeys.REQUEST_BEAN_REPORT); %>

<!-- Start navigation text -->
<TABLE cellspacing="0" cellpadding="0" width="96%">
    <TR>
        <TD class="path" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Personalised Reports &gt; <%= report.getName()%></TD>
        <TD align="right"><A href="JavaScript:help('help.html#reports')" onmouseover="document.help.src='images/Help.png'" onmouseout="document.help.src='images/Help_gray.png'"><IMG height="18" width="18" border="0" name="help" src="images/Help_gray.png"></A></TD>
    </TR>
</TABLE>
<!-- End navigation text -->
<jsp:include page="messages.jsp"/>


<display:table name="<%=WebKeys.REQUEST_BEAN_DISPLAYTAG_LIST %>" export="true" 
	requestURI="<%=WebConstants.FRONTCONTROLLER_URL %>" 
	pagesize="<%=WebConstants.MAX_HIT_LIST_LENGTH %>"
	>
	<% 
	for (Iterator iter=report.getSelectedColumns().iterator(); iter.hasNext();) {
		AvailableColumns column = AvailableColumns.getByName(iter.next().toString());
	%>
		<display:column title="<%=column.getDisplayableColumnName() %>" property="<%= column.getName()%>" decorator="<%=column.getDecoratorClass() %>" class="bodytext" headerClass="bodytext-bold"/>
	<% } %>
</display:table>

<jsp:include page="bottom.html"/>