package com.tec.des.taglib;

/**
 * Marks objects, that can generate a link (i.e. publication)
 * @author junm
 *
 */
public interface Linkable {
	public String getUrl();
	public String getLinkTitle();
}
