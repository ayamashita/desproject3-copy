package com.tec.des.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import com.tec.des.dto.ReportDTO;
import com.tec.des.report.AvailableColumns;
import com.tec.shared.exceptions.SystemException;

public class ReportDAO extends DAO {

	public ReportDAO() throws SystemException, SQLException {
		super();
	}

	public Vector listAllReports(String userId) {
		try {
			String sql = "select id, name from personalised_report where user_id=?";
			Connection con = getConnection();
			PreparedStatement stat = con.prepareStatement(sql);
			stat.setString(1, userId);

			ResultSet rs = stat.executeQuery();
			return resultsAsVector(rs);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	protected Vector resultsAsVector(ResultSet rs) throws SQLException {
		Vector list = new Vector();

		while(rs.next()) {
			HashMap rec = new HashMap();
			for(int i=1; i<=rs.getMetaData().getColumnCount(); i++) {
				rec.put(rs.getMetaData().getColumnName(i), rs.getObject(i));
			}
			list.add(rec);
		}
		rs.close();
		
		return list;
	}
	
	private void update(ReportDTO report) throws SQLException {
		// update basic values (name, etc.)
		String sql = "update personalised_report set name=?, conditionOperator=?, orderByTime=? where id=?";
		PreparedStatement stat = getConnection().prepareStatement(sql);
		stat.setString(1, report.getName());
		stat.setString(2, report.getConditionOperator());
		stat.setBoolean(3, report.isOrderByTime());
		stat.setInt(4, report.getId());
		stat.executeUpdate();
		stat.close();
	}
	private void insert(ReportDTO report) throws SQLException {
		// update basic values (name, etc.)
		String sql = "insert into personalised_report (name, conditionOperator, orderByTime, user_id) values (?, ?, ?, ?)";
		PreparedStatement stat = getConnection().prepareStatement(sql);
		stat.setString(1, report.getName());
		stat.setString(2, report.getConditionOperator());
		stat.setBoolean(3, report.isOrderByTime());
		stat.setString(4, report.getUserId());
		stat.executeUpdate();
		stat.close();

		if (report.getId() == 0) {
			sql = "select LAST_INSERT_ID()";
			ResultSet rs = getConnection().createStatement().executeQuery(sql);
			rs.next();
			report.setId(rs.getInt(1));
			rs.getStatement().close();
		}
	}
	
	private void saveFilterConditions(ReportDTO report) throws SQLException {
		String sql;
		PreparedStatement stat;
		// delete all assigned columns
		deleteById("personalised_report_filter", "report_id", report.getId());
		
		// insert newly assigned columns
		sql = "insert into personalised_report_filter (report_id, value) values (?, ?)";
		stat = getConnection().prepareStatement(sql);
		for (Iterator iterator = report.getSelectedResponsibles().iterator(); iterator.hasNext();) {
			stat.setInt(1, report.getId());
			stat.setString(2, (String) iterator.next());
			stat.executeUpdate();
			
			stat.clearParameters();
		}
		stat.close();
	}
	private void deleteById(String tableName, String columnName, int id) throws SQLException {
		String sql;
		PreparedStatement stat;
		sql = "delete from " + tableName + " where " + columnName + "=?";
		stat = getConnection().prepareStatement(sql);
		stat.setInt(1, id);
		stat.executeUpdate();
		stat.close();
	}
	
	private void saveColumns(ReportDTO report) throws SQLException {
		String sql;
		PreparedStatement stat;
		// delete all assigned columns
		deleteById("personalised_report_columns", "report_id", report.getId());
		
		// insert newly assigned columns
		sql = "insert into personalised_report_columns (report_id, column_name, seq) values (?, ?, ?)";
		stat = getConnection().prepareStatement(sql);
		int seq = 0;
		for (Iterator iterator = report.getSelectedColumns().iterator(); iterator.hasNext();) {
			String col = (String) iterator.next();
			
			stat.setInt(1, report.getId());
			stat.setString(2, col);
			stat.setInt(3, seq++);
			stat.executeUpdate();
			
			stat.clearParameters();
		}
		stat.close();
	}

	public void read(ReportDTO report) throws SQLException {
		String sql = "select * from personalised_report where id=?";
		PreparedStatement stat = getConnection().prepareStatement(sql);
		stat.setInt(1, report.getId());
		ResultSet rs = stat.executeQuery();
		
		rs.next();
		report.setId(rs.getInt("id"));
		report.setName(rs.getString("name"));
		report.setConditionOperator(rs.getString("conditionOperator"));
		report.setOrderByTime(rs.getBoolean("orderByTime"));
		
		rs.getStatement().close();
		
		readColumns(report);
		readFilterConditions(report);
	}
	
	private void readFilterConditions(ReportDTO report) throws SQLException {
		String sql;
		PreparedStatement stat;
		ResultSet rs;
		sql = "select * from personalised_report_filter where report_id=?";
		stat = getConnection().prepareStatement(sql);
		stat.setInt(1, report.getId());
		rs = stat.executeQuery();
		report.setSelectedResponsibles(new ArrayList());
		while(rs.next()) {
			report.getSelectedResponsibles().add(rs.getString("value"));
		}
		rs.getStatement().close();
	}
	
	private void readColumns(ReportDTO report) throws SQLException {
		String sql;
		PreparedStatement stat;
		ResultSet rs;
		sql = "select * from personalised_report_columns where report_id=? order by seq";
		stat = getConnection().prepareStatement(sql);
		stat.setInt(1, report.getId());
		rs = stat.executeQuery();
		report.setSelectedColumns(new ArrayList());
		while(rs.next()) {
			report.getSelectedColumns().add(rs.getString("column_name"));
		}
		rs.getStatement().close();
	}
	
	public void save(ReportDTO report) throws SQLException {
		if (report.getId() == 0) {
			insert(report);
		}
		else {
			update(report);
		}
		saveColumns(report);
		saveFilterConditions(report);
	}
	
	public void delete(ReportDTO report) throws SQLException {
		deleteById("personalised_report_columns", "report_id", report.getId());
		deleteById("personalised_report_filter", "report_id", report.getId());
		deleteById("personalised_report", "id", report.getId());
	}
	
	/**
	 * Fills parameters and runs the query. Result will be saved in session for displaying it in displaytag.
	 * @param con
	 * @param report
	 * @param sql
	 * @param req
	 * @return 
	 * @throws SQLException 
	 */
	public Vector runReport(ReportDTO report) throws Exception {
		StringBuffer sql = constructSqlQuery(report);
		PreparedStatement stat = getConnection().prepareStatement(sql.toString());
		int i = 1;
		for (Iterator iter = report.getSelectedResponsibles().iterator(); iter.hasNext();) {
			String respId = (String) iter.next();
			stat.setString(i++, respId);
		}

		// run the query
		ResultSet rs = stat.executeQuery();
		return resultsForReport(report, rs);
	}

	/**
	 * constructs the sql query for the report
	 * 
	 * @param report
	 * @return
	 */
	private StringBuffer constructSqlQuery(ReportDTO report) {
		StringBuffer sql = new StringBuffer().append("select distinct s.study_id");
		for (Iterator iter = report.getSelectedColumns().iterator(); iter.hasNext();) {
			String colName = (String) iter.next();
			AvailableColumns column = AvailableColumns.getByName(colName);
			
			sql.append(", ");
			sql.append(column.getDbColumnName());
		}
		sql.append(" from study s ")
			.append("left outer join people_study r on (r.study_id = s.study_id) ")
			.append("left outer join study_type t on (t.study_type_id = s.study_type_id) ")
			.append("left outer join study_duration_unit u on (u.study_duration_unit_id = s.study_duration_unit_id) ");
		StringBuffer where = new StringBuffer();
		for (Iterator iter = report.getSelectedResponsibles().iterator(); iter.hasNext(); iter.next()) {
			if (where.length() > 0) {
				where.append(" ").append(report.getConditionOperator());
			}
			where.append(" (r.people_id=?)");
		}
		
		if (where.length() > 0) {
			sql.append(" where").append(where);
		}
		
		sql.append(" order by ").append(AvailableColumns.study_type.getDbColumnName());
		if (report.isOrderByTime()) {
			sql.append(", ").append(AvailableColumns.study_end_date.getName());
		}
		
		return sql;
	}

	protected Vector resultsForReport(ReportDTO report, ResultSet rs) throws Exception {
		Vector list = new Vector();

		while(rs.next()) {
			HashMap rec = new HashMap();
			
			for(int i=1; i<=rs.getMetaData().getColumnCount(); i++) {
				String columnName = rs.getMetaData().getColumnName(i);
				AvailableColumns column = AvailableColumns.getByName(columnName);
				if (column != null) {
					rec.put(columnName, column.prepareValueForStoring(rs.getObject(i), rec));
				}
				else {
					rec.put(columnName, rs.getObject(i));
				}
			}
			list.add(rec);
		}
		rs.close();
		
		return list;
	}

}
