package com.tec.des.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.tec.des.dto.PublicationDTO;
import com.tec.des.util.Constants;
import com.tec.des.util.Validator;
import com.tec.desws.DesWSClientImpl;
import com.tec.desws.WSPublication;
import com.tec.shared.exceptions.SystemException;
import com.tec.shared.util.Nuller;

/**
 * Class encapsulating Publication data access.
 * 
 * @author : Per Kristian Foss
 */
public class PublicationDAO extends DAO {

	/**
	 * Constructs a PublicationDAO.
	 * 
	 * @throws SystemException
	 * @throws SQLException
	 */
	public PublicationDAO() throws SystemException, SQLException {
		super();
	}

	/**
	 * Returns a Vector containing selected Publications.
	 * 
	 * @param String
	 *            containing Id's of selected Publications
	 * @return Vector
	 * @throws SQLException
	 */

	public static Vector getSelectedPublications(String publicationIds) {
		Vector publications = new Vector();
		String[] publicationIdsAsArray = publicationIds.split(Constants.ID_SEPARATOR);
		
		for (int i = 0; i < publicationIdsAsArray.length; i++) {
			String pId = publicationIdsAsArray[i];
			publications.add(getPublication(pId));
		}
		
		return publications;
	}

	/**
	 * returns one publication found by given id.
	 * @param id
	 * @return
	 */
	public static PublicationDTO getPublication(String id) {
		Map pubFromDes = DesWSClientImpl.getPublications(new WSPublication().setId(id), null);
		if (pubFromDes.size() != 1) {
			throw new RuntimeException("Expected 1 result for publication ID: " + id +", but found " + pubFromDes.size());
		}

		Iterator pubIter = pubFromDes.values().iterator();
		WSPublication pub = (WSPublication) pubIter.next();
		
		PublicationDTO dto = new PublicationDTO();
		dto.setId(pub.getId());
		dto.setTitle(pub.getTitle());
		dto.setSource(pub.getSource());
		dto.setUrl(pub.getUrl());
		
		return dto;
	}

	/**
	 * Returns a Vector containing Publications, based on the given search
	 * criteria.
	 * 
	 * @param String
	 *            searchText containing the words typed in by user
	 * @return Vector
	 * @throws SQLException
	 */

	public static Vector findPublications(String searchText) throws SQLException {

		String[] searchCriteria = Validator.checkEscapes(searchText).split(" ");

		// first publications by first keyword
		String s = searchCriteria[0];
		Map publications = DesWSClientImpl.getPublications(new WSPublication().setTitle(s), null);
		publications.putAll(DesWSClientImpl.getPublications(new WSPublication().setSource(s), null));
		publications.putAll(DesWSClientImpl.getPublications(new WSPublication().setPubAbstract(s), null));
		publications.putAll(DesWSClientImpl.getPublications(new WSPublication().setAuthors(s), null));

		// filter all other keywords
		for (int i = 1; i < searchCriteria.length; i++) {
			s = searchCriteria[i].toLowerCase();
			Map publicationsBackup = publications;
			publications = new LinkedHashMap();
			for (Iterator iter = publicationsBackup.values().iterator(); iter.hasNext();) {
				WSPublication pub = (WSPublication) iter.next();
				if (Nuller.noNull(pub.getAuthors()).toLowerCase().indexOf(s)>=0 
						|| Nuller.noNull(pub.getTitle()).toLowerCase().indexOf(s)>=0 
						|| Nuller.noNull(pub.getPubAbstract()).toLowerCase().indexOf(s)>=0
						|| Nuller.noNull(pub.getSource()).toLowerCase().indexOf(s)>=0) {
					publications.put(pub.getId(), pub);
				}
			}
		}

		// sort on title
		List pubColl = new ArrayList(publications.values());
		Comparator comparator = new Comparator() {
			public int compare(Object arg0, Object arg1) {
				return Nuller.noNull(((WSPublication) arg0).getTitle()).compareTo(Nuller.noNull(((WSPublication) arg1).getTitle()));
			}
		};
		Collections.sort(pubColl, comparator);

		// convert to proper DTO objects
		Vector returnValue = new Vector(pubColl.size());
		for (Iterator iter = pubColl.iterator(); iter.hasNext();) {
			WSPublication pub = (WSPublication) iter.next();
			PublicationDTO dto = new PublicationDTO();
			dto.setId(pub.getId());
			dto.setSource(pub.getSource());
			dto.setTitle(pub.getTitle());
			dto.setUrl(pub.getUrl());
			returnValue.add(dto);
		}

		return returnValue;
	}

}