package com.tec.des.dao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;

import com.tec.des.dto.StudyResponsibleDTO;
import com.tec.des.dto.UserDTO;
import com.tec.des.util.ExceptionMessages;
import com.tec.des.util.Validator;
import com.tec.desws.DesWSClientImpl;
import com.tec.desws.WSPerson;
import com.tec.server.db.ResultSetHelper;
import com.tec.server.log4j.LoggerFactory;
import com.tec.shared.exceptions.SystemException;
import com.tec.shared.exceptions.UserException;
import com.tec.shared.util.Nuller;

/**
 * Class encapsulating People data access.
 *
 * @author :  Per Kristian Foss
 */
public class PeopleDAO extends DAO {

 /**
  * Constructs a PeopleDAO. 
  *
  * @throws SystemException
  * @throws SQLException
  */
  public PeopleDAO() throws SystemException, SQLException {
    super();
  }

 /**
  * Returns a Vector containing all People. 
  * @return Vector
  * @throws SQLException
  */

  public static Vector getAllPeople() throws SQLException {
	Map peopleFromDes = DesWSClientImpl.getPeople(null, WSPerson.SORT_ON_LASTNAME);
	Vector v = new Vector();
	
	for (Iterator iter = peopleFromDes.values().iterator(); iter.hasNext();) {
		WSPerson p = (WSPerson) iter.next();

		StudyResponsibleDTO dto = new StudyResponsibleDTO();
		dto.setId(p.getId());
		dto.setListName(p.getLastname() + ", " + p.getFirstname());
		dto.setSingleName(p.getFirstname() + " " + p.getLastname());
		dto.setUrl(p.getUrl());
		v.add(dto);
	}
	return v;
  }
  
 /**
  * Returns a Vector containing selected Study Responsibles. 
  * @param String[] containg the ids of the study responsibles to fetch.
  * @return Vector
  * @throws SQLException
  */

  public static Vector getSelectedPeople(String[] selectedPeople) throws SQLException {

    Vector people = new Vector();
    for (int i = 0; i < selectedPeople.length; i++) {
		String id = selectedPeople[i];
		if (!Nuller.isReallyNull(id)) {
			people.add(getPeople(id));
		}
	}
    
    return people;
  }

/**
 * Returns instance of StudyResponsibleDto found by id.
 * @param id
 * @return
 */
public static StudyResponsibleDTO getPeople(String id) {
	Map peopleFromDes = DesWSClientImpl.getPeople(new WSPerson().setId(id), null);
	
	if (peopleFromDes.size() != 1) {
		throw new RuntimeException("Expected 1 result for person ID: " + id +", but found " + peopleFromDes.size());
	}

	Iterator pubIter = peopleFromDes.values().iterator();
	WSPerson person = (WSPerson) pubIter.next();
	
	StudyResponsibleDTO dto = new StudyResponsibleDTO();
	dto.setId(person.getId());
	dto.setListName(person.getLastname() + ", " + person.getFirstname());
	dto.setSingleName(person.getFirstname() + " " + person.getLastname());
	dto.setUrl(person.getUrl());
	
	return dto;
}
  
 /**
  * Fetches all people matching the given searchText. 
  * @param onlyRole If true only people with any role matching the search criteria is fetched.
  * @param searchText The search criteria
  * @return Vector containing UserDTOs.
  * @throws SQLException
  */

  public Vector getPeopleRole(boolean onlyRole, String searchText) throws SQLException {

	Map foundPeople = findPeople(searchText);
	if (foundPeople.size() == 0) {
		// no people found
		return new Vector();
	}
	
    Vector peopleRole = new Vector();
    StringBuffer query = new StringBuffer();    

    query.append("select pr.people_id, r.role_id, r.role_name from ");
    query.append("people_role pr, role r where pr.role_id = r.role_id");
    
    query.append(" and pr.people_id in (");
    for (Iterator iter = foundPeople.keySet().iterator(); iter.hasNext();) {
		String pId = (String) iter.next();
		query.append("'").append(pId).append("'");
		if (iter.hasNext()) {
			query.append(",");
		}
	}
    query.append(");");

    LoggerFactory.getLogger(PeopleDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    while (rs.next()) {
        UserDTO user = new UserDTO();
        user.setId(rsh.getString("people_id"));
        WSPerson person = (WSPerson) foundPeople.get(user.getId());
        user.setListName(person.getLastname() + ", " + person.getFirstname());   
        user.setRoleId(rsh.getInt("role_id"));           
        user.setRoleName(rsh.getString("role_name"));
        peopleRole.add(user);
        // remove the person from foundPeople, because I will add remaining people form foundPeople later.
        foundPeople.remove(user.getId());
    }
    
    // add also found people, that match the search text, but do not have assigned any role
    if (!onlyRole) {
    	for (Iterator iter = foundPeople.values().iterator(); iter.hasNext();) {
			WSPerson person = (WSPerson) iter.next();
	        UserDTO user = new UserDTO();
	        user.setId(person.getId());
	        user.setListName(person.getLastname() + ", " + person.getFirstname());   
	        peopleRole.add(user);   
		}
    }
    
    rs.close();
    stmt.close();

    // sort by family name
	// sort on title
	Comparator comparator = new Comparator() {
		public int compare(Object arg0, Object arg1) {
			return Nuller.noNull(((UserDTO) arg0).getListName()).compareTo(Nuller.noNull(((UserDTO) arg1).getListName()));
		}
	};
	Collections.sort(peopleRole, comparator);

	return peopleRole;
  }
  
  /**
  * Returns a hashtable containing all Roles in the database 
  * @param Hashtable
  * @throws SQLException
  */

  public Hashtable getRoles() throws SQLException {

    Hashtable roles = new Hashtable();
    StringBuffer query = new StringBuffer();    
    
    query.append("select role_id, role_name from role;");
    
    LoggerFactory.getLogger(PeopleDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    while (rs.next()) {
        roles.put(String.valueOf(rsh.getInt("role_id")), rsh.getString("role_name"));
    }   
    
    rs.close();
    stmt.close();
    
    return roles;
  }
  
 /**
  * Updates the people_role tablewith the given hashtable.
  * @throws SQLException
  */

  public void setPeopleRole(Hashtable peopleRole) throws SQLException {

    StringBuffer query_find = new StringBuffer();
    String[] peopleId = (String[])peopleRole.keySet().toArray(new String[0]);
    String[] peopleRoleId = (String[])peopleRole.values().toArray(new String[0]);
    Vector peopleWithRole = new Vector();;
    
    // Fetch all people having a role
    query_find.append("select people_id from people_role;");    
    
    LoggerFactory.getLogger(PeopleDAO.class).debug(query_find.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query_find.toString());    
    ResultSetHelper rsh = new ResultSetHelper(rs);
    while (rs.next()) {
        peopleWithRole.add(rsh.getString("people_id"));
    }   
    
    // Checks previous role (if any) with new role and updates people_role table accordingly
    for (int i = 0; i < peopleId.length; i++) {       
        if (peopleWithRole.contains(peopleId[i])) {
            if (peopleRoleId[i].equals("0"))
                stmt.executeUpdate("delete from people_role where people_id = '" + peopleId[i] + "'");
            else {
                stmt.executeUpdate("update people_role set role_id = " + peopleRoleId[i] + " where people_id = '" + peopleId[i] + "';");
            }
        } else {            
            if (!(peopleRoleId[i].equals("0")))
                stmt.executeUpdate("insert into people_role values ('" +  peopleId[i] + "'," + peopleRoleId[i] + ");");
        }
    }
    
    rs.close();
    stmt.close();
  }
  
 /**
  * Returns the user connected to the specified username and password
  * @param username (e-mail) of the user to fetch
  * @param password of the user to fetch
  * @return UserDTO
  * @throws SQLException
  */
  public UserDTO getUser(String username, String password) throws SQLException, UserException {
	boolean ok = DesWSClientImpl.checkUser(username, password);
    Map people = DesWSClientImpl.getPeople(new WSPerson().setId(username).setPassword(password), null);
	if (!ok || (people.size() != 1)) {
        throw new UserException(ExceptionMessages.INVALID_USER_NAME_OR_PASSWORD);
    }
    
    WSPerson p = (WSPerson) people.values().iterator().next();
    UserDTO user = new UserDTO();
    user.setId(p.getId());
    user.setSingleName(p.getFirstname() + " " + p.getLastname());
    
    StringBuffer query = new StringBuffer();    
    query.append("select r.role_id, r.role_name from people_role pr ");
    query.append("left join role r on pr.role_id = r.role_id ");
    query.append("where pr.people_id='"+user.getId()+"';");
    
    LoggerFactory.getLogger(PeopleDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    if (rs.next()) {
        user.setRoleName(rsh.getString("role_name"));           
        user.setRoleId(rsh.getInt("role_id"));           
    }
    
    rs.close();
    stmt.close();

    return user;
  }

/**
 * retruns map of people found by keywords. Searched fields are first name and last name. 
 * @param search
 * @return
 */
public static Map findPeople(String search) {
    String[] searchCriteria = Validator.checkEscapes(search).split(" ");

	String s = searchCriteria[0];
	Map people = DesWSClientImpl.getPeople(new WSPerson().setFirstname(s), null);
	people.putAll(DesWSClientImpl.getPeople(new WSPerson().setLastname(s), null));

	// filter all other keywords
	for (int i = 1; i < searchCriteria.length; i++) {
		s = searchCriteria[i].toLowerCase();
		Map backup = people;
		people = new LinkedHashMap();
		for (Iterator iter = backup.values().iterator(); iter.hasNext();) {
			WSPerson person = (WSPerson) iter.next();
			if (Nuller.noNull(person.getFirstname()).toLowerCase().indexOf(s)>=0 || Nuller.noNull(person.getLastname()).toLowerCase().indexOf(s)>=0) {
				people.put(person.getId(), person);
			}
		}
	}

	return people;
}
      
}