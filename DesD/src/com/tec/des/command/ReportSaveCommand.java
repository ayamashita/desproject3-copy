package com.tec.des.command;

import com.tec.des.dao.ReportDAO;
import com.tec.des.dto.ReportDTO;

/**
 * Command for saving given personalised report.
 * 
 */
public class ReportSaveCommand extends Command {

	private ReportDTO report;

	/**
	 * Constructs a new Command object.
	 */
	public ReportSaveCommand() {
		super();
	}

	/**
	 * Executes the command.
	 * 
	 * @throws Exception
	 */
	protected void performExecute() throws Exception {
		ReportDAO dao = new ReportDAO();
		try {
			dao.save(report);
		} finally {
			dao.close();
		}
	}

	public ReportDTO getReport() {
		return report;
	}

	public void setReport(ReportDTO report) {
		this.report = report;
	}

}
