package com.tec.des.command;
import java.io.File;
import com.tec.des.dao.StudyDAO;
import com.tec.shared.util.Nuller;

/**
 * Command for fetching a file containin information about all studies.
 * The result of the command is File containing the information.
 *
 * @author :  Per Kristian Foss
 * 
 */
public class ExportStudiesCommand extends Command  {
    
    private File exportedStudies;
    
/**
 * Constructs a new Command object.
 */
    public ExportStudiesCommand() {
    }

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
    public boolean isReadyToCallExecute() {
        return super.isReadyToCallExecute();
    }

   /**
    * Executes the command. 
    * @throws Exception 
    */
    protected void performExecute() throws Exception {
    StudyDAO dao = new StudyDAO();
        try {
            exportedStudies = dao.exportStudies(); 
        } finally {
            dao.close();
        }
    }
    
    public File getResult() {
        return exportedStudies;
    }

}