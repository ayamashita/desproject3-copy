package com.tec.des.command;

import com.tec.des.dao.ReportDAO;
import com.tec.des.dto.ReportDTO;

/**
 * Command for reading details of given personalised report.
 * 
 */
public class ReportReadCommand extends Command {

	private ReportDTO report;

	/**
	 * Constructs a new Command object.
	 */
	public ReportReadCommand() {
		super();
	}

	/**
	 * Executes the command.
	 * 
	 * @throws Exception
	 */
	protected void performExecute() throws Exception {
		ReportDAO dao = new ReportDAO();
		try {
			dao.read(report);
		} finally {
			dao.close();
		}
	}

	public ReportDTO getReport() {
		return report;
	}

	public void setReport(ReportDTO report) {
		this.report = report;
	}

}
