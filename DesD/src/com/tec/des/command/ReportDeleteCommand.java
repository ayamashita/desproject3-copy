package com.tec.des.command;

import com.tec.des.dao.ReportDAO;
import com.tec.des.dto.ReportDTO;

/**
 * Command for deleting given personalised report.
 * 
 */
public class ReportDeleteCommand extends Command {

	private ReportDTO report;

	/**
	 * Constructs a new Command object.
	 */
	public ReportDeleteCommand() {
		super();
	}

	/**
	 * Executes the command.
	 * 
	 * @throws Exception
	 */
	protected void performExecute() throws Exception {
		ReportDAO dao = new ReportDAO();
		try {
			dao.delete(report);
		} finally {
			dao.close();
		}
	}

	public ReportDTO getReport() {
		return report;
	}

	public void setReport(ReportDTO report) {
		this.report = report;
	}

}
