package com.tec.des.command;

import java.util.Vector;

import com.tec.des.dao.ReportDAO;
import com.tec.des.dto.ReportDTO;

/**
 * Command for reading details of given personalised report.
 * 
 */
public class ReportRunCommand extends Command {

	private ReportDTO report;
	private Vector result;

	/**
	 * Constructs a new Command object.
	 */
	public ReportRunCommand() {
		super();
	}

	/**
	 * Executes the command.
	 * 
	 * @throws Exception
	 */
	protected void performExecute() throws Exception {
		ReportDAO dao = new ReportDAO();
		try {
			result = dao.runReport(report);
		} finally {
			dao.close();
		}
	}

	public ReportDTO getReport() {
		return report;
	}

	public void setReport(ReportDTO report) {
		this.report = report;
	}

	public Vector getResult() {
		return result;
	}

}
