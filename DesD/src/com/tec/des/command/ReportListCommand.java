package com.tec.des.command;

import java.util.Vector;

import com.tec.des.dao.ReportDAO;

/**
 * Command for listing all personalised reports.
 * 
 */
public class ReportListCommand extends Command {

	private Vector reports;
	private String userId;

	/**
	 * Constructs a new Command object.
	 */
	public ReportListCommand() {
		super();
	}

	/**
	 * Executes the command.
	 * 
	 * @throws Exception
	 */
	protected void performExecute() throws Exception {
		ReportDAO dao = new ReportDAO();
		try {
			reports = dao.listAllReports(userId);
		} finally {
			dao.close();
		}
	}

	public Vector getResult() {
		return reports;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
