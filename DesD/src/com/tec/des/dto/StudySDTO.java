package com.tec.des.dto;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;

import com.tec.shared.util.Nuller;
import com.tec.des.dao.PublicationDAO;
import com.tec.des.util.Constants;

/**
 * Simple Data transfer object for a study.
 *
 * @author  Norunn Haug Christensen
 */
public class StudySDTO implements DTO {

    private int id = Nuller.getIntNull();   
    private String name = Nuller.getStringNull();
    private String type = Nuller.getStringNull();
    private String endDate = Nuller.getStringNull();
    private Hashtable responsibles;
    private String description = Nuller.getStringNull();
    private Hashtable publications;

    public StudySDTO() {
        super();
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndDate() {
        return endDate;
    }
    
    public void setResponsibles(Hashtable responsibles) {
        this.responsibles = responsibles;
    }

    public Hashtable getResponsibles() {
        if (responsibles == null)
            responsibles = new Hashtable();
        return responsibles;
    }

    public String[] getResponsibleNames() {
        if (responsibles == null)
            responsibles = new Hashtable();
        
        String[] names = new String[responsibles.size()];
		int i = 0;
        for (Iterator iter = responsibles.values().iterator(); iter.hasNext();) {
			StudyResponsibleDTO resp = (StudyResponsibleDTO) iter.next();
			names[i++] = resp.getSingleName();
		}
        return names;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }

    public void setPublications(Hashtable publications) {
        this.publications = publications;
    }
    
    public Hashtable getPublications() {
        if (publications == null)
            publications = new Hashtable();
        return publications;
    }

    public String[] getPublicationTitles() {
        if (publications == null)
        	publications = new Hashtable();
        
        String[] names = new String[publications.size()];
		int i = 0;
        for (Iterator iter = publications.values().iterator(); iter.hasNext();) {
			PublicationDTO resp = (PublicationDTO) iter.next();
			names[i++] = resp.getTitle();
		}
        return names;
    }

    
}
