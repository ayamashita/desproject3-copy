package com.tec.des.dto;

import java.util.ArrayList;
import java.util.List;

import com.tec.shared.util.Nuller;

public class ReportDTO implements DTO {
	int id;
	String name = Nuller.getStringNull();
	boolean orderByTime = false;
	String conditionOperator;
	List selectedColumns = new ArrayList();
	List selectedResponsibles = new ArrayList();
	String userId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isOrderByTime() {
		return orderByTime;
	}
	public void setOrderByTime(boolean orderByTime) {
		this.orderByTime = orderByTime;
	}
	public String getConditionOperator() {
		return conditionOperator;
	}
	public void setConditionOperator(String conditionOperator) {
		this.conditionOperator = conditionOperator;
	}

	public List getSelectedColumns() {
		return selectedColumns;
	}
	public void setSelectedColumns(List selectedColumns) {
		this.selectedColumns = selectedColumns;
	}

	public List getSelectedResponsibles() {
		return selectedResponsibles;
	}
	public void setSelectedResponsibles(List selectedResponsibles) {
		this.selectedResponsibles = selectedResponsibles;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	

}
