package com.tec.des.dto;
import java.util.Vector;

/**
 * Data transfer object containg all items used in a graphical report presentation.
 *
 * @author  Norunn Haug Christensen
 */
public class GraphicalReportDTO implements DTO {

    private Vector studyTypesAndColour;
    private Vector graphicalItems;

    public GraphicalReportDTO() {
        super();
    }

    public void setStudyTypesAndColour(Vector studyTypesAndColour) {
        this.studyTypesAndColour = studyTypesAndColour;
    }
    
    public Vector getStudyTypesAndColour() {
        if (studyTypesAndColour == null)
            studyTypesAndColour = new Vector();
        return studyTypesAndColour;
    }
    
    public void setGraphicalItems(Vector graphicalItems) {
        this.graphicalItems = graphicalItems;
    }
    
    public Vector getGraphicalItems() {
        if (graphicalItems == null)
            graphicalItems = new Vector();
        return graphicalItems;
    }
    
    


}
