package com.tec.des.util;

/**
 * Class containig various constants and methods used in the application.
 *
 * @author  Norunn Haug Christensen
 */
public class Constants {
    
 /**
  * The class can not be instantiated
  */
  private Constants() {
  }
  
  public final static String DES_PROPERTIES_FILE = "des";
  public final static String SIMULA_DATABASE_NAME = "simulaDatabase";
    
  public final static String DATE_FORMAT_PRESENTATION = "dd. MMM yyyy";
  public final static String DATE_FORMAT_EDIT = "yyyy/MM/dd"; 
  public final static String DATE_FORMAT_DATABASE = "yyyyMMdd";
  
  public final static String ERROR_MESSAGE_SEPARATOR = " <BR> ";
  public final static String DATE_SEPARATOR = "/";
  public final static String ID_SEPARATOR = ",";
  
  public final static int MAX_FILE_SIZE = 16777216;
  
  //Access
  public final static int ROLE_DATABASE_ADMIN = 1;
  public final static int ROLE_STUDY_ADMIN = 2;
  
  //Maintain modes
  public final static int MAINTAIN_MODE_FINAL = 0;
  public final static int MAINTAIN_MODE_TEMP_NEW = 1;
  public final static int MAINTAIN_MODE_TEMP_EDIT = 2;
  
  public final static int MODE_OK = 0;
  public final static int MODE_CANCEL = 1;

}
