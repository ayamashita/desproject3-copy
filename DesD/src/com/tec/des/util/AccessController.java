package com.tec.des.util;

/**
 * Class for controlling access.
 *
 * @author  Norunn Haug Christensen
 */
public class AccessController {
 /**
  * The class can not be instantiated
  */
  private AccessController() {
  }    

  /**
   * Method for cheching access to a usecase.
   * @param role the role of the user trying to access the usecase.
   * @param requiredRoleForUsecase the required role for accessing the usecase. 
   * 'wider' than the input parameter requiredRoleForUsecase.
   */
   public static boolean hasAccess(int role, int requiredRoleForUsecase) {
    boolean hasAccess = false;
    switch (requiredRoleForUsecase) {
        case (Constants.ROLE_DATABASE_ADMIN):
        hasAccess = (role == Constants.ROLE_DATABASE_ADMIN);
        break;
        case (Constants.ROLE_STUDY_ADMIN):
        hasAccess = (role == Constants.ROLE_DATABASE_ADMIN || role == Constants.ROLE_STUDY_ADMIN);
        break;
    }
    return hasAccess;
   }
}
