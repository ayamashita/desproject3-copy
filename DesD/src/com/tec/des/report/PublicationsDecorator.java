package com.tec.des.report;

import java.util.Iterator;
import java.util.Map;

import javax.servlet.jsp.PageContext;

import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.exception.DecoratorException;
import org.displaytag.properties.MediaTypeEnum;

import com.tec.des.dto.PublicationDTO;

public class PublicationsDecorator implements DisplaytagColumnDecorator {

	public Object decorate(Object columnValue, PageContext pageContext, MediaTypeEnum media) throws DecoratorException {
		StringBuffer result = new StringBuffer();
		
		Map publications = (Map) columnValue;
		
        for (Iterator j = publications.values().iterator(); j.hasNext(); ) {
            PublicationDTO p = (PublicationDTO) j.next();
            if (MediaTypeEnum.HTML.equals(media)) {
	            result.append("<a href=\"").append(p.getUrl()).append("\">").append(p.getLinkTitle()).append("</a>");
	            if (j.hasNext()) {
	            	result.append("<br>");
	            }
            }
            else {
	            result.append(p.getLinkTitle());
	            if (j.hasNext()) {
	            	result.append("\n");
	            }
            }
        }
        
        return result.toString();
	}

}
