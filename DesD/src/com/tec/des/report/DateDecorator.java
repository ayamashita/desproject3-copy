package com.tec.des.report;

import javax.servlet.jsp.PageContext;

import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.exception.DecoratorException;
import org.displaytag.properties.MediaTypeEnum;

import com.tec.des.util.Constants;
import com.tec.shared.exceptions.UserException;
import com.tec.shared.util.Formatter;
import com.tec.shared.util.Parser;

public class DateDecorator implements DisplaytagColumnDecorator {

	public Object decorate(Object columnValue, PageContext pageContext, MediaTypeEnum media) throws DecoratorException {
		try {
			return Formatter.formatDate(Parser.parseDate((String)columnValue, Constants.DATE_FORMAT_DATABASE), Constants.DATE_FORMAT_PRESENTATION);
		} catch (UserException e) {
			throw new DecoratorException(this.getClass(), e.getMessage());
		}
	}

}
