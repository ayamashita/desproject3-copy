package com.tec.des.report;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.enums.Enum;

import com.tec.des.dao.StudyDAO;

public class AvailableColumns extends Enum {

	public static final AvailableColumns publications = new AvailableColumns("publications", PublicationsDecorator.class, "null as publications") {
		public Object prepareValueForStoring(Object value, HashMap rec) throws Exception {
			StudyDAO studyDAO = null;
			try {
				studyDAO = new StudyDAO();
				int studyId = ((Integer)rec.get("study_id")).intValue();
				return studyDAO.getPublications(studyId, false);
			} catch (Exception e) {
				throw e;
			}
			finally {
				studyDAO.close();
			}
		}
	};
	public static final AvailableColumns responsibles = new AvailableColumns("responsibles", ResponsiblesDecorator.class, "null as responsibles") {
		public Object prepareValueForStoring(Object value, HashMap rec) throws Exception {
			StudyDAO studyDAO = null;
			try {
				studyDAO = new StudyDAO();
				int studyId = ((Integer)rec.get("study_id")).intValue();
				return studyDAO.getStudyResponsibles(studyId, false);
			} catch (Exception e) {
				throw e;
			}
			finally {
				studyDAO.close();
			}
		}
	};
	public static final AvailableColumns study_owner = new AvailableColumns("study_owner");
	public static final AvailableColumns last_edited_by = new AvailableColumns("last_edited_by");
	public static final AvailableColumns study_name = new AvailableColumns("study_name");
	public static final AvailableColumns study_type = new AvailableColumns("study_type");
	public static final AvailableColumns study_desc = new AvailableColumns("study_description");
	public static final AvailableColumns study_duration = new AvailableColumns("study_duration");
	public static final AvailableColumns study_duration_unit = new AvailableColumns("study_duration_unit");
	public static final AvailableColumns study_start_date = new AvailableColumns("study_start_date", DateDecorator.class);
	public static final AvailableColumns study_end_date = new AvailableColumns("study_end_date", DateDecorator.class);
	public static final AvailableColumns keywords = new AvailableColumns("study_keywords");
	public static final AvailableColumns no_of_students = new AvailableColumns("no_students");
	public static final AvailableColumns no_of_professionals = new AvailableColumns("no_professionals");
	public static final AvailableColumns study_notes = new AvailableColumns("study_notes");
    
	private String dbColumnName;
	/** column decorator for displaytag */
	private String decoratorClassName;
	
	protected AvailableColumns(String name) {
		this(name, null);
	}
	protected AvailableColumns(String name, Class decoratorClass) {
		this(name, decoratorClass, name);
	}
	
	protected AvailableColumns(String name, Class decoratorClass, String dbColumnName) {
		super(name);
		this.dbColumnName = dbColumnName;
		if (decoratorClass != null) {
			this.decoratorClassName = decoratorClass.getName();
		}
	}

	public static List getValues() {
		return getEnumList(AvailableColumns.class);
	}
	public String getDbColumnName() {
		return dbColumnName;
	}
	public static AvailableColumns getByName(String name) {
		return (AvailableColumns) getEnum(AvailableColumns.class, name);
	}
	public String getDecoratorClass() {
		return decoratorClassName;
	}
	public String getDisplayableColumnName() {
		return StringUtils.capitalize(StringUtils.replace(getName(), "_", " "));
	}
	public Object prepareValueForStoring(Object value, HashMap rec) throws Exception {
		return value;
	}
	public Class getEnumClass() {
		return AvailableColumns.class;
	}
}
