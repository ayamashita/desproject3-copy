package com.tec.des.http.handler;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.GetAllPeopleCommand;
import com.tec.des.command.GetSelectedPeopleCommand;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.Constants;
import com.tec.shared.exceptions.UserException;

/**
 * 
 * Handles the Select Study Responsibles use case.
 *
 * @author Per Kristian Foss
 */
public class SelectStudyResponsiblesHandler extends UsecaseHandler {

  /**
   * Handles the use case by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception {

    String[] selectedPeople;
    if (request.getParameter(WebKeys.REQUEST_PARAM_BUTTON_ADD) != null) {
        selectedPeople = getSelectedPeopleAdd(request);
    } else {
        selectedPeople = getSelectedPeopleRemove(request);
    }
    
    GetAllPeopleCommand command_all = new GetAllPeopleCommand();   
    command_all.execute();     
    request.setAttribute(WebKeys.REQUEST_BEAN_ALL_PEOPLE, command_all.getResult());
    
    GetSelectedPeopleCommand command_select = new GetSelectedPeopleCommand();   
    command_select.setSelectedPeople(selectedPeople);
    command_select.execute();
    Vector selected = command_select.getResult();
    
    request.setAttribute(WebKeys.REQUEST_BEAN_SELECTED_PEOPLE, selected); 
    
    return WebConstants.VIEW_STUDY_RESPONSIBLES;
  }
   
   /**
   * Returns a String[] containing people selected to be study responsibles after the >> button is pressed.
   * @throws UserException
   */
  private String[] getSelectedPeopleAdd(HttpServletRequest request) throws UserException {
     
    String[] oldSelected = request.getParameter(WebKeys.REQUEST_BEAN_SELECTED_PEOPLE).split(Constants.ID_SEPARATOR);
    String[] newSelected = request.getParameterValues(WebKeys.REQUEST_PARAM_ALL_PEOPLE);
    String[] totalSelected;
    
    if (newSelected == null) {
        totalSelected = new String[oldSelected.length];
        
        for (int i = 0; i < oldSelected.length; i++) {
            totalSelected[i] = oldSelected[i];
        }
    } else {
        totalSelected = new String[oldSelected.length + newSelected.length];     
            
        for (int i = 0; i < oldSelected.length; i++) {
            totalSelected[i] = oldSelected[i];
        }
        for (int i = oldSelected.length; i < (oldSelected.length + newSelected.length); i++) {
            totalSelected[i] = newSelected[i - oldSelected.length];
        }
    }
    
    return totalSelected;
  }

   /**
   * Returns a String[] containing people selected to be study responsibles after the << button is pressed.
   * @throws UserException
   */
  private String[] getSelectedPeopleRemove(HttpServletRequest request) throws UserException {

    ArrayList selected = new ArrayList(Arrays.asList(request.getParameter(WebKeys.REQUEST_BEAN_SELECTED_PEOPLE).split(Constants.ID_SEPARATOR)));
    String[] removeSelected = request.getParameterValues(WebKeys.REQUEST_PARAM_SELECTED_PEOPLE);

    for (int i = 0; i < removeSelected.length; i++) {
		String toRemove = removeSelected[i];
		selected.remove(toRemove);
	}
    return (String[]) selected.toArray(new String[selected.size()]);
  }

  
  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Constants.ROLE_STUDY_ADMIN;
  }
  
}