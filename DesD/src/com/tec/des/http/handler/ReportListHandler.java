package com.tec.des.http.handler;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.ReportListCommand;
import com.tec.des.dto.UserDTO;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.shared.util.Nuller;

public class ReportListHandler extends UsecaseHandler {

	public String doHandle(HttpServletRequest request) throws Exception {
	    ReportListCommand command = new ReportListCommand();
	    UserDTO user = (UserDTO)request.getSession().getAttribute(WebKeys.SESSION_PARAM_USER);
	    command.setUserId(user.getId());
	    command.execute();
	    request.setAttribute(WebKeys.REQUEST_BEAN_DISPLAYTAG_LIST, command.getResult());
	    
	    return WebConstants.VIEW_REPORT_LIST;
	}

	public int getRequiredRole() {
		return Nuller.getIntNull();
	}

}
