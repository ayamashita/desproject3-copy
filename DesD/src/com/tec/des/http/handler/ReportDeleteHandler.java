package com.tec.des.http.handler;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.ReportDeleteCommand;
import com.tec.des.command.ReportListCommand;
import com.tec.des.dto.ReportDTO;
import com.tec.des.dto.UserDTO;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.shared.util.Nuller;

public class ReportDeleteHandler extends UsecaseHandler {

	public String doHandle(HttpServletRequest request) throws Exception {
	    ReportDeleteCommand command = new ReportDeleteCommand();
		ReportDTO report = new ReportDTO();
	    report.setId(Integer.parseInt(request.getParameter(WebKeys.REQUEST_PARAM_REPORT_ID)));
	    command.setReport(report);
	    command.execute();

	    // read list of reports
	    ReportListCommand listCommand = new ReportListCommand();
	    UserDTO user = (UserDTO)request.getSession().getAttribute(WebKeys.SESSION_PARAM_USER);
	    listCommand.setUserId(user.getId());
	    listCommand.execute();
	    request.setAttribute(WebKeys.REQUEST_BEAN_DISPLAYTAG_LIST, listCommand.getResult());

	    return WebConstants.VIEW_REPORT_LIST;
	}

	public int getRequiredRole() {
		return Nuller.getIntNull();
	}

}
