package com.tec.des.http.handler;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.ReportListCommand;
import com.tec.des.command.ReportSaveCommand;
import com.tec.des.dto.ReportDTO;
import com.tec.des.dto.UserDTO;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.shared.util.Nuller;

public class ReportSaveHandler extends UsecaseHandler {

	public String doHandle(HttpServletRequest request) throws Exception {
		// fill the DTO from request
		ReportDTO report = new ReportDTO();
	    report.setId(Integer.parseInt(request.getParameter(WebKeys.REQUEST_PARAM_REPORT_ID)));
	    report.setName(request.getParameter(WebKeys.REQUEST_PARAM_REPORT_NAME));
	    report.setOrderByTime(request.getParameter(WebKeys.REQUEST_PARAM_REPORT_SORT) != null);
	    UserDTO user = (UserDTO)request.getSession().getAttribute(WebKeys.SESSION_PARAM_USER);
	    report.setUserId(user.getId());
	    report.setConditionOperator(request.getParameter(WebKeys.REQUEST_PARAM_REPORT_OPERATOR));
		
	    report.setSelectedColumns(Arrays.asList(request.getParameterValues(WebKeys.REQUEST_PARAM_REPORT_COLUMNS)));
		
		String[] responsibles = request.getParameterValues(WebKeys.REQUEST_PARAM_REPORT_RESPONSIBLES);
		if (responsibles == null) {
			responsibles = new String[0];
		}
		report.setSelectedResponsibles(Arrays.asList(responsibles));

		// run the save command
		ReportSaveCommand command = new ReportSaveCommand();
		command.setReport(report);
		command.execute();

		// view list of reports
	    ReportListCommand listCommand = new ReportListCommand();
	    listCommand.setUserId(user.getId());
	    listCommand.execute();
	    request.setAttribute(WebKeys.REQUEST_BEAN_DISPLAYTAG_LIST, listCommand.getResult());
	    return WebConstants.VIEW_REPORT_LIST;
	}

	public int getRequiredRole() {
		return Nuller.getIntNull();
	}

}
