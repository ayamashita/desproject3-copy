package com.tec.des.http.handler;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.GetAllPeopleCommand;
import com.tec.des.command.ReportReadCommand;
import com.tec.des.dto.ReportDTO;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.shared.util.Nuller;

public class ReportEditHandler extends UsecaseHandler {

	public String doHandle(HttpServletRequest request) throws Exception {
		// read report details
		ReportDTO report = new ReportDTO();
	    report.setId(Integer.parseInt("0"+Nuller.noNull(request.getParameter(WebKeys.REQUEST_PARAM_REPORT_ID))));
		if (report.getId() > 0) {
			ReportReadCommand command = new ReportReadCommand();
			command.setReport(report);
			command.execute();
			request.setAttribute(WebKeys.REQUEST_BEAN_REPORT, report);
		}
		
		// read all people for the select box
	    GetAllPeopleCommand command = new GetAllPeopleCommand();
	    command.execute();
	    request.setAttribute(WebKeys.REQUEST_BEAN_ALL_PEOPLE, command.getResult());
	    
	    return WebConstants.VIEW_REPORT_EDIT;
	}

	public int getRequiredRole() {
		return Nuller.getIntNull();
	}

}
