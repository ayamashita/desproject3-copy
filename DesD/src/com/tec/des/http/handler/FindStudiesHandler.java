package com.tec.des.http.handler;
import javax.servlet.http.*;
import com.tec.shared.util.*;
import com.tec.shared.exceptions.UserException;
import com.tec.des.http.*;
import com.tec.des.dto.SearchDTO;
import com.tec.des.command.FindStudiesCommand;
import com.tec.des.command.GetStudyTypesCommand;
import com.tec.des.util.Validator;

/**
 * 
 * Handles the find studies use case. 
 *
 * @author Norunn Haug Christensen
 */
public class FindStudiesHandler extends UsecaseHandler {

  /**
   * Handles the use case by executing the command.
   * Validation of the search criterias is done before the search is performed.
   *
   * @return The name of the resulting view. 
   * @throws Exception
   */
  public String doHandle(HttpServletRequest request) throws Exception {
    
    SearchDTO searchCriteria = getSearchCriteria(request);   
    putSearchParameterOnRequest(searchCriteria, request);

    Validator.validateSearchCriteria(searchCriteria);      
    FindStudiesCommand command = new FindStudiesCommand();
    command.setSearchCriteria(searchCriteria); 
    command.setMaxHitListLength(Parser.parseInt(request.getParameter(WebKeys.REQUEST_PARAM_MAX_HIT_LIST_LENGTH)));
    command.setStartPosition(Parser.parseInt(request.getParameter(WebKeys.REQUEST_PARAM_START_POSITION)));
    command.execute();

    if (command.getResult().isEmpty())
        throw new UserException(ExceptionMessages.NO_HITS); 
    request.setAttribute(WebKeys.REQUEST_BEAN_SEARCH_RESULT, command.getResult());
    
    String view;
    String printerFriendly = request.getParameter(WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY);
    if (!Nuller.isReallyNull(printerFriendly) && Parser.parseBoolean(printerFriendly)) {
        view = WebConstants.VIEW_STUDY_OVERVIEW_REPORT_PF;
    } else {
        view = WebConstants.VIEW_STUDY_OVERVIEW_REPORT;
    }

    return view;
  }
  
  /**
   * Returns a SearchDTO containing the search criteria.
   */
  private SearchDTO getSearchCriteria(HttpServletRequest request) {
    SearchDTO search = new SearchDTO();
    search.setFreeText(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_TEXT));
    search.setTypeOfStudy(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_TYPE_OF_STUDY));
    search.setEndOfStudyFrom(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_END_DATE_FROM));    
    search.setEndOfStudyTo(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_END_DATE_TO));    
    search.setStudyResponsibles(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_STUDY_RESPONSIBLES));    
    search.setSortBy(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_SORT_BY));
    String directionAttr = request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_SORT_DESCENDING);
	if ((directionAttr == null) || !Boolean.valueOf(directionAttr).booleanValue()) {
        search.setSortOrder(WebConstants.SORT_ORDER_ASCENDING);    
    } else {
        search.setSortOrder(WebConstants.SORT_ORDER_DESCENDING);    
    }

    return search;
  }
  
  /**
   * Put the search criteria and the study types on the request so that they are available
   * if an exception occur and the user is returned to the sender view.
   * @throws Exception
   */
  private void putSearchParameterOnRequest(SearchDTO searchCriteria, HttpServletRequest request) throws Exception {
    
    request.setAttribute(WebKeys.REQUEST_BEAN_SEARCH_CRITERIA, searchCriteria);
    GetStudyTypesCommand command = new GetStudyTypesCommand();
    command.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_TYPES, command.getResult());
      
  }
  
  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Nuller.getIntNull();
  }
  
}