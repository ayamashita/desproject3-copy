package com.tec.des.http.handler;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.ReportReadCommand;
import com.tec.des.command.ReportRunCommand;
import com.tec.des.dto.ReportDTO;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.shared.util.Nuller;

public class ReportRunHandler extends UsecaseHandler {

	public String doHandle(HttpServletRequest request) throws Exception {
		// read report details
		ReportDTO report = new ReportDTO();
	    report.setId(Integer.parseInt(Nuller.noNull(request.getParameter(WebKeys.REQUEST_PARAM_REPORT_ID))));
		ReportReadCommand readCommand = new ReportReadCommand();
		readCommand.setReport(report);
		readCommand.execute();
		request.setAttribute(WebKeys.REQUEST_BEAN_REPORT, report);
		
		// run the report
		ReportRunCommand runCommand = new ReportRunCommand();
		runCommand.setReport(report);
		runCommand.execute();
	    request.setAttribute(WebKeys.REQUEST_BEAN_DISPLAYTAG_LIST, runCommand.getResult());

	    return WebConstants.VIEW_REPORT_RUN;
	}

	public int getRequiredRole() {
		return Nuller.getIntNull();
	}

}
