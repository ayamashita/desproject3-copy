package com.tec.server.taglib;

import javax.servlet.jsp.tagext.*;

/**
 * Simple List iteration JSP tag extra info class.
 *
 * @author Norunn Haug Christensen
 */
public class ListIterationTEI extends TagExtraInfo {
   public ListIterationTEI() {
      super();
   }

   public VariableInfo[] getVariableInfo(TagData data) {
      VariableInfo infoGroup
         = new VariableInfo(
            data.getAttributeString("name"),
            data.getAttributeString("type"),	
            true,
            VariableInfo.NESTED);
      VariableInfo infoAlternator
         = new VariableInfo(
            data.getAttributeString("rowAlternator"),
            "String",	
            true,
            VariableInfo.NESTED);            
      VariableInfo [] info = { infoGroup, infoAlternator };
      return info;
   }
}