
package com.tec.desws;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import com.tec.des.util.Constants;


public class DesWSClientImpl {
	/**
	 * low-level calling of the service
	 * @param action
	 * @param hash
	 * @return
	 * @throws MalformedURLException
	 * @throws XmlRpcException
	 */
	private static Object[] callService(String action, HashMap hash) throws MalformedURLException, XmlRpcException {
		XmlRpcClientConfigImpl rpcConfig = new XmlRpcClientConfigImpl();
		XmlRpcClient rcpClient = new XmlRpcClient();

	    ResourceBundle resource = ResourceBundle.getBundle(Constants.DES_PROPERTIES_FILE);
		rpcConfig.setServerURL(new URL(resource.getString("desdb_webservice_url")));
		rcpClient.setConfig(rpcConfig);

		Object[] methodParams = new Object[] { hash };
		Object resultSet = rcpClient.execute(action, methodParams);
		Object[] elementList = (Object[]) resultSet;
		return elementList;
	}

	/**
	 * Acts like normal hashmap, but does not fail when putting null value.
	 * In addition, ignores all "" (empty string) values.
	 * It is just helper class to avoid if (value != null)... see below.
	 * @author junm
	 *
	 */
	private static class HashMapHandlingNulls extends HashMap {
		public Object put(Object key, Object val) {
			if ((val != null) && !"".equals(val)) {
				return super.put(key, val);
			}
			else {
				return null;
			}
		}
	}
	/**
	 * returns all publications based on given filter and sorted by given field.
	 * @param hash
	 * @return
	 */
	public static Map getPublications(WSPublication filter, String sortOn) {
		try {
			HashMap hash = new HashMapHandlingNulls();
			if (filter != null) {
				hash.put("id", filter.getId());
				hash.put("title", filter.getTitle());
				hash.put("source", filter.getSource());
				hash.put("abstract", filter.getPubAbstract());
				hash.put("authors", filter.getAuthors());
			}
			hash.put("sort_on", sortOn);
			
		    ResourceBundle resource = ResourceBundle.getBundle(Constants.DES_PROPERTIES_FILE);
			Object[] serviceResult = callService(resource.getString("desdb_webservice_function_publications"), hash);
			
			LinkedHashMap publications = new LinkedHashMap(serviceResult.length);
			for (int i = 0; i < serviceResult.length; i++) {
				HashMap rec = (HashMap) serviceResult[i];
				WSPublication p = new WSPublication()
					.setId((String)rec.get("id"))
					.setTitle((String)rec.get("title"))
					.setUrl((String)rec.get("url"))
					.setSource((String)rec.get("source"))
					.setPubAbstract((String)rec.get("abstract"));
				publications.put(p.getId(), p);
			}
			
			return publications;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * returns all people based on given filter and sorted by given field
	 * @param hash
	 * @return
	 */
	public static Map getPeople(WSPerson filter, String sortOn) {
		try {
			HashMap hash = new HashMapHandlingNulls();
			if (filter != null) {
				hash.put("id", filter.getId());
				hash.put("firstname", filter.getFirstname());
				hash.put("lastname", filter.getLastname());
				hash.put("password", filter.getPassword());
			}
			hash.put("sort_on", sortOn);

		    ResourceBundle resource = ResourceBundle.getBundle(Constants.DES_PROPERTIES_FILE);
			Object[] serviceResult = callService(resource.getString("desdb_webservice_function_people"), hash);
			LinkedHashMap people = new LinkedHashMap(serviceResult.length);
			for (int i = 0; i < serviceResult.length; i++) {
				HashMap rec = (HashMap) serviceResult[i];
				WSPerson newPerson = new WSPerson()
					.setId((String)rec.get("id"))
					.setFirstname((String)rec.get("firstname"))
					.setLastname((String)rec.get("lastname"))
					.setUrl((String)rec.get("url"))
					.setJobTitle((String)rec.get("jobtitle"));
				
				people.put(newPerson.getId(), newPerson);
			}
			return people;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * remove characters like %20, etc.
	 * @param value
	 * @return
	 */
	static String correctValue(String value) {
		if (value == null) {
			return null;
		}
		
		try {
			return URLDecoder.decode(value, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RuntimeException("Encoding UTF-8 not supported.", e);
		}
	}

	public static boolean checkUser(String username, String password) {
		HashMap hash = new HashMap();
		hash.put("login", username);
		hash.put("password", password);

		XmlRpcClientConfigImpl rpcConfig = new XmlRpcClientConfigImpl();
		XmlRpcClient rcpClient = new XmlRpcClient();

		try {
			ResourceBundle resource = ResourceBundle.getBundle(Constants.DES_PROPERTIES_FILE);
			rpcConfig.setServerURL(new URL(resource.getString("desdb_webservice_auth_url")));
			rcpClient.setConfig(rpcConfig);

			Object[] methodParams = new Object[] { hash };
			Object resultSet = rcpClient.execute(resource.getString("desdb_webservice_function_auth"), methodParams);
			Boolean result = (Boolean) resultSet;

			return result.booleanValue();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
