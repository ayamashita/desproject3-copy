package com.tec.desws;

/**
 * Container class for publications
 * @author junm
 *
 */
public class WSPublication {
	public static final String SORT_ON_TITLE = "title";
	private String id;
	private String title;
	private String url;
	private String source;
	private String pubAbstract;
	private String authors;

	
	public String getId() {
		return id;
	}
	public String getTitle() {
		return title;
	}
	public WSPublication() {
		super();
	}
	public String getUrl() {
		return url;
	}
	public WSPublication setId(String id) {
		this.id = id;
		return this;
	}
	public WSPublication setTitle(String title) {
		this.title = DesWSClientImpl.correctValue(title);
		return this;
	}
	public WSPublication setUrl(String url) {
		this.url = url;
		return this;
	}
	public String getSource() {
		return source;
	}
	public WSPublication setSource(String source) {
		this.source = source;
		return this;
	}
	public String getPubAbstract() {
		return pubAbstract;
	}
	public WSPublication setPubAbstract(String pubAbstract) {
		this.pubAbstract = pubAbstract;
		return this;
	}
	public String getAuthors() {
		return authors;
	}
	public WSPublication setAuthors(String authors) {
		this.authors = authors;
		return this;
	}
}