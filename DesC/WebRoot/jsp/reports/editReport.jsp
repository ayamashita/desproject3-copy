<!-- THIS IS THE STUDY SEARCH PAGE --->
<%@ page import="no.machina.simula.*, java.util.*, org.apache.commons.lang.*" %>
<%@page import="no.machina.simula.reports.CustomizedReport"%>
<%@page import="no.machina.simula.reports.AvailableColumns"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<!-- Include standard header -->
<jsp:include page="/jsp/header.jsp" />
<script language="JavaScript" src="<%=request.getContextPath() %>/jsp/selectbox.js"></script>
<script language="JavaScript">
	function checkForm() {
		var validated = true;
		var message = "Errors:\n";
		if (document.getElementById("name").value == '') {
			message += "Name of the report is mandatory.\n";
			validated = false;
		}
		
		if (document.getElementById("columns_sel").options.length == 0) {
			message += "At least one column must be selected.\n";
			validated = false;
		}
		
		if (!validated) {
			window.alert(message);
		}
		
		return validated;
	}
</script>

<!-- MAIN BODY -->


<tr>

<td width="15%">
&nbsp;
</td>

<td width="45%">

<p>
<% CustomizedReport cr = (CustomizedReport) session.getAttribute("customizedReport"); %>
<span class="path">personalised reports > <%= cr.getName()%></span>

<form action="<%=request.getContextPath() %>/CustomizedReportsEdit">
	<input type="hidden" name="action" value="save">
	<input type="hidden" name="id" value="<%= cr.getId()%>">
	<table width="100%">
		<tr>
			<td>
				Report name
			</td>
			<td>
				<input id="name" type="text" name="name" value="<%= cr.getName()%>">
			</td>
		</tr>
		<tr>
			<td>
				Columns
			</td>
			<td>
				<table>
					<tr><td>
						<select ondblclick="moveSelectedOptions(this.form['columns_all'],this.form['columns_sel'],true)" style="width:100%" class="input" name="columns_all" size="8" multiple>
							<%
								for (Iterator iterator = AvailableColumns.getValues().iterator(); iterator.hasNext();) {
									AvailableColumns col = (AvailableColumns) iterator.next();
							 		if (!cr.getSelectedColumns().contains(col.getName())) {
							 %>
							 
										<option value="<%=col.getName() %>"><%=col.getName() %></option>
							<% 
									}
								} 
							%>
						</select>
					</td><td>
						<input type="button" value="->" onclick="moveSelectedOptions(this.form['columns_all'],this.form['columns_sel'],false)"><br><br>
						<input type="button" value="<-" onclick="moveSelectedOptions(this.form['columns_sel'],this.form['columns_all'],false)">
					</td><td>
						<select id="columns_sel" ondblclick="moveSelectedOptions(this.form['columns_sel'],this.form['columns_all'],true)" style="width:100%" class="input" name="columns_sel" size="8" multiple>
							<%
								for (Iterator iterator = cr.getSelectedColumns().iterator(); iterator.hasNext();) {
									String col = (String) iterator.next();
							 %>
							 
										<option value="<%=col %>"><%=col %></option>
							<% 
								} 
							%>
						</select>
					</td><td>
						<input type="button" value="Up" onclick="moveSelectedOptionsUp(this.form['columns_sel'])"><br><br>
						<input type="button" value="Down" onclick="moveSelectedOptionsDown(this.form['columns_sel'])">
					</td></tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				Conditions
			</td>
			<td>
				<table>
					<tr><td>
						<select ondblclick="moveSelectedOptions(this.form['responsibles_all'],this.form['responsibles_sel'],true)" style="width:100%" class="input" name="responsibles_all" size="8" multiple>
							<%
								List selectedResponsibles = new ArrayList(); // this list is used to prevent reading the people details repeatedly
								for (Iterator iterator = DB.getInstance().getAllResponsibles().iterator(); iterator.hasNext();) {
									Responsible resp = (Responsible) iterator.next();
							 		if (!cr.getSelectedResponsibles().contains(resp.getId())) {
							 %>
										<option value="<%=resp.getId() %>"><%=resp.getFirstName() + " " + resp.getFamilyName() %></option>
							<% 
									}
									else {
										selectedResponsibles.add(resp);
									}									
								} 
							%>
						</select>
					</td><td>
						<input type="button" value="->" onclick="moveSelectedOptions(this.form['responsibles_all'],this.form['responsibles_sel'],true)"><br><br>
						<input type="button" value="<-" onclick="moveSelectedOptions(this.form['responsibles_sel'],this.form['responsibles_all'],true)">
					</td><td>
						<select ondblclick="moveSelectedOptions(this.form['responsibles_sel'],this.form['responsibles_all'],true)" style="width:100%" class="input" name="responsibles_sel" size="8" multiple>
							<%
								for (Iterator iterator = selectedResponsibles.iterator(); iterator.hasNext();) {
									Responsible resp = (Responsible) iterator.next();
							 %>
									<option value="<%=resp.getId() %>"><%=resp.getFamilyName()%>, <%=resp.getFirstName()%></option>
							<% 
								} 
							%>
						</select>
					</td></tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				Operator
			</td>
			<td>
				<select name="conditionOperator">
					<option name="AND" selected="<%="AND".equals(cr.getConditionOperator()) %>">AND</option>
					<option name="OR" selected="<%="OR".equals(cr.getConditionOperator()) %>">OR</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				Order by time
			</td>
			<td>
				<input type="checkbox" name="orderByTime" <% if (cr.isOrderByTime()) out.print("checked"); %>>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="button" value="Save" onclick="if (checkForm()) {selectAllOptions(this.form['columns_sel']); selectAllOptions(this.form['responsibles_sel']);this.form.submit();}">
				<input type="button" value="Cancel" onclick="window.location.replace('<%=request.getContextPath() %>/CustomizedReports');">
			</td>
		</tr>
	</table>
</form>

</td>

<td valign="top" width="15%">
<br><br><br>
<jsp:include page="/jsp/menu.jsp" />
</td>


<td width="10%">
&nbsp;
</td>

</tr>

</table>



<!-- Include standard footer -->
<br><br>
<jsp:include page="/jsp/footer.jsp" />
