<!-- THIS IS THE STUDY SEARCH PAGE --->
<%@ page import="no.machina.simula.*, java.util.*, org.apache.commons.lang.*" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<!-- Include standard header -->
<jsp:include page="/jsp/header.jsp" />


<!-- MAIN BODY -->


<tr>

<td width="15%">
&nbsp;
</td>

<td width="45%">

<p>
<span class="path">personalised reports</span>
<br>
<display:table name="list">
	<display:column property="name" url="/CustomizedReportsEdit" paramId="id" paramProperty="id"/>
	<display:column title="run" value="run report" url="/CustomizedReportsRun" paramId="id" paramProperty="id"/>
	<display:column title="delete" value="delete report" url="/CustomizedReportsEdit?action=delete" paramId="id" paramProperty="id"/>
</display:table>

<br>
<input type="button" value="Create new report" onclick="window.location.replace('<%=request.getContextPath() %>/CustomizedReportsEdit?action=create');">

</td>

<td valign="top" width="15%">
<br><br><br>
<jsp:include page="/jsp/menu.jsp" />
</td>


<td width="10%">
&nbsp;
</td>

</tr>

</table>



<!-- Include standard footer -->
<br><br>
<jsp:include page="/jsp/footer.jsp" />
