<!-- THIS IS THE STUDY SEARCH PAGE --->
<%@ page import="no.machina.simula.*,java.util.*,org.apache.commons.lang.*" %>
<%@page import="no.machina.simula.reports.CustomizedReport"%>
<%@page import="no.machina.simula.reports.AvailableColumns"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<!-- Include standard header -->
<jsp:include page="/jsp/header.jsp" />


<!-- MAIN BODY -->


<tr>

<td width="15%">
&nbsp;
</td>

<td width="45%">

<p>
<% CustomizedReport cr = (CustomizedReport) session.getAttribute("customizedReport"); %>
<span class="path">personalised reports > <%= cr.getName()%></span>
<br>
<display:table name="list" export="true" 
	requestURI="<%=request.getContextPath() + "/CustomizedReportsRun"%>" 
	pagesize="<%=Integer.parseInt(System.getProperty("MaxRecordsInSearchList")) %>">
	<% 
	for (Iterator iter=cr.getSelectedColumns().iterator(); iter.hasNext();) {
		AvailableColumns column = AvailableColumns.getByName(iter.next().toString());
	%>
		<display:column title="<%=column.getDisplayableColumnName() %>" property="<%= column.getOutputColumn()%>" decorator="<%=column.getDecoratorClass() %>"/>
	<% } %>
</display:table>

<br>
<input type="button" value="Back" onclick="window.location.replace('<%=request.getContextPath() %>/CustomizedReports');">

</td>


<td width="10%">
&nbsp;
</td>

</tr>

</table>



<!-- Include standard footer -->
<br><br>
<jsp:include page="/jsp/footer.jsp" />
