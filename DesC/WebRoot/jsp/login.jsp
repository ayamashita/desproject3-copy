<%@page import="org.apache.commons.httpclient.*"%>
<%@page import="org.apache.commons.httpclient.methods.*"%>
<%@page import="org.apache.commons.httpclient.protocol.*"%>
<%@page import="org.apache.commons.httpclient.cookie.*"%>
<%@page import="org.apache.commons.httpclient.contrib.ssl.*"%>
<%@page import="org.apache.commons.httpclient.contrib.utils.*"%>
<%@page import="java.util.*"%>
<%@page import="no.machina.simula.desws.DesWSClientImpl"%>


<%
	String userId = request.getParameter("userId");	
	String password = request.getParameter("password");
	boolean successfulLogin = false;
	
	if (userId != null && !"".equals(userId)) {
		successfulLogin = DesWSClientImpl.loginUser(session, userId, password);
	}
	
	if (successfulLogin) {
		response.sendRedirect(request.getContextPath() + "/jsp/admin/main.jsp");
	}
	else {
%>

<jsp:include page="header.jsp" />

<form name="form1" method="post" action="login.jsp">
  <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" bordercolor="0">
    <tr>
    <td height="400" valign="middle"> 
        <table width="450" border="0" align="center" cellpadding="1" cellspacing="1">
          <tr align="center"> 
            <td colspan="2" height="35">Simula People Login</td>
          </tr>
          <tr> 
            <td height="35" align="center">User id</td>
            <td height="35" align="center" > 
              <input type="text" name="userId" maxlength="50" size="20" value="<%=((userId==null)?"":userId) %>">
            </td>
          </tr>
          <tr> 
            <td align="center" height="35">Password</td>
            <td height="35" align="center" > 
              <input type="password" name="password" size="20" maxlength="20">
            </td>
          </tr>
          <tr align="center"> 
            <td colspan="2" height="35"> 
              <input type="submit" name="Submit" value="Submit">
              <input type="reset" name="reset" value="Reset">
            </td>
          </tr>
          <tr> 
            <td colspan="4">&nbsp;</td>
          </tr>
        </table>
    </td>
  </tr>
</table>

<%
	}
%>


<jsp:include page="footer.jsp" />
