package no.machina.simula.reports.decorators;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.servlet.jsp.PageContext;

import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.exception.DecoratorException;
import org.displaytag.properties.MediaTypeEnum;

public class DateDecorator implements DisplaytagColumnDecorator {

	public Object decorate(Object columnValue, PageContext pageContext, MediaTypeEnum media) throws DecoratorException {
        if (columnValue == null) {
        	return null;
        }
        DateFormat df = new SimpleDateFormat(System.getProperty("DateFormat"), new Locale("en", "GB"));
        
        return df.format(columnValue);
	}

}
