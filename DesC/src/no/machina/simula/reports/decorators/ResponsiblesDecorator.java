package no.machina.simula.reports.decorators;

import java.util.List;
import java.util.ListIterator;

import javax.servlet.jsp.PageContext;

import no.machina.simula.DB;
import no.machina.simula.Responsible;

import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.exception.DecoratorException;
import org.displaytag.properties.MediaTypeEnum;

public class ResponsiblesDecorator implements DisplaytagColumnDecorator {

	public Object decorate(Object columnValue, PageContext pageContext, MediaTypeEnum media) throws DecoratorException {
		StringBuffer result = new StringBuffer();
        List responsibles;
		try {
			responsibles = DB.getInstance().getResponsiblesForStudy(((Integer)columnValue).intValue());
		} catch (Exception e) {
			e.printStackTrace();
			throw new DecoratorException(this.getClass(), "Error.", e);
		}
        for (ListIterator j = responsibles.listIterator(); j.hasNext(); ) {
            Responsible r = (Responsible) j.next();
            if (MediaTypeEnum.HTML.equals(media)) {
	            result.append("<a href=\"").append(r.getUrl()).append("\">").append(r.getFirstName()).append(" ").append(r.getFamilyName()).append("</a>");
	            if (j.hasNext()) {
	            	result.append("<br>");
	            }
            }
            else {
	            result.append(r.getFirstName()).append(" ").append(r.getFamilyName());
	            if (j.hasNext()) {
	            	result.append("\n");
	            }
            }
        }
        
        return result.toString();
	}

}
