package no.machina.simula.reports.decorators;

import java.util.List;
import java.util.ListIterator;

import javax.servlet.jsp.PageContext;

import no.machina.simula.DB;
import no.machina.simula.Publication;

import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.exception.DecoratorException;
import org.displaytag.properties.MediaTypeEnum;

public class PublicationsDecorator implements DisplaytagColumnDecorator {

	public Object decorate(Object columnValue, PageContext pageContext, MediaTypeEnum media) throws DecoratorException {
		StringBuffer result = new StringBuffer();
        List publications;
		try {
			publications = DB.getInstance().getPublicationsForStudy(((Integer)columnValue).intValue());
		} catch (Exception e) {
			e.printStackTrace();
			throw new DecoratorException(this.getClass(), "Error.", e);
		}
        for (ListIterator j = publications.listIterator(); j.hasNext(); ) {
            Publication p = (Publication) j.next();
            if (MediaTypeEnum.HTML.equals(media)) {
	            result.append("<a href=\"").append(p.getUrl()).append("\" title=\"").append(p.getTitle()).append("\">").append(p.getShortTitle()).append("</a>");
	            if (j.hasNext()) {
	            	result.append("<br>");
	            }
            }
            else {
	            result.append(p.getShortTitle());
	            if (j.hasNext()) {
	            	result.append("\n");
	            }
            }
        }
        
        return result.toString();
	}

}
