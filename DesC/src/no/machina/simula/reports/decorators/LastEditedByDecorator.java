package no.machina.simula.reports.decorators;

import javax.servlet.jsp.PageContext;

import no.machina.simula.Audit;
import no.machina.simula.DB;

import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.exception.DecoratorException;
import org.displaytag.properties.MediaTypeEnum;

public class LastEditedByDecorator implements DisplaytagColumnDecorator {

	public Object decorate(Object columnValue, PageContext pageContext, MediaTypeEnum media) throws DecoratorException {
        try {
            Audit lastAudit = DB.getInstance().getMostRecentAudit(((Integer)columnValue).intValue());
			if (lastAudit != null) {
		        return lastAudit.getUserId();
			}
			else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DecoratorException(this.getClass(), "Error.", e);
		}
	}

}
