package no.machina.simula.reports;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.machina.simula.DB;

public class ListReportsServlet extends DisplaytagSupportServlet {
	
	protected void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			String sql = "select id, name from tbl_personalised_report where user_id=?";
			Connection con = DB.getInstance().getConnection();
			PreparedStatement stat = con.prepareStatement(sql);
			stat.setString(1, (String) req.getSession().getAttribute("simula_userid"));

			ResultSet rs = stat.executeQuery();
			saveResultsForDisplaytag(req, rs);

			con.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
        forward(req, resp, "/jsp/reports/listReports.jsp");
	}
}
