package no.machina.simula.reports;

import java.io.IOException;
import java.sql.Connection;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.machina.simula.DB;

public class EditReportsServlet extends MyServlet {

	protected void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		CustomizedReport cr = new CustomizedReport();
		req.getSession().setAttribute("customizedReport", cr);
		
		String action = req.getParameter("action");
		// view is the default action
		if (action == null) {
			action = "view";
		}
		
		String forward = "/jsp/reports/editReport.jsp";
		// view existing record for editing
		if ("view".equals(action)) {
			doView(req, cr);
			forward(req, resp, forward);
		}		
		else if ("create".equals(action)) {
			// do nothing
			forward(req, resp, forward);
		}		
		else if ("delete".equals(action)) {
			try {
				Connection con = DB.getInstance().getConnection();
				cr.setId(Integer.parseInt(req.getParameter("id")));
				cr.delete(con);
			    con.close();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			resp.sendRedirect(req.getContextPath() + "/CustomizedReports");
		}		
		// save the edited record
		else if ("save".equals(action)) {
			doSave(req, cr);
			resp.sendRedirect(req.getContextPath() + "/CustomizedReports");
		}
	}

	private void doView(HttpServletRequest req, CustomizedReport cr) {
		try {
			Connection con = DB.getInstance().getConnection();
			int id = Integer.parseInt(req.getParameter("id"));
			cr.read(con, id);
		    con.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private void doSave(HttpServletRequest req, CustomizedReport cr) {
		fillFromRequest(req, cr);
		
		try {
			Connection con = DB.getInstance().getConnection();
			cr.save(con);
		    con.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private void fillFromRequest(HttpServletRequest req, CustomizedReport cr) {
		cr.setId(Integer.parseInt(req.getParameter("id")));
		cr.setName(req.getParameter("name"));
		cr.setOrderByTime(req.getParameter("orderByTime") != null);
		cr.setConditionOperator(req.getParameter("conditionOperator"));
		cr.setUserId((String) req.getSession().getAttribute("simula_userid"));
		
		cr.setSelectedColumns(Arrays.asList(req.getParameterValues("columns_sel")));
		
		String[] responsibles = req.getParameterValues("responsibles_sel");
		if (responsibles == null) {
			responsibles = new String[0];
		}
		cr.setSelectedResponsibles(Arrays.asList(responsibles));
	}

}
