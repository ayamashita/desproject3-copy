package no.machina.simula.reports;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.machina.simula.DB;

public class RunReportsServlet extends DisplaytagSupportServlet {

	protected void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// read the report definition
		Connection con;
		CustomizedReport cr = new CustomizedReport();
		try {
			con = DB.getInstance().getConnection();
			int id = Integer.parseInt(req.getParameter("id"));
			cr.read(con, id);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
        
		// construct the sql query
		StringBuffer sql = constructSqlQuery(cr);

		// prepare prepared statement and fill parameters
		runQuery(con, cr, sql, req);
		
		try {
		    con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		req.getSession().setAttribute("customizedReport", cr);
		forward(req, resp, "/jsp/reports/runReport.jsp");
	}

	/**
	 * Fills parameters and runs the query. Result will be saved in session for displaying it in displaytag.
	 * @param con
	 * @param cr
	 * @param sql
	 * @param req
	 */
	private void runQuery(Connection con, CustomizedReport cr, StringBuffer sql, HttpServletRequest req) {
		try {
			PreparedStatement stat = con.prepareStatement(sql.toString());
			int i = 1;
			for (Iterator iter = cr.getSelectedResponsibles().iterator(); iter.hasNext();) {
				String respId = (String) iter.next();
				stat.setString(i++, respId);
			}
			
			// run the query
			ResultSet rs = stat.executeQuery();
			saveResultsForDisplaytag(req, rs);
			
		} catch (SQLException e1) {
			throw new RuntimeException(e1);
		}
	}

	/**
	 * constructs the sql query for the report
	 * @param cr
	 * @return
	 */
	private StringBuffer constructSqlQuery(CustomizedReport cr) {
		StringBuffer sql = new StringBuffer().append("select distinct s.study_id");
		for (Iterator iter = cr.getSelectedColumns().iterator(); iter.hasNext();) {
			String colName = (String) iter.next();
			AvailableColumns column = AvailableColumns.getByName(colName);
			
			// skip non-database columns, like publications, etc.
			if (column.getDbColumnName() != null) {
				sql.append(", ");
				sql.append(column.getDbColumnName());
			}
		}
		sql.append(" from tbl_study s ")
			.append("left outer join tbl_study_has_responsible r on (r.study_id = s.study_id) ")
			.append("left outer join tbl_study_type t on (t.study_type_id = s.study_type_id) ")
			.append("left outer join tbl_unit u on (u.unit_id = s.study_duration_unit_id) ");
		StringBuffer where = new StringBuffer();
		for (Iterator iter = cr.getSelectedResponsibles().iterator(); iter.hasNext(); iter.next()) {
			if (where.length() > 0) {
				where.append(" ").append(cr.getConditionOperator());
			}
			where.append(" (r.responsible_id=?)");
		}
		
		if (where.length() > 0) {
			sql.append(" where").append(where);
		}
		
		sql.append(" order by ").append(AvailableColumns.study_type.getDbColumnName());
		if (cr.isOrderByTime()) {
			sql.append(", ").append(AvailableColumns.study_end_date.getName());
		}
		
		return sql;
	}

}
