package no.machina.simula.reports;

import java.util.List;

import no.machina.simula.reports.decorators.DateDecorator;
import no.machina.simula.reports.decorators.LastEditedByDecorator;
import no.machina.simula.reports.decorators.OwnerDecorator;
import no.machina.simula.reports.decorators.PublicationsDecorator;
import no.machina.simula.reports.decorators.ResponsiblesDecorator;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.enums.Enum;

public class AvailableColumns extends Enum {

	public static final AvailableColumns publications = new AvailableColumns("publications", PublicationsDecorator.class, null, "study_id");
	public static final AvailableColumns responsibles = new AvailableColumns("responsibles", ResponsiblesDecorator.class, null, "study_id");
	public static final AvailableColumns study_owner = new AvailableColumns("study_owner", OwnerDecorator.class, null, "study_id");
	public static final AvailableColumns last_edited_by = new AvailableColumns("last_edited_by", LastEditedByDecorator.class, null, "study_id");
	public static final AvailableColumns study_name = new AvailableColumns("study_name");
	public static final AvailableColumns study_type = new AvailableColumns("study_type", "study_type_name");
	public static final AvailableColumns study_desc = new AvailableColumns("study_desc");
	public static final AvailableColumns study_duration = new AvailableColumns("study_duration");
	public static final AvailableColumns study_duration_unit = new AvailableColumns("study_duration_unit", "unit_name");
	public static final AvailableColumns study_start_date = new AvailableColumns("study_start_date", DateDecorator.class);
	public static final AvailableColumns study_end_date = new AvailableColumns("study_end_date", DateDecorator.class);
	public static final AvailableColumns keywords = new AvailableColumns("keywords");
	public static final AvailableColumns no_of_students = new AvailableColumns("no_of_students");
	public static final AvailableColumns no_of_professionals = new AvailableColumns("no_of_professionals");
	public static final AvailableColumns study_notes = new AvailableColumns("study_notes");
    
	private String dbColumnName;
	/** column decorator for displaytag */
	private String decoratorClassName;
	private String outputColumn;
	
	protected AvailableColumns(String name) {
		this(name, name);
	}
	protected AvailableColumns(String name, String dbColumnName) {
		this(name, null, dbColumnName, dbColumnName);
	}
	protected AvailableColumns(String name, Class decoratorClass) {
		this(name, decoratorClass, name, name);
	}
	
	protected AvailableColumns(String name, Class decoratorClass, String dbColumnName, String outputColumn) {
		super(name);
		this.dbColumnName = dbColumnName;
		if (decoratorClass != null) {
			this.decoratorClassName = decoratorClass.getName();
		}
		this.outputColumn = outputColumn;
	}

	public static List getValues() {
		return getEnumList(AvailableColumns.class);
	}
	public String getDbColumnName() {
		return dbColumnName;
	}
	public static AvailableColumns getByName(String name) {
		return (AvailableColumns) getEnum(AvailableColumns.class, name);
	}
	public String getDecoratorClass() {
		return decoratorClassName;
	}
	public String getOutputColumn() {
		return outputColumn;
	}
	public String getDisplayableColumnName() {
		return StringUtils.capitalize(StringUtils.replace(getName(), "_", " "));
	}
}
