package no.machina.simula.reports;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

public abstract class DisplaytagSupportServlet extends MyServlet {

	protected void saveResultsForDisplaytag(HttpServletRequest req, ResultSet rs) throws SQLException {
		ArrayList list = new ArrayList();

		while(rs.next()) {
			HashMap rec = new HashMap();
			for(int i=1; i<=rs.getMetaData().getColumnCount(); i++) {
				rec.put(rs.getMetaData().getColumnName(i), rs.getObject(i));
			}
			list.add(rec);
		}
		rs.close();

		req.setAttribute("list", list);
	}

}
