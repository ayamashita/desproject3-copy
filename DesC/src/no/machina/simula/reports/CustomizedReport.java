package no.machina.simula.reports;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CustomizedReport {
	int id;
	String name = "";
	boolean orderByTime = false;
	String conditionOperator;
	List selectedColumns = new ArrayList();
	List selectedResponsibles = new ArrayList();
	String userId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isOrderByTime() {
		return orderByTime;
	}
	public void setOrderByTime(boolean orderByTime) {
		this.orderByTime = orderByTime;
	}
	public String getConditionOperator() {
		return conditionOperator;
	}
	public void setConditionOperator(String conditionOperator) {
		this.conditionOperator = conditionOperator;
	}

	public void read(Connection con, int id) throws SQLException {
		String sql = "select * from tbl_personalised_report where id=?";
		PreparedStatement stat = con.prepareStatement(sql);
		stat.setInt(1, id);
		ResultSet rs = stat.executeQuery();
		
		rs.next();
		setId(rs.getInt("id"));
		setName(rs.getString("name"));
		setConditionOperator(rs.getString("conditionOperator"));
		setOrderByTime(rs.getBoolean("orderByTime"));
		
		rs.close();
		
		readColumns(con, id);
		readFilterConditions(con, id);
		
		
	}
	private void readFilterConditions(Connection con, int id) throws SQLException {
		String sql;
		PreparedStatement stat;
		ResultSet rs;
		sql = "select * from tbl_personalised_report_filter where report_id=?";
		stat = con.prepareStatement(sql);
		stat.setInt(1, id);
		rs = stat.executeQuery();
		selectedResponsibles = new ArrayList();
		while(rs.next()) {
			selectedResponsibles.add(rs.getString("value"));
		}
		rs.close();
	}
	private void readColumns(Connection con, int id) throws SQLException {
		String sql;
		PreparedStatement stat;
		ResultSet rs;
		sql = "select * from tbl_personalised_report_columns where report_id=? order by seq";
		stat = con.prepareStatement(sql);
		stat.setInt(1, id);
		rs = stat.executeQuery();
		selectedColumns = new ArrayList();
		while(rs.next()) {
			selectedColumns.add(rs.getString("column_name"));
		}
		rs.close();
	}
	public List getSelectedColumns() {
		return selectedColumns;
	}
	public void setSelectedColumns(List selectedColumns) {
		this.selectedColumns = selectedColumns;
	}
	public void save(Connection con) throws SQLException {
		if (getId() == 0) {
			insert(con);
		}
		else {
			update(con);
		}
		saveColumns(con);
		saveFilterConditions(con);
	}

	private void update(Connection con) throws SQLException {
		// update basic values (name, etc.)
		String sql = "update tbl_personalised_report set name=?, conditionOperator=?, orderByTime=? where id=?";
		PreparedStatement stat = con.prepareStatement(sql);
		stat.setString(1, getName());
		stat.setString(2, getConditionOperator());
		stat.setBoolean(3, isOrderByTime());
		stat.setInt(4, getId());
		stat.executeUpdate();
		stat.close();
	}
	private void insert(Connection con) throws SQLException {
		// update basic values (name, etc.)
		String sql = "insert into tbl_personalised_report (name, conditionOperator, orderByTime, user_id) values (?, ?, ?, ?)";
		PreparedStatement stat = con.prepareStatement(sql);
		stat.setString(1, getName());
		stat.setString(2, getConditionOperator());
		stat.setBoolean(3, isOrderByTime());
		stat.setString(4, getUserId());
		stat.executeUpdate();
		stat.close();

		if (getId() == 0) {
			sql = "select LAST_INSERT_ID()";
			ResultSet rs = con.createStatement().executeQuery(sql);
			rs.next();
			setId(rs.getInt(1));
			rs.getStatement().close();
		}
	}
	
	private void saveFilterConditions(Connection con) throws SQLException {
		String sql;
		PreparedStatement stat;
		// delete all assigned columns
		deleteById(con, "tbl_personalised_report_filter", "report_id");
		
		// insert newly assigned columns
		sql = "insert into tbl_personalised_report_filter (report_id, value) values (?, ?)";
		stat = con.prepareStatement(sql);
		for (Iterator iterator = getSelectedResponsibles().iterator(); iterator.hasNext();) {
			stat.setInt(1, getId());
			stat.setString(2, (String) iterator.next());
			stat.executeUpdate();
			
			stat.clearParameters();
		}
		stat.close();
	}
	private void deleteById(Connection con, String tableName, String columnName) throws SQLException {
		String sql;
		PreparedStatement stat;
		sql = "delete from " + tableName + " where " + columnName + "=?";
		stat = con.prepareStatement(sql);
		stat.setInt(1, getId());
		stat.executeUpdate();
		stat.close();
	}
	
	private void saveColumns(Connection con) throws SQLException {
		String sql;
		PreparedStatement stat;
		// delete all assigned columns
		deleteById(con, "tbl_personalised_report_columns", "report_id");
		
		// insert newly assigned columns
		sql = "insert into tbl_personalised_report_columns (report_id, column_name, seq) values (?, ?, ?)";
		stat = con.prepareStatement(sql);
		int seq = 0;
		for (Iterator iterator = getSelectedColumns().iterator(); iterator.hasNext();) {
			String col = (String) iterator.next();
			
			stat.setInt(1, getId());
			stat.setString(2, col);
			stat.setInt(3, seq++);
			stat.executeUpdate();
			
			stat.clearParameters();
		}
		stat.close();
	}
	public List getSelectedResponsibles() {
		return selectedResponsibles;
	}
	public void setSelectedResponsibles(List selectedResponsibles) {
		this.selectedResponsibles = selectedResponsibles;
	}
	public void delete(Connection con) throws SQLException {
		deleteById(con, "tbl_personalised_report_columns", "report_id");
		deleteById(con, "tbl_personalised_report_filter", "report_id");
		deleteById(con, "tbl_personalised_report", "id");
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
