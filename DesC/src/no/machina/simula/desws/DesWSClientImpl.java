package no.machina.simula.desws;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;


public class DesWSClientImpl {
	/**
	 * low-level calling of the service
	 * @param action
	 * @param hash
	 * @return
	 * @throws MalformedURLException
	 * @throws XmlRpcException
	 */
	private static Object[] callService(String action, HashMap hash) throws MalformedURLException, XmlRpcException {
		XmlRpcClientConfigImpl rpcConfig = new XmlRpcClientConfigImpl();
		XmlRpcClient rcpClient = new XmlRpcClient();

		rpcConfig.setServerURL(new URL(System.getProperty("no.machina.simula.desdb_webservice")));
		rcpClient.setConfig(rpcConfig);

		Object[] methodParams = new Object[] { hash };
		Object resultSet = rcpClient.execute(action, methodParams);
		Object[] elementList = (Object[]) resultSet;
		return elementList;
	}

	/**
	 * Acts like normal hashmap, but does not fail when putting null value.
	 * It is just helper class to avoid if (value != null)... see below.
	 * @author junm
	 *
	 */
	private static class HashMapHandlingNulls extends HashMap {
		public Object put(Object key, Object val) {
			if (val != null) {
				return super.put(key, val);
			}
			else {
				return null;
			}
		}
	}
	/**
	 * returns all publications based on given filter and sorted by given field.
	 * @param hash
	 * @return
	 */
	public static WSPublication[] getPublications(WSPublication filter, String sortOn) {
		try {
			HashMap hash = new HashMapHandlingNulls();
			if (filter != null) {
				hash.put("id", filter.getId());
				hash.put("title", filter.getTitle());
			}
			hash.put("sort_on", sortOn);
			
			Object[] serviceResult = callService("getPublications", hash);
			
			WSPublication[] publications = new WSPublication[serviceResult.length];
			for (int i = 0; i < serviceResult.length; i++) {
				HashMap rec = (HashMap) serviceResult[i];
				publications[i] = new WSPublication()
					.setId((String)rec.get("id"))
					.setTitle((String)rec.get("title"))
					.setUrl((String)rec.get("url"));
			}
			
			return publications;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * returns all people based on given filter and sorted by given field
	 * @param hash
	 * @return
	 */
	public static WSPerson[] getPeople(WSPerson filter, String sortOn) {
		try {
			HashMap hash = new HashMapHandlingNulls();
			if (filter != null) {
				hash.put("id", filter.getId());
				hash.put("firstname", filter.getFirstname());
				hash.put("lastname", filter.getLastname());
			}
			hash.put("sort_on", sortOn);

			Object[] serviceResult = callService("getPeople", hash);
			WSPerson[] people = new WSPerson[serviceResult.length];
			for (int i = 0; i < serviceResult.length; i++) {
				HashMap rec = (HashMap) serviceResult[i];
				people[i] = new WSPerson()
					.setId((String)rec.get("id"))
					.setFirstname((String)rec.get("firstname"))
					.setLastname((String)rec.get("lastname"))
					.setUrl((String)rec.get("url"))
					.setJobTitle((String)rec.get("jobtitle"));
			}
			return people;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * remove characters like %20, etc.
	 * @param value
	 * @return
	 */
	static String correctValue(String value) {
		if (value == null) {
			return null;
		}
		
		try {
			return URLDecoder.decode(value, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RuntimeException("Encoding UTF-8 not supported.", e);
		}
	}
	
	public static boolean loginUser(HttpSession session, String username, String password) {
		try {
			HashMap hash = new HashMap();
			hash.put("login", username);
			hash.put("password", password);

			XmlRpcClientConfigImpl rpcConfig = new XmlRpcClientConfigImpl();
			XmlRpcClient rcpClient = new XmlRpcClient();

			rpcConfig.setServerURL(new URL(System.getProperty("no.machina.simula.auth_webservice")));
			rcpClient.setConfig(rpcConfig);

			Object[] methodParams = new Object[] { hash };
			Object resultSet = rcpClient.execute("check_credentials", methodParams);
			Boolean result = (Boolean) resultSet;

			session.setAttribute("simula_username", username);
			session.setAttribute("status", "OK");
			session.setAttribute("simula_userid", username);
			
			return result.booleanValue();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}
