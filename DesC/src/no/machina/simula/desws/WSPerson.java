package no.machina.simula.desws;

/**
 * Container class for people
 * @author junm
 *
 */
public class WSPerson {
	private String id;
	private String firstname;
	private String lastname;
	private String url;
	private String jobTtitle;

	public String getId() {
		return id;
	}
	public String getFirstname() {
		return firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public WSPerson() {
		super();
	}
	public String getUrl() {
		return url;
	}
	public WSPerson setUrl(String url) {
		this.url = url;
		return this;
	}
	public WSPerson setId(String id) {
		this.id = id;
		return this;
	}
	public WSPerson setFirstname(String firstname) {
		this.firstname = DesWSClientImpl.correctValue(firstname);
		return this;
	}
	public WSPerson setLastname(String lastname) {
		this.lastname = DesWSClientImpl.correctValue(lastname);
		return this;
	}
	public String getJobTitle() {
		return jobTtitle;
	}
	public WSPerson setJobTitle(String title) {
		this.jobTtitle = title;
		return this;
	}
}