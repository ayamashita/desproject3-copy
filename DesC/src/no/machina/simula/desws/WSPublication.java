package no.machina.simula.desws;

/**
 * Container class for publications
 * @author junm
 *
 */
public class WSPublication {
	public static final String SORT_ON_TITLE = "title";
	private String id;
	private String title;
	private String url;
	
	public String getId() {
		return id;
	}
	public String getTitle() {
		return title;
	}
	public WSPublication() {
		super();
	}
	public String getUrl() {
		return url;
	}
	public WSPublication setId(String id) {
		this.id = id;
		return this;
	}
	public WSPublication setTitle(String title) {
		this.title = DesWSClientImpl.correctValue(title);
		return this;
	}
	public WSPublication setUrl(String url) {
		this.url = url;
		return this;
	}
}