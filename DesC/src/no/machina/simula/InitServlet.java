/*
 * InitServlet.java
 *
 * Created on 15. oktober 2003, 14:47
 */

package no.machina.simula;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author  Cato Ervik
 * @version
 */
public class InitServlet extends HttpServlet {
    
    /** Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        String dbdriver = config.getServletContext().getInitParameter("DBDriver");
        String dburl = config.getServletContext().getInitParameter("DBURL");
        String dbuser = config.getServletContext().getInitParameter("DBUser");
        String dbpwd = config.getServletContext().getInitParameter("DBPassword");
        String security = config.getServletContext().getInitParameter("Security");
        
        config.getServletContext().getServerInfo();
        System.setProperty("no.machina.simula.dbdriver", dbdriver);
        System.setProperty("no.machina.simula.dburl", dburl);
        System.setProperty("no.machina.simula.dbuser", dbuser);
        System.setProperty("no.machina.simula.dbpwd", dbpwd);
        System.setProperty("no.machina.simula.security", security);
        System.err.println("Security is " + security);
        
        System.setProperty("no.machina.simula.desdb_webservice", config.getServletContext().getInitParameter("DesDB_webservice"));
        System.setProperty("no.machina.simula.auth_webservice", config.getServletContext().getInitParameter("Auth_webservice"));
        System.setProperty("DateFormat", config.getServletContext().getInitParameter("DateFormat"));
        System.setProperty("MaxRecordsInSearchList", config.getServletContext().getInitParameter("MaxRecordsInSearchList"));
    }

    /** Destroys the servlet.
     */
    public void destroy() {
    }
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    }
    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    
}
