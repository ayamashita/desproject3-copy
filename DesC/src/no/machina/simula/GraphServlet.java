package no.machina.simula;

import java.awt.HeadlessException;
import java.io.*;
import java.net.*;

import javax.servlet.*;
import javax.servlet.http.*;

public class GraphServlet extends HttpServlet {
    
    /** Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
    }
    
    /** Destroys the servlet.
     */
    public void destroy() {
        
    }
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        try {
            response.setContentType("image/png");
            OutputStream os = response.getOutputStream();
            os.write( Charts.getBytes() );
            os.close();
        } catch( HeadlessException e ) {
            e.printStackTrace();
            System.out.println("Setting headless to true...");
            Charts.setHeadLess( true );
        } catch( Exception e ) {
            e.printStackTrace();
            throw new ServletException( "Error!" + e );
        }
    }
    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    
}
